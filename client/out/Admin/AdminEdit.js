"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var AdminRecordFields_1 = require('./AdminRecordFields');
var Flash = require('../Components/Flash');
function arrayToLines(array) {
    return array.join('\n');
}
var AdminEdit = (function (_super) {
    __extends(AdminEdit, _super);
    function AdminEdit(props) {
        _super.call(this, props);
        this.state = {
            id: props.location.query.id,
            modelName: props.location.query.modelName,
            recordValues: {
                values: {},
                fields: [],
                extraRecordData: {
                    tableauUrls: [],
                    embedStrs: []
                }
            },
        };
    }
    AdminEdit.prototype.componentDidMount = function () {
        if (this.state.id) {
            var req = {
                model: this.state.modelName,
                id: this.state.id
            };
            server.getP('/record_values/', req)
                .then(this.valuesArrived.bind(this))
                .catch(showError.show);
        }
        else {
            server.getModelFields(this.state.modelName)
                .then(this.fieldsArrived.bind(this))
                .catch(showError.show);
        }
    };
    AdminEdit.prototype.valuesArrived = function (data) {
        this.state.recordValues = data;
        this.setState(this.state);
        return null;
    };
    AdminEdit.prototype.fieldsArrived = function (data) {
        this.state.recordValues.fields = data.fields;
        this.setState(this.state);
        return null;
    };
    AdminEdit.prototype.saveItem = function () {
        var itemData = {};
        for (var prop in this.state.recordValues.values) {
            var value = this.state.recordValues.values[prop];
            if (value == undefined)
                value = null;
            itemData[prop] = value;
        }
        if (this.state.id) {
            itemData.id = this.state.id;
            var params = {
                model: this.state.modelName,
                record: itemData,
                extraRecordData: this.state.recordValues.extraRecordData
            };
            server.putP('/save_item/', { data: JSON.stringify(params) })
                .then(this.onSave.bind(this))
                .catch(showError.show);
        }
        else {
            var params = {
                model: this.state.modelName,
                newItemData: itemData,
                extraRecordData: this.state.recordValues.extraRecordData
            };
            server.postP('/create_item/', { data: JSON.stringify(params) })
                .then(this.onSave.bind(this))
                .catch(showError.show);
        }
    };
    AdminEdit.prototype.onSave = function (status) {
        Flash.create('success', status.msg);
        react_router_1.browserHistory.push('/app/model_view?model=' + this.state.modelName);
    };
    AdminEdit.prototype.onExtraRecordDataChange = function (erdFieldName, event) {
        this.state.recordValues.extraRecordData[erdFieldName] =
            event.target.value.split('\n');
        this.setState(this.state);
    };
    AdminEdit.prototype.getSpecialFields = function () {
        return null;
    };
    AdminEdit.prototype.render = function () {
        var _this = this;
        var tableauHTMLcontent = arrayToLines(this.state.recordValues.extraRecordData.tableauUrls);
        var tableauHTML = (React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2"}, "URLs Tableau:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("textarea", {type: "text", className: "form-control", value: tableauHTMLcontent, onChange: this.onExtraRecordDataChange.bind(this, 'tableauUrls')}))));
        var embedHTMLcontent = arrayToLines(this.state.recordValues.extraRecordData.embedStrs);
        var embedHTML = (React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2"}, "Embeds:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("textarea", {type: "text", className: "form-control", value: embedHTMLcontent, onChange: this.onExtraRecordDataChange.bind(this, 'embedStrs')}))));
        return (React.createElement("div", {className: "row"}, React.createElement("div", {className: "form-horizontal"}, React.createElement(AdminRecordFields_1.AdminRecordFields, {fields: this.state.recordValues.fields, values: this.state.recordValues.values, onChange: function (v) { _this.state.recordValues.values = v; }}), tableauHTML, embedHTML, this.getSpecialFields(), React.createElement("div", {className: "form-group"}, React.createElement("div", {className: "col-sm-offset-2 col-sm-10"}, React.createElement("button", {className: "btn btn-default", onClick: this.saveItem.bind(this)}, "Salvar"))))));
    };
    return AdminEdit;
}(React.Component));
exports.AdminEdit = AdminEdit;
