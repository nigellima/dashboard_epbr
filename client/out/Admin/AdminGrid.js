"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ModelOperations = require('../lib/ModelOperations');
var ModelViewService = require('../lib/ModelViewUtils');
var react_router_1 = require('react-router');
var Flash = require('../Components/Flash');
var ExcelUploadButton_1 = require('./ExcelUploadButton');
var BytesUtils_1 = require('../lib/BytesUtils');
var FileSaver = require('file-saver');
var AdminGrid = (function (_super) {
    __extends(AdminGrid, _super);
    function AdminGrid(props) {
        _super.call(this, props);
        this.state = {
            viewParams: {},
            dataTableElement: null,
            modelOperations: {},
            modelName: props.location.query.model,
            dataTable: null
        };
        window['adminGridRef'] = this;
    }
    AdminGrid.prototype.componentDidMount = function () {
        this.state.dataTableElement = $('#mainTable');
        this.state.modelOperations = ModelOperations.getModelOperations(this.state.modelName);
        var req = { table: this.state.modelName };
        server.getP('/view_params', req)
            .then(this.initTable.bind(this))
            .catch(showError.show);
    };
    AdminGrid.prototype.initTable = function (res) {
        var columns = ModelViewService.getColumns(res.viewParams, res.types);
        columns.push({ title: "Edit", data: 'edit' });
        columns.push({ title: "Delete", data: 'delete' });
        columns.push({ title: "View", data: 'view_record' });
        this.state.dataTable = this.state.dataTableElement.DataTable({
            columns: columns,
            language: ModelViewService.datatablesPtBrTranslation,
            processing: true,
            serverSide: true,
            searching: true,
            ajax: this.ajaxFn.bind(this)
        });
        this.state.viewParams = res.viewParams;
        this.setState(this.state);
        this.state.dataTable.draw();
        return null;
    };
    AdminGrid.prototype.ajaxFn = function (data, callback, settings) {
        var orderColumns = [];
        for (var i = 0; i < data.order.length; i++) {
            var columnIndex = data.order[i].column;
            var orderObj = {
                fieldName: data.columns[columnIndex].data,
                dir: data.order[i].dir
            };
            orderColumns.push(orderObj);
        }
        var filters = [];
        if (data.search.value && data.search.value != '') {
            filters.push({
                field: orderColumns[0].fieldName,
                value: data.search.value,
            });
        }
        var req = {
            table: this.state.modelName,
            pagination: {
                first: data.start,
                itemsPerPage: data.length
            },
            order: orderColumns,
            filters: filters
        };
        server.getP('/table_data', req)
            .then(this.onTableData.bind(this, callback))
            .catch(showError.show);
    };
    AdminGrid.prototype.onTableData = function (callback, res) {
        var dataSet = [];
        for (var i = 0; i < res.records.length; i++) {
            var record = res.records[i];
            record.edit = '<a class="btn btn-large btn-primary" onclick="window.adminGridRef.editRecord(' + record.id + ')">Edit</a>';
            record.delete = '<button class="btn btn-large btn-danger" onclick="window.adminGridRef.deleteRecord(' + record.id + ')">Delete</button>';
            var paramsStr = 'source=' + this.state.modelName + '&id=' + record.id;
            record.view_record = '<a href="/app/view_record?' + paramsStr + '">Ficha</a>';
            dataSet.push(record);
        }
        var result = {
            aaData: dataSet,
            recordsTotal: res.count,
            recordsFiltered: res.count
        };
        callback(result);
        return null;
    };
    AdminGrid.prototype.editRecord = function (id) {
        this.state.modelOperations.editRecord(id);
    };
    AdminGrid.prototype.deleteRecord = function (id) {
        if (confirm("Deseja realmente apagar o registro?")) {
            this.state.modelOperations.deleteItem(id, this.onDelete.bind(this), showError.show);
        }
    };
    AdminGrid.prototype.onDelete = function (status) {
        Flash.create('success', status.msg);
        this.state.dataTable.draw();
    };
    AdminGrid.prototype.showMap = function () {
        react_router_1.browserHistory.push('/app/map?model=' + this.state.modelName);
    };
    AdminGrid.prototype.onFileUploaded = function (status) {
        Flash.create('success', status);
        this.state.dataTable.draw();
    };
    AdminGrid.prototype.getExcelFile = function () {
        server.downloadExcelFile(this.state.modelName, onExcelFile.bind(this), showError.show);
        function onExcelFile(xlsxBinary) {
            var ba = BytesUtils_1.StrToByteArray(xlsxBinary);
            var blob = new Blob([ba], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            FileSaver.saveAs(blob, "arquivo.xlsx");
        }
    };
    AdminGrid.prototype.importFromURL = function () {
        server.importFromURL(this.state.modelName, onImportFromURL.bind(this), showError.show);
        function onImportFromURL(response) {
            var statusStr = ModelViewService.formatExcelUploadResult(response);
            this.onFileUploaded(statusStr);
        }
    };
    AdminGrid.prototype.createItem = function () {
        this.state.modelOperations.createItem();
    };
    AdminGrid.prototype.render = function () {
        return (React.createElement("div", null, React.createElement("h1", null, this.state.viewParams.tableLabel), React.createElement("div", {className: "table-responsive"}, React.createElement("table", {id: "mainTable", className: "table table-striped table-bordered", width: "100%"})), React.createElement("button", {className: "btn btn-large btn-success", onClick: this.createItem.bind(this)}, "Add ", this.state.viewParams.tableLabel), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement(ExcelUploadButton_1.ExcelUploadButton, {modelName: this.state.modelName}), React.createElement("br", null), React.createElement("button", {onClick: this.getExcelFile.bind(this)}, "Download excel file")));
    };
    return AdminGrid;
}(React.Component));
exports.AdminGrid = AdminGrid;
