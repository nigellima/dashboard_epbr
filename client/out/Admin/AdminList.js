"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var Flash = require('../Components/Flash');
var AdminList = (function (_super) {
    __extends(AdminList, _super);
    function AdminList(props) {
        _super.call(this, props);
        this.state = { sourcesList: [] };
        function onData(sourcesList) {
            this.setState({ sourcesList: sourcesList });
        }
        server.sourceList(onData.bind(this), showError.show);
    }
    AdminList.prototype.onProductionFileUploaded = function (status) {
        Flash.create('success', status);
    };
    AdminList.prototype.onKmlLoad = function (model, base64Str) {
        var kmlStr = atob(base64Str);
        Flash.create('success', 'Arquivo pronto para ser enviado');
        var params = { model: model, data: JSON.stringify(kmlStr), };
        server.postP('/db_server/upload_kml', params)
            .then(function (res) { return Flash.create('success', 'KML processado: ' + res.status); })
            .catch(showError.show);
        return null;
    };
    AdminList.prototype.render = function () {
        var sourcesLinks = [];
        for (var key in this.state.sourcesList) {
            var link = "/app/model_view?model=" + key;
            var source = this.state.sourcesList[key];
            if (key.includes('ProductionUnit') || key.includes('User') || key.includes('Bid'))
                sourcesLinks.push(React.createElement("p", {key: key}, React.createElement(react_router_1.Link, {to: link}, source)));
        }
        return (React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-5"}, React.createElement("h4", null, React.createElement("p", null, React.createElement(react_router_1.Link, {to: "/app/model_view?model=News"}, "News")), React.createElement("p", null, React.createElement(react_router_1.Link, {to: "/app/insights_publisher"}, "Publisher")))), React.createElement("div", {className: "col-md-5"}, sourcesLinks)));
    };
    return AdminList;
}(React.Component));
exports.AdminList = AdminList;
