"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ListOfInputs_1 = require('./ListOfInputs');
var ListOfProjects_1 = require('./ListOfProjects');
var ManyToMany_1 = require('./ManyToMany');
var DataCombo_1 = require('./DataCombo');
var ImageShow_1 = require('./ImageShow');
var DateTime = require('react-datetime');
function editLineHTML(value, label, key) {
    return (React.createElement("div", {className: "form-group", key: key}, React.createElement("label", {className: "control-label col-sm-2"}, label, ":"), React.createElement("div", {className: "col-sm-10"}, value)));
}
exports.editLineHTML = editLineHTML;
var AdminRecordFields = (function (_super) {
    __extends(AdminRecordFields, _super);
    function AdminRecordFields(props) {
        _super.call(this, props);
        this.state = {
            values: {},
        };
    }
    AdminRecordFields.prototype.componentWillReceiveProps = function (nextProps) {
        this.buildValues(nextProps);
    };
    AdminRecordFields.prototype.buildValues = function (props) {
        for (var i = 0; i < props.fields.length; i++) {
            var field = props.fields[i];
            field.hasRef = field.type == 'ref';
            field.isDate = field.type == 'DATE';
            field.isDateTime = field.type == 'DATETIME';
            field.isBool = field.type == 'TINYINT(1)';
            if (field.hasRef) {
                if (props.values[field.name]) {
                    this.state.values[field.name] = props.values[field.name].toString();
                }
            }
            else if (field.isDate) {
                var dateStr = props.values[field.name];
                if (dateStr) {
                    var date = new Date(dateStr);
                    date.setTime(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
                    this.state.values[field.name] = date;
                }
            }
            else if (field.isPhoto) {
                var fieldValues = props.values[field.name];
                if (fieldValues && fieldValues.data) {
                    this.state.values[field.name] = fieldValues.data;
                }
            }
            else {
                this.state.values[field.name] = props.values[field.name];
            }
        }
    };
    AdminRecordFields.prototype.onChange = function (fieldName, event) {
        var value = event;
        if (event.target)
            value = event.target.value;
        value = value == '' ? null : value;
        this.state.values[fieldName] = value;
        this.props.onChange(this.state.values);
    };
    AdminRecordFields.prototype.onChangeDate = function (fieldName, value) {
        var formattedValue = value.toDate();
        this.state.values[fieldName] = formattedValue;
        this.props.onChange(this.state.values);
        this.setState(this.state);
    };
    AdminRecordFields.prototype.onComboChange = function (fieldName, value) {
        this.onChange(fieldName, value);
        this.setState(this.state);
    };
    AdminRecordFields.prototype.onMultiFieldChange = function (fieldName, event) {
        var lines = event.target.value.split('\n');
        var result = {};
        for (var _i = 0, lines_1 = lines; _i < lines_1.length; _i++) {
            var line = lines_1[_i];
            var parts = line.split(':');
            result[parts[0]] = parts[1];
        }
        this.onChange(fieldName, JSON.stringify(result));
        this.setState(this.state);
    };
    AdminRecordFields.prototype.getMultiFieldValue = function (fieldName) {
        if (!this.state.values[fieldName])
            return '';
        var obj = JSON.parse(this.state.values[fieldName]);
        var lines = [];
        for (var key in obj) {
            lines.push(key + ': ' + obj[key]);
        }
        return lines.join('\n');
    };
    AdminRecordFields.prototype.onCheckboxChange = function (field, event) {
        var value = !this.state.values[field.name];
        this.onChange(field.name, value);
        this.setState(this.state);
    };
    AdminRecordFields.prototype.fieldHTML = function (field) {
        if (field.type == 'ref') {
            return (React.createElement(DataCombo_1.DataCombo, {value: this.state.values[field.name], modelName: field.model, onChange: this.onComboChange.bind(this, field.name)}));
        }
        else if (field.type == 'DATE' || field.type == 'DATETIME') {
            var dateFormat = "DD/MM/YYYY";
            var timeFormat = field.isDate ? false : "HH:mm";
            var fieldValue = null;
            if (this.state.values[field.name]) {
                fieldValue = new Date(this.state.values[field.name]);
            }
            return React.createElement(DateTime, {value: fieldValue, input: true, dateFormat: dateFormat, timeFormat: timeFormat, onChange: this.onChangeDate.bind(this, field.name), className: "form-control input-group"});
        }
        else if (field.isList) {
            return React.createElement(ListOfInputs_1.ListOfInputs, {value: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)});
        }
        else if (field.isBool) {
            return React.createElement("input", {type: "checkbox", checked: this.state.values[field.name], onChange: this.onCheckboxChange.bind(this, field)});
        }
        else if (field.isMultiFieldText) {
            return React.createElement("textarea", {type: "text", className: "form-control", defaultValue: this.getMultiFieldValue(field.name), onChange: this.onMultiFieldChange.bind(this, field.name)});
        }
        else if (field.type.indexOf('TEXT') > -1 || field.isTextArea) {
            return React.createElement("textarea", {type: "text", className: "form-control", defaultValue: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)});
        }
        else if (field.enumValues) {
            var options = field.enumValues.map(function (enumValue, index) {
                console.log('index ' + index + ' ' + enumValue.label);
                if (index != 0)
                    return React.createElement("option", {value: enumValue, key: 'enum' + index}, enumValue);
                else
                    return React.createElement("option", {selected: 'selected', value: enumValue, key: 'enum' + index}, enumValue);
            });
            return (React.createElement("select", {className: "form-control", defaultValue: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)}, options));
        }
        else if (field.isManyToMany) {
            return React.createElement(ManyToMany_1.ManyToMany, {comboSource: field.comboSource, value: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)});
        }
        else if (field.isPhoto) {
            return React.createElement(ImageShow_1.ImageShow, {onChange: this.onChange.bind(this, field.name)});
        }
        else if (field.isProjectList) {
            return React.createElement(ListOfProjects_1.ListOfProjects, {value: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)});
        }
        else {
            return React.createElement("input", {type: "text", className: "form-control", defaultValue: this.state.values[field.name], onChange: this.onChange.bind(this, field.name)});
        }
    };
    AdminRecordFields.prototype.render = function () {
        var _this = this;
        var fieldsHTML = this.props.fields.map(function (field, index) {
            return editLineHTML(_this.fieldHTML(field), field.label, 'r' + index);
        });
        return (React.createElement("div", null, fieldsHTML));
    };
    return AdminRecordFields;
}(React.Component));
exports.AdminRecordFields = AdminRecordFields;
