"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var showError = require('../lib/ShowError');
var BytesUtils_1 = require('../lib/BytesUtils');
var ButtonUploadFile = (function (_super) {
    __extends(ButtonUploadFile, _super);
    function ButtonUploadFile(props) {
        _super.call(this, props);
        this.state = {};
    }
    ButtonUploadFile.prototype.onFileSelected = function (event) {
        var file = event.target.files[0];
        BytesUtils_1.ReadFileToBase64Str(file)
            .then(this.props.onFileLoad)
            .catch(showError.show);
    };
    ButtonUploadFile.prototype.render = function () {
        return (React.createElement("div", {className: "highlight"}, React.createElement("input", {type: "file", onChange: this.onFileSelected.bind(this)})));
    };
    return ButtonUploadFile;
}(React.Component));
exports.ButtonUploadFile = ButtonUploadFile;
