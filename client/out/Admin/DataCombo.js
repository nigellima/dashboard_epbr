"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var showError = require('../lib/ShowError');
var Server_1 = require('../lib/Server');
function optionsInCombo(values) {
    if (!values)
        return [];
    return values.map(function (value, index) {
        if (index == 0)
            return React.createElement("option", {selected: "selected", value: value.id, key: 'c' + index}, value.label);
        else
            return React.createElement("option", {value: value.id, key: 'c' + index}, value.label);
    });
}
var DataCombo = (function (_super) {
    __extends(DataCombo, _super);
    function DataCombo(props) {
        _super.call(this, props);
        this.state = {
            value: props.value,
            comboValues: []
        };
    }
    DataCombo.prototype.componentDidMount = function () {
        var req = { model: this.props.modelName };
        Server_1.getP('/combo_values/', req)
            .then(this.onComboValues.bind(this))
            .catch(showError.show);
    };
    DataCombo.prototype.componentWillReceiveProps = function (nextProps) {
        this.state.value = nextProps.value;
        this.setState(this.state);
    };
    DataCombo.prototype.onComboValues = function (res) {
        res.values.unshift({ id: null, label: '' });
        this.state.comboValues = res.values;
        this.setState(this.state);
    };
    DataCombo.prototype.onComboChange = function (evt) {
        this.state.value = evt.target.value;
        this.props.onChange(evt.target.value);
        this.setState(this.state);
    };
    DataCombo.prototype.render = function () {
        return (React.createElement("select", {className: "form-control", value: this.state.value, onChange: this.onComboChange.bind(this)}, optionsInCombo(this.state.comboValues)));
    };
    return DataCombo;
}(React.Component));
exports.DataCombo = DataCombo;
