"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Flash = require('../Components/Flash');
var showError = require('../lib/ShowError');
var Server_1 = require('../lib/Server');
var ModelViewUtils_1 = require('../lib/ModelViewUtils');
var BytesUtils_1 = require('../lib/BytesUtils');
var ExcelUploadButton = (function (_super) {
    __extends(ExcelUploadButton, _super);
    function ExcelUploadButton(props) {
        _super.call(this, props);
    }
    ExcelUploadButton.prototype.onFileLoad = function (base64Str) {
        var convertedArray = BytesUtils_1.base64ToArray(base64Str);
        Flash.create('success', 'Arquivo pronto para ser enviado');
        var params = {
            data: JSON.stringify(convertedArray),
            model: this.props.modelName
        };
        Server_1.postP('/db_server/upload_file', params)
            .then(function (res) { return Flash.create('success', ModelViewUtils_1.formatExcelUploadResult(res)); })
            .catch(showError.show);
    };
    ExcelUploadButton.prototype.render = function () {
        return (React.createElement("div", {className: "highlight"}));
    };
    return ExcelUploadButton;
}(React.Component));
exports.ExcelUploadButton = ExcelUploadButton;
