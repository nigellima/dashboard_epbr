"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var BytesUtils_1 = require('../lib/BytesUtils');
var ImageShow = (function (_super) {
    __extends(ImageShow, _super);
    function ImageShow(props) {
        _super.call(this, props);
        this.state = {
            file: '',
            imageBase64: null
        };
    }
    ImageShow.prototype.componentWillReceiveProps = function (nextProps) {
        if (!nextProps.value)
            return;
        var contentType = 'image/JPEG';
        var base64Header = 'data:' + contentType + ';base64,';
        this.state.imageBase64 = base64Header + BytesUtils_1.arrayBufferToBase64(nextProps.value);
        this.setState(this.state);
    };
    ImageShow.prototype.handleImageChange = function (e) {
        var reader = new FileReader();
        var file = e.target.files[0];
        reader.onloadend = this.onFileLoad.bind(this, reader, file);
        reader.readAsDataURL(file);
    };
    ImageShow.prototype.onFileLoad = function (reader, file) {
        this.setState({
            file: file,
            imageBase64: reader.result
        });
        var dsBase64 = reader.result;
        var trimmedBase64 = BytesUtils_1.removeBase64Header(dsBase64);
        this.props.onChange(BytesUtils_1.base64ToArray(trimmedBase64));
    };
    ImageShow.prototype.render = function () {
        var imageBase64 = this.state.imageBase64;
        var imagePreview = null;
        if (imageBase64) {
            imagePreview = (React.createElement("img", {src: imageBase64}));
        }
        else {
            imagePreview = (React.createElement("img", {src: this.props.imgPath}));
        }
        return (React.createElement("div", null, React.createElement("input", {type: "file", onChange: this.handleImageChange.bind(this)}), imagePreview));
    };
    return ImageShow;
}(React.Component));
exports.ImageShow = ImageShow;
