"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ListOfInputs = (function (_super) {
    __extends(ListOfInputs, _super);
    function ListOfInputs(props) {
        _super.call(this, props);
        this.state = {
            items: props.value ? props.value : []
        };
    }
    ListOfInputs.prototype.addItem = function () {
        this.state.items.push('');
        this.setState(this.state);
        this.props.onChange(this.state.items);
    };
    ListOfInputs.prototype.removeItem = function (index) {
        this.state.items.splice(index, 1);
        this.setState(this.state);
        this.props.onChange(this.state.items);
    };
    ListOfInputs.prototype.updateItem = function (index, event) {
        this.state.items[index] = event.target.value;
        this.setState(this.state);
        this.props.onChange(this.state.items);
    };
    ListOfInputs.prototype.render = function () {
        var _this = this;
        var items = this.state.items.map(function (item, index) {
            return (React.createElement("tr", {key: 'item' + index}, React.createElement("td", null, React.createElement("input", {type: "text", value: item, onChange: _this.updateItem.bind(_this, index)}), React.createElement("br", null)), React.createElement("td", null, React.createElement("button", {className: "btn btn-default", onClick: _this.removeItem.bind(_this, index)}, "Remover"))));
        });
        return (React.createElement("table", {className: "table"}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("td", {width: "300"}, React.createElement("table", {className: "table"}, React.createElement("tbody", null, " ", items, " "))), React.createElement("td", null, React.createElement("button", {onClick: this.addItem.bind(this)}, "Add"), React.createElement("br", null))))));
    };
    return ListOfInputs;
}(React.Component));
exports.ListOfInputs = ListOfInputs;
