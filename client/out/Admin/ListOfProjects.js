"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ProjectSearch_1 = require('../Components/ProjectSearch');
var ListOfProjects = (function (_super) {
    __extends(ListOfProjects, _super);
    function ListOfProjects(props) {
        _super.call(this, props);
        this.state = {
            projects: props.value ? props.value : [],
            selectedProject: {},
            description: ''
        };
    }
    ListOfProjects.prototype.removeItem = function (index) {
        this.state.projects.splice(index, 1);
        this.setState(this.state);
        this.props.onChange(this.state.projects);
    };
    ;
    ListOfProjects.prototype.onProjectSelected = function (selectedItem) {
        selectedItem.description = this.state.description;
        this.state.projects.push(selectedItem);
        this.state.description = "";
        this.setState(this.state);
        this.props.onChange(this.state.projects);
    };
    ;
    ListOfProjects.prototype.render = function () {
        var _this = this;
        var projects = this.state.projects.map(function (project, index) {
            return (React.createElement("tr", {key: 'item' + index}, React.createElement("td", null, project.name), React.createElement("td", null, project.description), React.createElement("td", null, React.createElement("button", {className: "btn btn-default", onClick: _this.removeItem.bind(_this, index)}, "Remover"))));
        });
        return (React.createElement("table", {className: "table"}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("td", {width: "300"}, React.createElement("table", {className: "table table-bordered"}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("th", null, "Projeto"), React.createElement("th", null, "Descrição")), projects))), React.createElement("td", null, React.createElement("table", {className: "table table-bordered"}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("td", null, React.createElement(ProjectSearch_1.ProjectSearch, {onItemSelected: this.onProjectSelected.bind(this)}))), React.createElement("tr", null, React.createElement("td", null, "Descrição: ", React.createElement("input", {type: "text", defaultValue: this.state.description, onChange: function (e) { _this.state.description = e.target.value; }}))))))))));
    };
    return ListOfProjects;
}(React.Component));
exports.ListOfProjects = ListOfProjects;
