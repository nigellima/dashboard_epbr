"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var showError = require('../lib/ShowError');
var server = require('../lib/Server');
var ManyToMany = (function (_super) {
    __extends(ManyToMany, _super);
    function ManyToMany(props) {
        _super.call(this, props);
        this.state = {
            comboValues: [],
            modelValues: props.value ? props.value : [],
            comboValuesMap: {}
        };
    }
    ManyToMany.prototype.componentDidMount = function () {
        var req = { model: this.props.comboSource };
        server.getP('/combo_values/', req)
            .then(this.onComboValues.bind(this))
            .catch(showError.show);
    };
    ManyToMany.prototype.componentWillReceiveProps = function (nextProps) {
        this.state.modelValues = nextProps.value ? nextProps.value : [];
        this.setState(this.state);
    };
    ManyToMany.prototype.onComboValues = function (res) {
        var values = res.values;
        this.state.comboValues = values;
        this.state.comboValuesMap = {};
        for (var i = 0; i < values.length; i++) {
            this.state.comboValuesMap[values[i].id] = values[i].label;
        }
        this.selectedId = values[0].id;
        this.setState(this.state);
    };
    ManyToMany.prototype.remove = function (index) {
        this.state.modelValues.splice(index, 1);
        this.setState(this.state);
        this.props.onChange(this.state.modelValues);
    };
    ManyToMany.prototype.add = function () {
        var selectedId = this.selectedId;
        var newItem = {
            id: selectedId,
            name: this.state.comboValuesMap[selectedId]
        };
        this.state.modelValues.push(newItem);
        this.setState(this.state);
        this.props.onChange(this.state.modelValues);
    };
    ManyToMany.prototype.render = function () {
        var _this = this;
        var items = this.state.modelValues.map(function (modelValue, index) {
            var label = _this.state.comboValuesMap[modelValue.id];
            return (React.createElement("tr", {key: 'item' + index}, React.createElement("td", null, label), React.createElement("td", null, React.createElement("button", {className: "btn btn-default", onClick: _this.remove.bind(_this, index)}, "Remover"))));
        });
        var comboItems = this.state.comboValues.map(function (comboValue, index) {
            return React.createElement("option", {value: comboValue.id, key: 'ci' + index}, " ", comboValue.label, " ");
        });
        return (React.createElement("table", {className: "table", style: { width: "100%" }}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("td", {width: "300"}, React.createElement("table", {className: "table table-bordered"}, React.createElement("tbody", null, items))), React.createElement("td", null, React.createElement("button", {className: "btn btn-default", onClick: this.add.bind(this)}, "Add"), React.createElement("br", null), React.createElement("select", {onChange: function (e) { _this.selectedId = e.target.value; }}, comboItems)), React.createElement("td", null)))));
    };
    return ManyToMany;
}(React.Component));
exports.ManyToMany = ManyToMany;
