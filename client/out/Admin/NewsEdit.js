"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var ProjectSearch_1 = require('../Components/ProjectSearch');
var Flash = require('../Components/Flash');
var ImageShow_1 = require('./ImageShow');
var ReactQuill = require('react-quill');
var NewsEdit = (function (_super) {
    __extends(NewsEdit, _super);
    function NewsEdit(props) {
        _super.call(this, props);
        this.state = {
            mainTitle: '',
            title: '',
            content: '',
            tableauUrl: null,
            id: props.location.query.id,
            production_unit_id: null,
            image: null,
            production_units: []
        };
        this.modelName = 'News';
    }
    NewsEdit.prototype.componentDidMount = function () {
        if (this.state.id) {
            this.state.mainTitle = "Editar notícia";
            var req = {
                model: this.modelName,
                id: this.state.id
            };
            server.getP('/record_values/', req)
                .then(this.onServerData.bind(this))
                .catch(showError.show);
        }
        else {
            this.state.mainTitle = "Nova notícia";
            this.setState(this.state);
        }
        server.getP('/combo_values', { model: 'ProductionUnit' })
            .then(this.onUsers.bind(this))
            .catch(showError.show);
    };
    NewsEdit.prototype.onServerData = function (data) {
        this.state.title = data.values.title;
        this.state.content = data.values.content;
        this.state.production_unit_id = data.values.production_unit_id;
        this.state.tableauUrl = data.values.tableau_url;
        console.log(data.values);
        this.setState(this.state);
    };
    NewsEdit.prototype.onUsers = function (res) {
        this.state.production_units = res.values;
        console.log(res.values);
        this.setState(this.state);
    };
    NewsEdit.prototype.onProjectSelected = function (selectedItem) {
        var linkStr = '<a href="/app/view_record?source=' + selectedItem.model + '&id=' + selectedItem.id + '">' + selectedItem.name + '</a>';
        this.state.content = this.insertLinkInContent(this.state.content, linkStr);
        this.setState(this.state);
    };
    NewsEdit.prototype.insertLinkInContent = function (previousContent, linkStr) {
        var symbolPos = previousContent.lastIndexOf('<');
        var beforeSymbolStr = previousContent.slice(0, symbolPos);
        var afterSymboStr = previousContent.slice(symbolPos, previousContent.length);
        return beforeSymbolStr + linkStr + afterSymboStr;
    };
    NewsEdit.prototype.saveItem = function () {
        var itemData = {};
        itemData.title = this.state.title;
        itemData.content = this.state.content;
        itemData.production_unit_id = (this.state.production_unit_id) ? this.state.production_unit_id : this.state.production_units[0].id;
        itemData.image = this.state.image;
        itemData.tableau_url = this.state.tableauUrl;
        if (this.state.id) {
            itemData.id = this.state.id;
            var params = {
                model: this.modelName,
                record: itemData,
                extraRecordData: {
                    tableauUrls: [],
                    embedStrs: []
                }
            };
            server.putP('/save_item/', { data: JSON.stringify(params) })
                .then(this.onSave.bind(this))
                .catch(showError.show);
        }
        else {
            var params = {
                model: this.modelName,
                newItemData: itemData,
            };
            server.postP('/create_item/', { data: JSON.stringify(params) })
                .then(this.onSave.bind(this))
                .catch(showError.show);
        }
    };
    NewsEdit.prototype.onSave = function (status) {
        Flash.create('success', status.msg);
        react_router_1.browserHistory.push("/app/model_view?model=" + this.modelName);
    };
    NewsEdit.prototype.onImage = function (image) {
        this.state.image = image;
    };
    NewsEdit.prototype.onUserChange = function (e) {
        this.state.production_unit_id = e.target.value;
        this.setState(this.state);
    };
    NewsEdit.prototype.render = function () {
        var _this = this;
        var imgPath = server.paths.baseImg + 'insights/img_' + this.state.id + '.jpg';
        var titleHTML = (React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", htmlFor: "title_box"}, "Título:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("input", {type: "text", className: "form-control", value: this.state.title, onChange: function (e) { _this.state.title = e.target.value; _this.setState(_this.state); }}))));
        var newContentHTML = (React.createElement("div", {className: "form-group", style: { border: "3px" }}, React.createElement("label", {className: "control-label col-sm-2", htmlFor: "content_box"}, "Notícia:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("table", {className: "table table-bordered"}, React.createElement("tbody", null, React.createElement("tr", null, React.createElement("td", null, React.createElement(ReactQuill, {theme: "snow", value: this.state.content, onChange: function (v) { _this.state.content = v; _this.setState(_this.state); }}))))), "Busca: ", React.createElement(ProjectSearch_1.ProjectSearch, {onItemSelected: this.onProjectSelected.bind(this)}))));
        var usersHTMLOptions = this.state.production_units.map(function (user, i) {
            return React.createElement("option", {value: user.id, key: i}, user.label);
        });
        var authortHTML = (React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", htmlFor: "title_box"}, "FPSo:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("select", {className: "form-control", value: this.state.production_unit_id, onChange: this.onUserChange.bind(this)}, usersHTMLOptions))));
        return (React.createElement("div", null, React.createElement("h4", {className: "col-sm-2"}, this.state.mainTitle), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("br", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "form-horizontal"}, titleHTML, authortHTML, newContentHTML, React.createElement("div", {className: "form-group"}, React.createElement("div", {className: "col-sm-offset-2 col-sm-10"}, React.createElement("textarea", {rows: 8, cols: 100, value: this.state.content, onChange: function (e) { _this.state.content = e.target.value; _this.setState(_this.state); }}))), React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", htmlFor: "title_box"}, "Tableau URLs:"), React.createElement("div", {className: "col-sm-10"}, React.createElement("textarea", {rows: 4, className: "form-control", value: this.state.tableauUrl, onChange: function (e) { _this.state.tableauUrl = e.target.value; _this.setState(_this.state); }}))), React.createElement("div", {className: "form-group"}, React.createElement("div", {className: "col-sm-offset-2 col-sm-10"}, React.createElement("button", {className: "btn btn-default", onClick: this.saveItem.bind(this)}, "Salvar"))))), "Foto de capa", React.createElement(ImageShow_1.ImageShow, {onChange: this.onImage.bind(this), imgPath: imgPath})));
    };
    return NewsEdit;
}(React.Component));
exports.NewsEdit = NewsEdit;
