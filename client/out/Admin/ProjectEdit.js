"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var AdminRecordFields_1 = require('./AdminRecordFields');
var AdminEdit = require('./AdminEdit');
var ManyToMany_1 = require('./ManyToMany');
var DataCombo_1 = require('./DataCombo');
var ProjectEdit = (function (_super) {
    __extends(ProjectEdit, _super);
    function ProjectEdit(props) {
        _super.call(this, props);
        this.state.modelName = 'Project';
        if (!this.state.id) {
            var newJsonField = {
                contractors: [],
                owner_persons_id: []
            };
            this.state.recordValues.values = { json_field: newJsonField };
        }
    }
    ProjectEdit.prototype.valuesArrived = function (data) {
        _super.prototype.valuesArrived.call(this, data);
        if (!this.state.recordValues.values.json_field)
            return null;
        this.state.recordValues.values.json_field =
            JSON.parse(this.state.recordValues.values.json_field);
        this.setState(this.state);
        return null;
    };
    ProjectEdit.prototype.contractorInterface = function (index) {
        var contractor_id = this.state.recordValues.values.json_field.contractors[index].contractor_id;
        var contractedHTML = (React.createElement(DataCombo_1.DataCombo, {value: contractor_id, modelName: "Company", onChange: this.onContractorFieldChanged.bind(this, index, 'contractor_id')}));
        var scope = this.state.recordValues.values.json_field.contractors[index].scope;
        var scopeHTML = (React.createElement("input", {type: "text", className: "form-control", value: scope, onChange: this.onContractorFieldChanged.bind(this, index, 'scope')}));
        var persons_id = this.state.recordValues.values.json_field.contractors[index].persons_id;
        var personsHTML = (React.createElement(ManyToMany_1.ManyToMany, {comboSource: 'Person', value: this.idsToObj(persons_id), onChange: this.onContractorMTMChanged.bind(this, index, 'persons_id')}));
        var contracts_id = this.state.recordValues.values.json_field.contractors[index].contracts_id;
        var contractsHTML = (React.createElement(ManyToMany_1.ManyToMany, {comboSource: 'Contract', value: this.idsToObj(contracts_id), onChange: this.onContractorMTMChanged.bind(this, index, 'contracts_id')}));
        return (React.createElement("div", {key: 'contracted' + index}, React.createElement("hr", null), React.createElement("h3", null, "Contratada ", index + 1), React.createElement("br", null), AdminRecordFields_1.editLineHTML(contractedHTML, 'Contratada', 'c' + index), AdminRecordFields_1.editLineHTML(scopeHTML, 'Escopo', 'e' + index), AdminRecordFields_1.editLineHTML(personsHTML, 'Pessoas', 'p' + index), AdminRecordFields_1.editLineHTML(contractsHTML, 'Contratos', 'ctr' + index), React.createElement("button", {onClick: this.onRemoveContractedButtonClick.bind(this, index)}, "Remover"), React.createElement("hr", null)));
    };
    ProjectEdit.prototype.onRemoveContractedButtonClick = function (index) {
        this.state.recordValues.values.json_field.contractors.splice(index, 1);
        this.setState(this.state);
    };
    ProjectEdit.prototype.onContractorFieldChanged = function (index, fieldName, evt) {
        var value = evt;
        if (evt.target)
            value = evt.target.value;
        this.state.recordValues.values.json_field.contractors[index][fieldName] = value;
        this.setState(this.state);
    };
    ProjectEdit.prototype.onContractorMTMChanged = function (index, fieldName, ids_objs) {
        var ids = ids_objs.map(function (obj) { return obj.id; });
        this.state.recordValues.values.json_field.contractors[index][fieldName] = ids;
        this.setState(this.state);
    };
    ProjectEdit.prototype.onPersonOwnerChange = function (persons_ids) {
        var ids = persons_ids.map(function (obj) { return obj.id; });
        this.state.recordValues.values.json_field.owner_persons_id = ids;
        this.setState(this.state);
    };
    ProjectEdit.prototype.idsToObj = function (ids) {
        return ids.map(function (id) { return { id: id }; });
    };
    ProjectEdit.prototype.addContracted = function () {
        var emptyContracted = {
            contractor_id: "",
            persons_id: [],
            contracts_id: [],
            scope: ""
        };
        this.state.recordValues.values.json_field.contractors.push(emptyContracted);
        this.setState(this.state);
    };
    ProjectEdit.prototype.getSpecialFields = function () {
        var jsonField = this.state.recordValues.values.json_field;
        if (!jsonField || !jsonField.contractors)
            return null;
        var manyToManyHTML = (React.createElement(ManyToMany_1.ManyToMany, {comboSource: 'Person', value: this.idsToObj(this.state.recordValues.values.json_field.owner_persons_id), onChange: this.onPersonOwnerChange.bind(this)}));
        var contractorsHTML = [];
        for (var i = 0; i < jsonField.contractors.length; i++) {
            contractorsHTML.push(this.contractorInterface(i));
        }
        return (React.createElement("div", null, AdminRecordFields_1.editLineHTML(manyToManyHTML, 'Pessoas da contratante', 'pc'), contractorsHTML, React.createElement("button", {onClick: this.addContracted.bind(this)}, "Adicionar contratada")));
    };
    return ProjectEdit;
}(AdminEdit.AdminEdit));
exports.ProjectEdit = ProjectEdit;
