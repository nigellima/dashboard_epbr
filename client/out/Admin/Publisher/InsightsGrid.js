"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../../lib/Server');
var showError = require('../../lib/ShowError');
var DateUtils_1 = require('../../lib/DateUtils');
var InsightsGrid = (function (_super) {
    __extends(InsightsGrid, _super);
    function InsightsGrid(props) {
        _super.call(this, props);
        this.state = {
            insights: null,
            insightsCount: -1,
            insightsIndex: 0,
            itemsPerPage: 10
        };
    }
    InsightsGrid.prototype.componentDidMount = function () {
        this.getInsights();
    };
    InsightsGrid.prototype.getInsights = function () {
        var req = {
            table: 'News',
            pagination: {
                first: this.state.insightsIndex.toString(),
                itemsPerPage: this.state.itemsPerPage.toString()
            },
            order: [{
                    fieldName: 'created_at',
                    dir: 'desc'
                }],
            filters: []
        };
        Server_1.getP('/table_data', req)
            .then(this.onInsights.bind(this))
            .catch(showError.show);
    };
    InsightsGrid.prototype.onInsights = function (res) {
        this.state.insights = res;
        this.state.insightsCount = res.count;
        this.setState(this.state);
        return null;
    };
    InsightsGrid.prototype.drag = function (insight, ev) {
        ev.dataTransfer.setData("text", JSON.stringify(insight));
    };
    InsightsGrid.prototype.paginationClicked = function (increment) {
        this.state.insightsIndex += increment;
        if (this.state.insightsIndex < 0) {
            this.state.insightsIndex = 0;
        }
        this.getInsights();
    };
    InsightsGrid.prototype.render = function () {
        var _this = this;
        var insights = this.state.insights;
        if (!insights) {
            return React.createElement("div", null);
        }
        var insightsHtml = insights.records.map(function (insight, index) {
            return (React.createElement("tr", {key: index}, React.createElement("td", {draggable: true, onDragStart: _this.drag.bind(_this, insight)}, insight.title), React.createElement("td", null, DateUtils_1.dateTimeFormat(insight.created_at))));
        });
        return (React.createElement("div", null, React.createElement("table", {className: "table"}, React.createElement("thead", null, React.createElement("tr", null, React.createElement("th", null, "Título"), React.createElement("th", null, "Data"))), React.createElement("tbody", null, insightsHtml)), React.createElement("br", null), React.createElement("button", {className: "btn btn-default", onClick: this.paginationClicked.bind(this, -this.state.itemsPerPage)}, "Anterior"), React.createElement("span", {style: { padding: 10 }}, this.state.insightsIndex + 1, " de ", this.state.insightsCount), React.createElement("button", {className: "btn btn-default", onClick: this.paginationClicked.bind(this, this.state.itemsPerPage)}, "Próximo")));
    };
    return InsightsGrid;
}(React.Component));
exports.InsightsGrid = InsightsGrid;
