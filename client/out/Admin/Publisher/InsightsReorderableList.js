"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
function removeById(array, id) {
    for (var i = 0; i < array.length; i++) {
        if (array[i].id == id) {
            array.splice(i, 1);
            break;
        }
    }
}
exports.removeById = removeById;
function allowDrop(ev) {
    ev.preventDefault();
}
exports.allowDrop = allowDrop;
var InsightsReorderableList = (function (_super) {
    __extends(InsightsReorderableList, _super);
    function InsightsReorderableList(props) {
        _super.call(this, props);
        this.state = {
            data: this.props.data
        };
    }
    InsightsReorderableList.prototype.drag = function (insight, ev) {
        insight.source = this.props.listName;
        ev.dataTransfer.setData("text", JSON.stringify(insight));
    };
    InsightsReorderableList.prototype.dropOnItem = function (objIndex, ev) {
        ev.preventDefault();
        var insightStr = ev.dataTransfer.getData("text");
        var insight = JSON.parse(insightStr);
        var items = this.state.data;
        removeById(items, insight.id);
        items.splice(objIndex, 0, insight);
        this.props.onChange(this.props.listName, items);
        this.state.data = items;
        this.setState(this.state);
    };
    InsightsReorderableList.prototype.dropOnPlus = function (ev) {
        ev.preventDefault();
        var insightStr = ev.dataTransfer.getData("text");
        var insight = JSON.parse(insightStr);
        var items = this.state.data;
        removeById(items, insight.id);
        items.push(insight);
        this.props.onChange(this.props.listName, items);
        this.state.data = items;
        this.setState(this.state);
    };
    InsightsReorderableList.prototype.render = function () {
        var _this = this;
        var listItems = this.state.data.map(function (item, i) {
            return React.createElement("li", {className: "list-group-item", draggable: true, onDragStart: _this.drag.bind(_this, item), onDrop: _this.dropOnItem.bind(_this, i), onDragOver: allowDrop, key: i}, item.title);
        });
        return (React.createElement("div", null, React.createElement("div", {style: { width: 300 }}, React.createElement("ul", {className: "list-group"}, listItems), React.createElement("img", {src: "images/plus.png", alt: "", onDragOver: allowDrop, onDrop: this.dropOnPlus.bind(this)})), React.createElement("br", null)));
    };
    return InsightsReorderableList;
}(React.Component));
exports.InsightsReorderableList = InsightsReorderableList;
