"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../../lib/Server');
var showError = require('../../lib/ShowError');
var InsightsReorderableList_1 = require('./InsightsReorderableList');
var InsightsGrid_1 = require('./InsightsGrid');
var react_router_1 = require('react-router');
var Flash = require('../../Components/Flash');
var Publisher = (function (_super) {
    __extends(Publisher, _super);
    function Publisher(props) {
        _super.call(this, props);
        this.state = {
            insights: null
        };
    }
    Publisher.prototype.componentDidMount = function () {
        this.getInsights();
    };
    Publisher.prototype.getInsights = function () {
        Server_1.getP('/insights', {})
            .then(this.onInsights.bind(this))
            .catch(showError.show);
    };
    Publisher.prototype.onInsights = function (res) {
        this.state.insights = res;
        this.setState(this.state);
        return null;
    };
    Publisher.prototype.onListChange = function (listName, items) {
        this.state.insights[listName] = items;
    };
    Publisher.prototype.drop = function (ev) {
        ev.preventDefault();
        var insightStr = ev.dataTransfer.getData("text");
        var insight = JSON.parse(insightStr);
        var sourceName = insight['source'];
        var items = this.state.insights[sourceName];
        if (!items)
            return;
        InsightsReorderableList_1.removeById(items, insight.id);
        this.state.insights[sourceName] = items;
        this.setState(this.state);
    };
    Publisher.prototype.save = function () {
        function getIds(insights) {
            var ids = [];
            for (var _i = 0, insights_1 = insights; _i < insights_1.length; _i++) {
                var item = insights_1[_i];
                ids.push(item.id);
            }
            return ids;
        }
        var flexSlider = [];
        var data = {
            flexSlider: getIds(this.state.insights.flexSlider),
            section1Articles: getIds(this.state.insights.section1Articles),
            section2Articles: getIds(this.state.insights.section2Articles),
            section3Articles: getIds(this.state.insights.section3Articles),
            section4Articles: getIds(this.state.insights.section4Articles),
        };
        Server_1.postP('/save_insights_publisher', { data: JSON.stringify(data) })
            .then(this.onSave.bind(this))
            .catch(showError.show);
    };
    Publisher.prototype.onSave = function (res) {
        Flash.create('success', res.msg);
        react_router_1.browserHistory.push('/app/admin');
        return null;
    };
    Publisher.prototype.render = function () {
        var insights = this.state.insights;
        if (!insights) {
            return React.createElement("div", null);
        }
        return (React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, "FlexSlider", React.createElement(InsightsReorderableList_1.InsightsReorderableList, {data: insights.flexSlider, onChange: this.onListChange.bind(this), listName: "flexSlider"})), React.createElement("div", {className: "col-md-3"}, React.createElement("img", {src: "images/trash.jpg", alt: "", onDragOver: InsightsReorderableList_1.allowDrop, onDrop: this.drop.bind(this)})), React.createElement("div", {className: "col-md-3"}, React.createElement("button", {className: "btn btn-default", onClick: this.save.bind(this)}, "Salvar"))), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, "Section 1", React.createElement(InsightsReorderableList_1.InsightsReorderableList, {data: insights.section1Articles, onChange: this.onListChange.bind(this), listName: "section1Articles"})), React.createElement("div", {className: "col-md-6"}, "Section 2", React.createElement(InsightsReorderableList_1.InsightsReorderableList, {data: insights.section2Articles, onChange: this.onListChange.bind(this), listName: "section2Articles"}))), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, "Section 3", React.createElement(InsightsReorderableList_1.InsightsReorderableList, {data: insights.section3Articles, onChange: this.onListChange.bind(this), listName: "section3Articles"})), React.createElement("div", {className: "col-md-6"}, "Section 4", React.createElement(InsightsReorderableList_1.InsightsReorderableList, {data: insights.section4Articles, onChange: this.onListChange.bind(this), listName: "section4Articles"})))), React.createElement("div", {className: "col-md-6"}, React.createElement(InsightsGrid_1.InsightsGrid, null))));
    };
    return Publisher;
}(React.Component));
exports.Publisher = Publisher;
