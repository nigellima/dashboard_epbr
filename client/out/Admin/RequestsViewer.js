"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ModelViewService = require('../lib/ModelViewUtils');
var TableUtils_1 = require('../lib/TableUtils');
var moment = require('moment');
var DateTime = require('react-datetime');
var dateFormat = "DD/MM/YYYY";
var RequestsViewer = (function (_super) {
    __extends(RequestsViewer, _super);
    function RequestsViewer(props) {
        _super.call(this, props);
        this.state = {
            dataTable: null,
            type: "INDIVIDUAL_ACCESS",
            users: [],
            accessQtyUser: []
        };
        window['paginatedTableRef'] = this;
    }
    RequestsViewer.prototype.componentDidMount = function () {
        this.initTable(this.props);
        var req = { model: 'UsersUsername' };
        server.getP('/combo_values', req)
            .then(this.onUsers.bind(this))
            .catch(showError.show);
    };
    RequestsViewer.prototype.onUsers = function (res) {
        this.state.users = res.values;
        this.setState(this.state);
    };
    RequestsViewer.prototype.initTable = function (props) {
        var dataTableElement = $('#mainTable');
        var fields = [
            {
                label: 'Usuário',
                fieldName: 'user',
                type: 'VARCHAR'
            },
            {
                label: 'Caminho',
                fieldName: 'path',
                type: 'VARCHAR'
            },
            {
                label: 'Texto',
                fieldName: 'translation',
                type: 'VARCHAR'
            },
            {
                label: 'Momento',
                fieldName: 'created_at',
                type: 'DATETIME'
            },
        ];
        var tableParams = { fields: fields };
        var _a = TableUtils_1.genColumns(tableParams), columns = _a.columns, currencyColumnsIndexes = _a.currencyColumnsIndexes;
        this.state.dataTable = dataTableElement.DataTable({
            columns: columns,
            language: ModelViewService.datatablesPtBrTranslation,
            processing: true,
            serverSide: true,
            searching: false,
            dom: 'rtip',
            ajax: this.ajaxFn.bind(this, props),
        });
        this.state.dataTable.draw();
    };
    RequestsViewer.prototype.getDateFilters = function () {
        var filters = [];
        if (this.startDate) {
            filters.push({
                field: 'request_log.created_at',
                gte: '"' + this.startDate + '"'
            });
        }
        if (this.endDate) {
            filters.push({
                field: 'request_log.created_at',
                lte: '"' + this.endDate + '"'
            });
        }
        return filters;
    };
    RequestsViewer.prototype.ajaxFn = function (props, data, callback, settings) {
        var orderColumns = [];
        for (var i = 0; i < data.order.length; i++) {
            var columnIndex = data.order[i].column;
            var orderObj = {
                fieldName: data.columns[columnIndex].data,
                dir: data.order[i].dir
            };
            orderColumns.push(orderObj);
        }
        var options = {
            queryName: 'requests',
            queryParams: {
                pagination: {
                    first: data.start,
                    itemsPerPage: data.length
                },
                order: orderColumns,
                filters: []
            }
        };
        if (this.usernameFilter) {
            options.queryParams.filters.push({
                field: 'user',
                equal: '"' + this.usernameFilter + '"'
            });
        }
        var dateFilters = this.getDateFilters();
        options.queryParams.filters = options.queryParams.filters.concat(dateFilters);
        server.getTableData(options)
            .then(this.onTableData.bind(this, callback))
            .catch(showError.show);
    };
    RequestsViewer.prototype.onChangeDate = function (varName, value) {
        var formattedValue = moment(value, dateFormat).format("YYYY-MM-DD");
        this[varName] = formattedValue;
        this.update();
    };
    RequestsViewer.prototype.onUserChange = function (e) {
        this.usernameFilter = e.target.value;
        this.update();
    };
    RequestsViewer.prototype.onTypeChange = function (e) {
        this.state.type = e.target.value;
        if (this.state.type == "INDIVIDUAL_ACCESS") {
            this.initTable(this.props);
        }
        this.update();
    };
    RequestsViewer.prototype.update = function () {
        if (this.state.type == "INDIVIDUAL_ACCESS") {
            this.state.dataTable.draw();
        }
        else {
            var options = {
                queryName: 'requestsByUser',
                queryParams: {
                    pagination: {
                        first: 0,
                        itemsPerPage: 200
                    },
                    order: [],
                    filters: []
                }
            };
            var dateFilters = this.getDateFilters();
            options.queryParams.filters = options.queryParams.filters.concat(dateFilters);
            server.getTableData(options)
                .then(this.onUsersQtyData.bind(this))
                .catch(showError.show);
        }
    };
    RequestsViewer.prototype.onUsersQtyData = function (serverResult) {
        this.state.users = serverResult.records;
        this.setState(this.state);
    };
    RequestsViewer.prototype.onTableData = function (callback, serverResult) {
        var result = {
            aaData: serverResult.records,
            recordsTotal: serverResult.count,
            recordsFiltered: serverResult.count
        };
        callback(result);
        return null;
    };
    RequestsViewer.prototype.render = function () {
        var today = new Date();
        var minusWeek = new Date();
        minusWeek.setDate(minusWeek.getDate() - 7);
        var startDate = minusWeek;
        var endDate = today;
        var bottomHTML = null;
        if (this.state.type == "INDIVIDUAL_ACCESS") {
            var usersComboSource = this.state.users;
            usersComboSource.splice(0, 0, { label: 'Todos' });
            var usersHTMLOptions = usersComboSource.map(function (user, i) {
                return React.createElement("option", {value: user.id, key: i}, user.label);
            });
            bottomHTML = (React.createElement("div", null, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-2"}, "Filtrar usuário:"), React.createElement("div", {className: "col-md-3"}, React.createElement("select", {className: "form-control", onChange: this.onUserChange.bind(this)}, usersHTMLOptions))), React.createElement("br", null), React.createElement("div", {className: "main-table table-responsive bootstrap-table"}, React.createElement("table", {id: "mainTable", className: "table", cellSpacing: "0", width: "100%"}))));
        }
        else {
            var rowsHTML = this.state.users.map(function (u, i) {
                return (React.createElement("tr", {key: i}, React.createElement("td", null, u.name), React.createElement("td", {style: { textAlign: "right" }}, u.qty)));
            });
            bottomHTML = (React.createElement("table", null, React.createElement("thead", null, React.createElement("tr", null, React.createElement("th", null, "Usuário"), React.createElement("th", null, "Nro acessos"))), React.createElement("tbody", null, rowsHTML)));
        }
        return (React.createElement("div", null, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-2"}, "Data de início:"), React.createElement("div", {className: "col-md-3"}, React.createElement(DateTime, {defaultValue: startDate, input: true, dateFormat: dateFormat, timeFormat: false, onChange: this.onChangeDate.bind(this, 'startDate'), className: "form-control input-group"}))), React.createElement("br", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-2"}, "Data de fim:"), React.createElement("div", {className: "col-md-3"}, React.createElement(DateTime, {defaultValue: endDate, input: true, dateFormat: dateFormat, timeFormat: false, onChange: this.onChangeDate.bind(this, 'endDate'), className: "form-control input-group"}))), React.createElement("br", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-2"}, "Mostrar:"), React.createElement("div", {className: "col-md-3"}, React.createElement("select", {className: "form-control", onChange: this.onTypeChange.bind(this)}, React.createElement("option", {value: "INDIVIDUAL_ACCESS", key: "ALL"}, "Acessos individuais"), React.createElement("option", {value: "PER_USER", key: "PER_USER"}, "Quantidade por usuário")))), React.createElement("br", null), bottomHTML));
    };
    return RequestsViewer;
}(React.Component));
exports.RequestsViewer = RequestsViewer;
