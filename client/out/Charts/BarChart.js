"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var DiscreteChart_1 = require('./DiscreteChart');
var BarChart = (function (_super) {
    __extends(BarChart, _super);
    function BarChart() {
        _super.apply(this, arguments);
    }
    BarChart.prototype.render = function () {
        var chartOptions = {
            chartArea: { width: '50%' },
            height: 500,
            animation: {
                duration: 500,
                easing: 'inAndOut',
            },
            hAxis: {
                title: 'Quantidade',
                minValue: 0
            },
            vAxis: {
                title: this.props.axisName,
                format: '0'
            }
        };
        return (React.createElement(DiscreteChart_1.DiscreteChart, {analyticsData: this.props.analyticsData, gConstrutorFunctionName: 'BarChart', chartOptions: chartOptions}));
    };
    return BarChart;
}(React.Component));
exports.BarChart = BarChart;
