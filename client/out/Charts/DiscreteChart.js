"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Google_1 = require('../lib/Google');
var DiscreteChart = (function (_super) {
    __extends(DiscreteChart, _super);
    function DiscreteChart(props) {
        _super.call(this, props);
        this.state = {};
        this.chartsLoaded = false;
    }
    DiscreteChart.prototype.componentDidMount = function () {
        Google_1.loadCharts().then(this.onGoogleLoad.bind(this));
    };
    DiscreteChart.prototype.onGoogleLoad = function () {
        this.chartsLoaded = true;
        this.initChart();
        this.showChart(this.props.analyticsData);
    };
    DiscreteChart.prototype.initChart = function () {
        var gConstrutorFunctionName = Google_1.googleRef.visualization[this.props.gConstrutorFunctionName];
        this.chart = new gConstrutorFunctionName(document.getElementById('chart_div'));
    };
    DiscreteChart.prototype.componentWillReceiveProps = function (nextProps) {
        if (!this.chartsLoaded)
            return;
        this.showChart(nextProps.analyticsData);
    };
    DiscreteChart.prototype.showChart = function (analyticsCount) {
        var dataTable = new Google_1.googleRef.visualization.DataTable();
        dataTable.addColumn('string', 'Item');
        dataTable.addColumn('number', 'quantidade');
        var arrayData = analyticsCount.items.map(function (item) {
            var label = item.label ? item.label.toString() : null;
            return [label, item.value];
        });
        if (arrayData.length == 0) {
            arrayData.push(['vazio', 0]);
        }
        else if (analyticsCount.othersValue > 0.1) {
            arrayData.push(['Outros', analyticsCount.othersValue]);
        }
        dataTable.addRows(arrayData);
        this.chart.draw(dataTable, this.props.chartOptions);
    };
    DiscreteChart.prototype.render = function () {
        return React.createElement("div", {id: "chart_div"});
    };
    return DiscreteChart;
}(React.Component));
exports.DiscreteChart = DiscreteChart;
