"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Google_1 = require('../lib/Google');
var LineChart = (function (_super) {
    __extends(LineChart, _super);
    function LineChart(props) {
        _super.call(this, props);
        this.state = {};
        this.chartsLoaded = false;
    }
    LineChart.prototype.componentDidMount = function () {
        Google_1.loadCharts().then(this.onGoogleLoad.bind(this));
    };
    LineChart.prototype.onGoogleLoad = function () {
        this.chartsLoaded = true;
        this.initChart();
        this.showChart(this.props.analyticsData);
    };
    LineChart.prototype.initChart = function () {
        this.chart = new Google_1.googleRef.visualization.LineChart(document.getElementById('chart_div'));
    };
    LineChart.prototype.componentWillReceiveProps = function (nextProps) {
        if (!this.chartsLoaded)
            return;
        this.showChart(nextProps.analyticsData);
    };
    LineChart.prototype.showChart = function (analyticsCount) {
        var dataTable = new Google_1.googleRef.visualization.DataTable();
        dataTable.addColumn('string', 'Item');
        dataTable.addColumn('number', 'quantidade');
        var arrayData = analyticsCount.items.map(function (item) {
            return [item.label.toString(), item.value];
        });
        if (arrayData.length == 0) {
            arrayData.push(['vazio', 0]);
        }
        dataTable.addRows(arrayData);
        var options = {
            vAxis: {
                title: 'Título'
            },
        };
        this.chart.draw(dataTable, options);
    };
    LineChart.prototype.render = function () {
        return React.createElement("div", {id: "chart_div"});
    };
    return LineChart;
}(React.Component));
exports.LineChart = LineChart;
