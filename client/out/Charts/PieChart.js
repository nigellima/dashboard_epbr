"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var DiscreteChart_1 = require('./DiscreteChart');
var Google_1 = require('../lib/Google');
var PieChart = (function (_super) {
    __extends(PieChart, _super);
    function PieChart() {
        _super.apply(this, arguments);
    }
    PieChart.prototype.render = function () {
        if (!Google_1.googleRef.visualization)
            return React.createElement("div", null);
        var chartOptions = {
            chartArea: { width: '50%' },
            height: 500,
            pieSliceText: 'value-and-percentage'
        };
        return (React.createElement(DiscreteChart_1.DiscreteChart, {analyticsData: this.props.analyticsData, gConstrutorFunctionName: 'PieChart', chartOptions: chartOptions}));
    };
    return PieChart;
}(React.Component));
exports.PieChart = PieChart;
