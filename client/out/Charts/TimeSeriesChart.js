"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var Google_1 = require('../lib/Google');
var TimeSeriesChart = (function (_super) {
    __extends(TimeSeriesChart, _super);
    function TimeSeriesChart(props) {
        _super.call(this, props);
        this.state = {};
        this.chartsLoaded = false;
    }
    TimeSeriesChart.prototype.componentDidMount = function () {
        Google_1.loadCharts().then(this.onGoogleLoad.bind(this));
    };
    TimeSeriesChart.prototype.onGoogleLoad = function () {
        this.chartsLoaded = true;
        this.initChart();
        this.getChartData(this.props);
    };
    TimeSeriesChart.prototype.initChart = function () {
        this.chart = new Google_1.googleRef.visualization.LineChart(document.getElementById('ts_chart_div'));
    };
    TimeSeriesChart.prototype.getChartData = function (props) {
        var query = {
            queryName: props.queryName,
            queryParams: props.qParams
        };
        server.getTimeSeries(query)
            .then(this.showChart.bind(this))
            .catch(showError.show);
    };
    TimeSeriesChart.prototype.showChart = function (records) {
        var _this = this;
        if (!records)
            return;
        var rawData = records.records;
        if (rawData.length == 0)
            return;
        var data = rawData.map(function (item) {
            var dateStr = item[_this.props.chartParams.xAxis];
            var dateParts = dateStr.split('/');
            var date = new Date(dateParts[1], dateParts[0]);
            var result = [date];
            for (var _i = 0, _a = _this.props.chartParams.seriesList; _i < _a.length; _i++) {
                var serie = _a[_i];
                result.push(item[serie.fieldName]);
            }
            return result;
        });
        var dataTable = new Google_1.googleRef.visualization.DataTable();
        dataTable.addColumn('date', 'Datas');
        for (var _i = 0, _a = this.props.chartParams.seriesList; _i < _a.length; _i++) {
            var serie = _a[_i];
            dataTable.addColumn('number', serie.label);
        }
        dataTable.addRows(data);
        var options = {
            vAxis: {
                title: this.props.chartParams.yLabel
            },
        };
        this.chart.draw(dataTable, options);
    };
    TimeSeriesChart.prototype.render = function () {
        return React.createElement("div", {id: "ts_chart_div"});
    };
    return TimeSeriesChart;
}(React.Component));
exports.TimeSeriesChart = TimeSeriesChart;
