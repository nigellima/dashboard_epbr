"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var react_router_1 = require('react-router');
var Server_1 = require('../lib/Server');
var Card = (function (_super) {
    __extends(Card, _super);
    function Card(props) {
        _super.call(this, props);
        this.state = {};
    }
    Card.prototype.render = function () {
        var data = this.props.data;
        var url = Server_1.paths.baseImg + 'Company/img_' + data.company_id + '_original.jpg';
        return (React.createElement("div", {"data-card": "operador", className: "col-xs-18 col-sm-4 col-md-3 padding-tb-15"}, React.createElement("div", {className: "card-body"}, React.createElement("h2", null, data.company_name), React.createElement("img", {src: url, style: { width: 300, height: 180 }}), React.createElement("ul", null, React.createElement("li", null, React.createElement("div", {className: "col-no-padding"}, React.createElement(react_router_1.Link, {to: "/app/view_record?source=Company&id=" + data.company_id}, "Detalhes da empresa")))))));
    };
    return Card;
}(React.Component));
exports.Card = Card;
