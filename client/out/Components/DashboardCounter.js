"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var DashboardCounter = (function (_super) {
    __extends(DashboardCounter, _super);
    function DashboardCounter() {
        _super.apply(this, arguments);
    }
    DashboardCounter.prototype.componentDidUpdate = function () {
        var countRef = this.refs['countRef'];
        var this_ = $(countRef);
        this_.prop('Counter', 0).animate({
            Counter: this_.text()
        }, {
            duration: 2000,
            easing: 'swing',
            step: function (now) {
                this_.text(Math.ceil(now));
            }
        });
    };
    DashboardCounter.prototype.render = function () {
        return React.createElement("span", {className: "count", ref: "countRef"}, this.props.count);
    };
    return DashboardCounter;
}(React.Component));
exports.DashboardCounter = DashboardCounter;
