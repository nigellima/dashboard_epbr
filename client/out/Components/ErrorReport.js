"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Modal = require('react-modal');
var Server_1 = require('../lib/Server');
var showError = require('../lib/ShowError');
var Flash = require('./Flash');
var ErrorReport = (function (_super) {
    __extends(ErrorReport, _super);
    function ErrorReport(props) {
        _super.call(this, props);
        this.state = {
            modalIsOpen: false,
            description: ''
        };
    }
    ErrorReport.prototype.closeModal = function () {
        this.state.modalIsOpen = false;
        this.setState(this.state);
    };
    ErrorReport.prototype.openModal = function () {
        this.state.modalIsOpen = true;
        this.setState(this.state);
    };
    ErrorReport.prototype.sendReport = function () {
        var report = {
            url: this.props.url,
            description: this.state.description
        };
        Server_1.postP('/send_error_report', report)
            .then(this.onReportSent.bind(this))
            .catch(showError.show);
        this.state.modalIsOpen = false;
        this.setState(this.state);
    };
    ErrorReport.prototype.onReportSent = function (res) {
        Flash.create('success', 'Erro enviado com sucesso');
    };
    ErrorReport.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", null, React.createElement(Modal, {className: "Modal__Bootstrap modal-dialog", closeTimeoutMS: 150, isOpen: this.state.modalIsOpen}, React.createElement("div", {className: "modal-content"}, React.createElement("div", {className: "modal-header"}, React.createElement("button", {type: "button", className: "close", onClick: this.closeModal.bind(this)}, React.createElement("span", {"aria-hidden": "true"}, "× "), React.createElement("span", {className: "sr-only"}, "Close")), React.createElement("h4", {className: "modal-title"}, "Reportar erro")), React.createElement("div", {className: "modal-body"}, React.createElement("h4", null, "Objeto: ", React.createElement("b", null, this.props.objectLabel)), React.createElement("hr", null), "Descrição:", React.createElement("textarea", {type: "text", rows: 8, className: "form-control", onChange: function (e) { _this.state.description = e.target.value; }})), React.createElement("div", {className: "modal-footer"}, React.createElement("button", {type: "button", className: "btn btn-default", onClick: this.closeModal.bind(this)}, "Fechar"), React.createElement("button", {type: "button", className: "btn btn-primary", onClick: this.sendReport.bind(this)}, "Enviar"))))));
    };
    return ErrorReport;
}(React.Component));
exports.ErrorReport = ErrorReport;
