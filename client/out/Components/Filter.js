"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../lib/Server');
var showError = require('../lib/ShowError');
var Multiselect = require('react-bootstrap-multiselect');
var Filter = (function (_super) {
    __extends(Filter, _super);
    function Filter(props) {
        _super.call(this, props);
        this.state = {
            data: [],
            selectedObjs: {}
        };
    }
    Filter.prototype.componentDidMount = function () {
        this.getFilterSourceData(this.props);
    };
    Filter.prototype.componentWillReceiveProps = function (nextProps) {
        if (this.props.queryName != nextProps.queryName) {
            this.getFilterSourceData(nextProps);
        }
    };
    Filter.prototype.getFilterSourceData = function (props) {
        this.state.selectedObjs = {};
        var req = {
            queryName: props.queryName,
            fieldName: props.fieldName
        };
        console.log(props);
        Server_1.getP('/filter_source', req)
            .then(this.onFilterSourceData.bind(this))
            .catch(showError.show);
    };
    Filter.prototype.onFilterSourceData = function (res) {
        this.state.data = res.values;
        console.log(this.props.queryName, this.props.fieldName, res.values);
        for (var _i = 0, _a = res.values; _i < _a.length; _i++) {
            var value = _a[_i];
            this.state.selectedObjs[value.value] = true;
        }
        this.setState(this.state);
    };
    Filter.prototype.genFilter = function () {
        var selectedValues = [];
        var deselectedValues = [];
        for (var key in this.state.selectedObjs) {
            var formattedKey = '"' + key + '"';
            if (this.state.selectedObjs[key]) {
                selectedValues.push(formattedKey);
            }
            else {
                deselectedValues.push(formattedKey);
            }
        }
        if (selectedValues.length < deselectedValues.length) {
            return {
                field: this.props.fieldName,
                in: selectedValues
            };
        }
        else {
            return {
                field: this.props.fieldName,
                notIn: deselectedValues
            };
        }
    };
    Filter.prototype.onChange = function (option, checked) {
        var rawValueStr = $(option).val();
        var index = rawValueStr.lastIndexOf('(');
        var valueStr = rawValueStr.substring(0, index - 1);
        this.state.selectedObjs[valueStr] = checked;
        this.props.onFilterChange(this.genFilter());
    };
    Filter.prototype.onSelectAll = function () {
        for (var key in this.state.selectedObjs) {
            this.state.selectedObjs[key] = true;
        }
        this.props.onFilterChange(null);
    };
    Filter.prototype.onDeselectAll = function () {
        for (var key in this.state.selectedObjs) {
            this.state.selectedObjs[key] = false;
        }
        this.props.onFilterChange({
            field: this.props.fieldName,
            equal: '"***"'
        });
    };
    Filter.prototype.render = function () {
        var _this = this;
        var data = this.state.data.map(function (v) {
            return {
                value: v.value + ' (' + v.qtt + ')',
                selected: _this.state.selectedObjs[v.value]
            };
        });
        return (React.createElement("div", null));
    };
    return Filter;
}(React.Component));
exports.Filter = Filter;
