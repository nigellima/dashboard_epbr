"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Filter = require('../Components/Filter');
var FiltersGroup = (function (_super) {
    __extends(FiltersGroup, _super);
    function FiltersGroup(props) {
        _super.call(this, props);
        this.state = {
            filterParams: []
        };
        this.filters = {};
    }
    FiltersGroup.prototype.componentDidMount = function () {
        this.genFilterParams(this.props);
        this.setState(this.state);
    };
    FiltersGroup.prototype.componentWillReceiveProps = function (nextProps) {
        this.genFilterParams(nextProps);
        if (nextProps.tableParams.source != this.props.tableParams.source) {
            this.filters = {};
        }
        this.setState(this.state);
    };
    FiltersGroup.prototype.genFilterParams = function (props) {
        this.state.filterParams = [];
        for (var _i = 0, _a = props.tableParams.fields; _i < _a.length; _i++) {
            var field = _a[_i];
            if (!field.hasFilter)
                continue;
            var filter = {
                label: field.label,
                fieldName: field.fieldName,
                queryName: props.tableParams.source
            };
            if (!field.fieldName) {
                filter.fieldName = field.ref.valueField;
            }
            this.state.filterParams.push(filter);
        }
    };
    FiltersGroup.prototype.getFiltersArray = function () {
        var result = [];
        for (var fieldName in this.filters) {
            var filter = this.filters[fieldName];
            if (!filter)
                continue;
            result.push(filter);
        }
        return result;
    };
    FiltersGroup.prototype.onFilterChange = function (fieldName, filter) {
        this.filters[fieldName] = filter;
        var filterArray = this.getFiltersArray();
        this.props.onChange(filterArray);
    };
    FiltersGroup.prototype.render = function () {
        var _this = this;
        var filtersHTML = this.state.filterParams.map(function (f, i) {
            return (React.createElement("div", {className: "col-md-2", key: 'f' + i}, React.createElement(Filter.Filter, {queryName: f.queryName, fieldName: f.fieldName, label: f.label, onFilterChange: _this.onFilterChange.bind(_this, f.fieldName)})));
        });
        return (React.createElement("div", {className: "row"}, filtersHTML));
    };
    return FiltersGroup;
}(React.Component));
exports.FiltersGroup = FiltersGroup;
