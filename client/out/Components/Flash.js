"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var self = null;
function create(className, message) {
    self.setMessage(className, message);
}
exports.create = create;
var Flash = (function (_super) {
    __extends(Flash, _super);
    function Flash(props) {
        _super.call(this, props);
        this.state = {
            message: '',
            className: 'success'
        };
        self = this;
    }
    Flash.prototype.setMessage = function (className, message) {
        this.setState({ className: className, message: message });
        setTimeout(this.cleanMessage.bind(this), this.props.timeout);
    };
    Flash.prototype.cleanMessage = function () {
        this.setState({ message: '', className: '' });
    };
    Flash.prototype.render = function () {
        var message = this.state.message;
        var className = "alert alert-" + this.state.className;
        var result = null;
        if (message && message != '') {
            result = React.createElement("div", {className: className, role: "alert", dangerouslySetInnerHTML: { __html: message }});
        }
        return result;
    };
    return Flash;
}(React.Component));
exports.Flash = Flash;
