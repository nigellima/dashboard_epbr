"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var react_router_1 = require('react-router');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var FIRST_OFFSET = 120;
var TIME_INTERVAL = 40;
var DX_ON_EACH_EVENT = 2;
var NewsTicker = (function (_super) {
    __extends(NewsTicker, _super);
    function NewsTicker(props) {
        _super.call(this, props);
        this.state = {
            offset: FIRST_OFFSET,
            items: [],
            tickerWidth: -1
        };
    }
    NewsTicker.prototype.componentDidMount = function () {
        var req = {};
        server.getP('/ticker_updates', req)
            .then(this.onData.bind(this))
            .catch(showError.show);
    };
    NewsTicker.prototype.onData = function (res) {
        this.state.items = res.items;
        setInterval(this.updateTicker.bind(this), TIME_INTERVAL);
        this.calculateTotalWidth();
    };
    NewsTicker.prototype.calculateTotalWidth = function () {
        this.setState(this.state);
        var tiContentDiv = this.refs['tiContentRef'];
        this.state.tickerWidth = 0;
        for (var _i = 0, _a = tiContentDiv.children; _i < _a.length; _i++) {
            var child = _a[_i];
            this.state.tickerWidth += child.clientWidth;
        }
    };
    NewsTicker.prototype.updateTicker = function () {
        this.state.offset -= DX_ON_EACH_EVENT;
        if (this.state.offset < -(this.state.tickerWidth - FIRST_OFFSET)) {
            this.state.offset = FIRST_OFFSET;
        }
        this.setState(this.state);
    };
    NewsTicker.prototype.render = function () {
        var content = this.state.items.map(function (item, index) {
            var className = item.category == 'Insight' ? 'data' : 'all';
            return (React.createElement("div", {className: "ti_news", key: index}, React.createElement(react_router_1.Link, {to: item.link, className: className}, React.createElement("span", null, item.category), " ", item.title)));
        });
        var style = {
            width: this.state.tickerWidth,
            marginLeft: this.state.offset
        };
        return (React.createElement("div", {className: "hotnews"}, React.createElement("span", {className: "blockname"}, "News"), React.createElement("div", {className: "TickerNews default_theme", id: "T2"}, React.createElement("div", {className: "ti_wrapper"}, React.createElement("div", {className: "ti_slide"}, React.createElement("div", {className: "ti_content", ref: "tiContentRef", style: style}, content), React.createElement("div", {className: "ti_content ti_clone", style: { marginLeft: 0 }}, content))))));
    };
    return NewsTicker;
}(React.Component));
exports.NewsTicker = NewsTicker;
