"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var StringUtils = require('../lib/StringUtils');
var react_router_1 = require('react-router');
var DateUtils = require('../lib/DateUtils');
var ObjectNews = (function (_super) {
    __extends(ObjectNews, _super);
    function ObjectNews(props) {
        _super.call(this, props);
        this.state = { newsData: [] };
        this.getNews(this.props);
    }
    ObjectNews.prototype.getNews = function (props) {
        var relateNewsQuery = {
            queryName: 'NewsByPU',
            title: 'Related News',
            filters: {
                modelName: props.modelName,
                id: props.objId
            }
        };
        server.getQueryData(relateNewsQuery)
            .then(this.onNews.bind(this))
            .catch(function (err) { showError.show; });
    };
    ObjectNews.prototype.onNews = function (newsData) {
        this.state.newsData = newsData.records;
        this.setState(this.state);
    };
    ObjectNews.prototype.getFirstParagraph = function (newsContent) {
        newsContent = newsContent.split('<div>')[1];
        newsContent = newsContent.split('</div>')[0];
        return newsContent;
    };
    ObjectNews.prototype.componentWillReceiveProps = function (props) {
        this.getNews(props);
    };
    ObjectNews.prototype.render = function () {
        var _this = this;
        var newsItems = this.state.newsData.map(function (newsItem, index) {
            var url = StringUtils.format("/app/view_new?id={0}", newsItem.id);
            return React.createElement("li", {key: index}, React.createElement(react_router_1.Link, {to: url}, React.createElement("h3", null, newsItem.title)), React.createElement("div", {className: "col-md-12 col-no-padding"}, React.createElement("div", {className: "col-md-6 col-no-padding"}, React.createElement("div", {className: "item-related"}, "Posted:", React.createElement("span", null, DateUtils.dateFormat(newsItem.created_at))), React.createElement("br", null), React.createElement("div", {className: "item-related"}, React.createElement("div", {className: "content", dangerouslySetInnerHTML: { __html: _this.getFirstParagraph(newsItem.content) }})))));
        });
        var result = null;
        if (newsItems.length > 0) {
            result = (React.createElement("div", {className: "main-news"}, React.createElement("div", {className: "page-header"}, React.createElement("h2", null, "Related News")), React.createElement("div", {id: "carousel"}), React.createElement("div", {className: "row"}, React.createElement("div", {className: "carousel slide fade-quote-carousel"}, React.createElement("ol", {className: "carousel-indicators"}, React.createElement("li", {"data-target": ".fade-quote-carousel", "data-slide-to": "0", className: "active"}), React.createElement("li", {"data-target": ".fade-quote-carousel", "data-slide-to": "1"}), React.createElement("li", {"data-target": ".fade-quote-carousel", "data-slide-to": "2"})), React.createElement("div", {className: "carousel-inner"}, React.createElement("div", {className: "active item"}, React.createElement("ul", null, newsItems)))))));
        }
        return result;
    };
    return ObjectNews;
}(React.Component));
exports.ObjectNews = ObjectNews;
