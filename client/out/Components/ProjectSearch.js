"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var showError = require('../lib/ShowError');
var server = require('../lib/Server');
var ProjectSearch = (function (_super) {
    __extends(ProjectSearch, _super);
    function ProjectSearch(props) {
        _super.call(this, props);
        this.state = {
            value: '',
            suggestions: []
        };
    }
    ProjectSearch.prototype.onSuggestionsFetchRequested = function (_a) {
        var value = _a.value;
        var req = {
            searchValue: value,
            countLimit: 5
        };
        server.getP('/search', req)
            .then(this.onServerSearchResult.bind(this))
            .catch(showError.show);
    };
    ProjectSearch.prototype.onServerSearchResult = function (res) {
        this.state.suggestions = res.values;
        this.setState(this.state);
    };
    ProjectSearch.prototype.onUserTypeChar = function (event, _a) {
        var newValue = _a.newValue, method = _a.method;
        this.state.value = newValue;
        this.setState(this.state);
    };
    ProjectSearch.prototype.onSuggestionSelected = function (event, _a) {
        var suggestion = _a.suggestion, suggestionValue = _a.suggestionValue, sectionIndex = _a.sectionIndex, method = _a.method;
        this.props.onItemSelected(suggestion);
        this.state.value = '';
        this.setState(this.state);
    };
    ProjectSearch.prototype.getSuggestionValue = function (suggestion) {
        return suggestion.name;
    };
    ProjectSearch.prototype.renderSuggestion = function (suggestion) {
        return React.createElement("span", null, suggestion.modelLabel + ': ' + suggestion.name);
    };
    ProjectSearch.prototype.onFormSubmit = function (event) {
        event.preventDefault();
        if (this.props.onSearchTyped && this.state.value != '') {
            this.props.onSearchTyped(this.state.value);
            this.state.value = '';
            this.setState(this.state);
        }
    };
    ProjectSearch.prototype.render = function () {
        var inputProps = {
            placeholder: 'Buscar...',
            value: this.state.value,
            onChange: this.onUserTypeChar.bind(this)
        };
        return (React.createElement("form", {onSubmit: this.onFormSubmit.bind(this)}));
    };
    return ProjectSearch;
}(React.Component));
exports.ProjectSearch = ProjectSearch;
