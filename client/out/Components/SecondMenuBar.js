"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var react_router_1 = require('react-router');
var SecondMenuBar = (function (_super) {
    __extends(SecondMenuBar, _super);
    function SecondMenuBar(props) {
        _super.call(this, props);
        this.state = {};
    }
    SecondMenuBar.prototype.render = function () {
        var adminLink = React.createElement("li", {className: "nav-item"}, React.createElement(react_router_1.Link, {to: "/app/admin"}, React.createElement("b", null, "Admin")));
        var analyticsLink = React.createElement("li", {className: "nav-item"}, React.createElement(react_router_1.Link, {to: "/app/charts"}, React.createElement("b", null, "Gráficos")));
        var logo = (React.createElement("div", {className: "col-md-3"}, React.createElement("div", {className: "navbar-header navbar-brand-container"}, React.createElement("button", {type: "button", className: "navbar-toggle", "data-toggle": "collapse", "data-target": "#bs-example-navbar-collapse-1"}, React.createElement("span", {className: "sr-only"}, "Toggle navigation"), React.createElement("span", {className: "icon-bar"}), React.createElement("span", {className: "icon-bar"}), React.createElement("span", {className: "icon-bar"})), React.createElement("a", {className: "navbar-brand logo-topo", href: "/app/", id: "homeLink"}))));
        var navBarItems = (React.createElement("div", {className: "col-no-padding col-md-9"}, React.createElement("div", {className: "collapse navbar-collapse", id: "bs-example-navbar-collapse-1"}, React.createElement("ul", {className: "nav navbar-nav navbar-nav-full"}, React.createElement("li", {className: "nav-item dropdown"}, React.createElement("a", {"aria-haspopup": "true", "aria-expanded": "false"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=FPSOs"}, "FPSOs"))), React.createElement("li", {className: "nav-item dropdown"}, React.createElement("a", {"aria-haspopup": "true", "aria-expanded": "false"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Bids"}, "BIDs"))), React.createElement("li", {className: "nav-item"}, React.createElement(react_router_1.Link, {to: "/app/maps_all"}, "Map")), this.props.isAdmin ? adminLink : ''))));
        var navBar = (React.createElement("nav", {className: "navbar navbar-default navbar-border-none", role: "navigation"}, React.createElement("div", {className: "container-fluid padding-lr-20"}, React.createElement("div", {className: "row"}, logo, navBarItems))));
        return navBar;
    };
    return SecondMenuBar;
}(React.Component));
exports.SecondMenuBar = SecondMenuBar;
