"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var showError = require('../lib/ShowError');
var ModelViewService = require('../lib/ModelViewUtils');
var server = require('../lib/Server');
var StringUtils = require('../lib/StringUtils');
var react_router_1 = require('react-router');
var ShowQueryData = (function (_super) {
    __extends(ShowQueryData, _super);
    function ShowQueryData(props) {
        _super.call(this, props);
        this.state = {
            header: [],
            records: [],
            title: this.props.model.title
        };
    }
    ShowQueryData.prototype.componentDidMount = function () {
        this.getQueryData(this.props);
    };
    ShowQueryData.prototype.componentWillReceiveProps = function (nextProps) {
        this.getQueryData(nextProps);
    };
    ShowQueryData.prototype.getQueryData = function (props) {
        var filters = props.model.filters;
        if (!filters) {
            filters = {};
            filters.id = props.objId;
        }
        var options = {
            filters: filters,
            queryName: props.model.queryName
        };
        server.getQueryData(options)
            .then(this.onData.bind(this))
            .catch(showError.show);
    };
    ShowQueryData.prototype.onData = function (data) {
        var records = data.records;
        var fields = data.fields;
        var header = [];
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            header.push(field.label);
        }
        this.state.header = header;
        var showRecords = [];
        for (var i = 0; i < records.length; i++) {
            var recordValues = [];
            var record = records[i];
            for (var j = 0; j < fields.length; j++) {
                var item = {};
                var field = fields[j];
                if (field.ref) {
                    item.model = record[field.ref.modelField];
                    item.id = record[field.ref.idField];
                    item.value = record[field.ref.valueField];
                }
                else {
                    var recordValue = record[field.fieldName];
                    var fn = ModelViewService.formatFnByType(field);
                    recordValue = fn(recordValue);
                    if (field.type == 'CURRENCY') {
                        item.rightAlign = true;
                    }
                    item.value = recordValue;
                }
                recordValues.push(item);
            }
            showRecords.push(recordValues);
        }
        this.state.records = showRecords;
        this.setState(this.state);
        return null;
    };
    ShowQueryData.prototype.render = function () {
        if (this.state.records.length == 0) {
            return null;
        }
        var headerHtml = this.state.header.map(function (headerItem) {
            return React.createElement("th", {key: headerItem}, headerItem);
        });
        var rowsHtml = this.state.records.map(function (record, i) {
            var recordColumns = record.map(function (recordColumn, j) {
                var valueHtml = null;
                if (recordColumn.model) {
                    var url = StringUtils.format("/app/view_record?source={0}&id={1}", recordColumn.model, recordColumn.id);
                    valueHtml = React.createElement(react_router_1.Link, {to: url}, recordColumn.value);
                }
                else {
                    valueHtml = React.createElement("div", {className: "{'text-right': record_column.rightAlign}"}, recordColumn.value);
                }
                return React.createElement("td", {key: j}, " ", valueHtml, " ");
            });
            return React.createElement("tr", {key: i}, recordColumns);
        });
        var table = React.createElement("table", {className: "borderless table table-hover", "data-height": "500"}, React.createElement("thead", null, React.createElement("tr", null, headerHtml)), React.createElement("tbody", null, rowsHtml));
        return (React.createElement("div", null, React.createElement("h4", null, this.state.title), React.createElement("div", {className: "main-table"}, React.createElement("div", {className: "bootstrap-table fixed-table-container"}, React.createElement("div", {className: "fixed-table-body"}, table, " "))), React.createElement("hr", null)));
    };
    return ShowQueryData;
}(React.Component));
exports.ShowQueryData = ShowQueryData;
