"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Tableau = (function (_super) {
    __extends(Tableau, _super);
    function Tableau(props) {
        _super.call(this, props);
        this.state = {};
        this.viz = null;
    }
    Tableau.prototype.componentDidMount = function () {
        this.initViz();
    };
    Tableau.prototype.componentDidUpdate = function () {
        this.viz.dispose();
        this.initViz();
    };
    Tableau.prototype.shouldComponentUpdate = function (nextProps) {
        return this.props.vizUrl != nextProps.vizUrl;
    };
    Tableau.prototype.initViz = function () {
        var options = { hideTabs: true };
        var vizDiv = this.refs['countRef'];
        this.viz = new tableau.Viz(vizDiv, this.props.vizUrl, options);
    };
    Tableau.prototype.componentWillUnmount = function () {
        this.viz.dispose();
    };
    Tableau.prototype.render = function () {
        var style = {
            height: '100%',
            width: '100%',
            overflow: 'scroll',
            align: 'center'
        };
        return (React.createElement("div", {id: "vizContainer", ref: "countRef", style: style}));
    };
    return Tableau;
}(React.Component));
exports.Tableau = Tableau;
