"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var session = require('../lib/session');
var react_router_1 = require('react-router');
var ProjectSearch_1 = require('./ProjectSearch');
var TopMenu = (function (_super) {
    __extends(TopMenu, _super);
    function TopMenu() {
        _super.apply(this, arguments);
    }
    TopMenu.prototype.onProjectSelected = function (selectedItem) {
        var url = '/app/view_record?source=' + selectedItem.model + '&id=' + selectedItem.id;
        react_router_1.browserHistory.push(url);
    };
    ;
    TopMenu.prototype.onSearchTyped = function (typedText) {
        react_router_1.browserHistory.replace('/app/search_results' + '?search=' + typedText);
    };
    TopMenu.prototype.render = function () {
        var userNameMenuItem = (React.createElement("div", {className: "col-md-3 col-no-padding"}, React.createElement("div", {className: "user-name"}, "Hello,", React.createElement("span", null, " ", this.props.username, " | "), React.createElement("a", {href: "#", onClick: session.logout}, "logout"))));
        var myAccountMenuItem = (React.createElement("div", {className: "col-md-3 col-no-padding"}, React.createElement("div", {className: "top-menu-item"}, React.createElement("div", {className: "top-menu-icon user"}), React.createElement(react_router_1.Link, {to: "/app/change_password"}, "Account"))));
        var sacMenuItem = (React.createElement("div", {className: "col-md-3 col-no-padding"}, React.createElement("div", {className: "top-menu-item"}, React.createElement("div", {className: "top-menu-icon question"}), "SAC")));
        var search = (React.createElement("div", {className: "col-md-3 col-no-padding"}, React.createElement("div", {style: { padding: 10, width: 150 }}, React.createElement(ProjectSearch_1.ProjectSearch, {onItemSelected: this.onProjectSelected.bind(this), onSearchTyped: this.onSearchTyped.bind(this)}))));
        var menu = (React.createElement("div", {className: "top-menu"}, React.createElement("div", {className: "container-fluid"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-4 col-hidden"}), React.createElement("div", {className: "col-no-padding col-md-8"}, userNameMenuItem, myAccountMenuItem, search)))));
        return menu;
    };
    return TopMenu;
}(React.Component));
exports.TopMenu = TopMenu;
