"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../lib/Server');
var react_router_1 = require('react-router');
var FlexSlider = (function (_super) {
    __extends(FlexSlider, _super);
    function FlexSlider(props) {
        _super.call(this, props);
        this.state = {};
    }
    FlexSlider.prototype.componentDidMount = function () {
        this.initFlexSlider();
    };
    FlexSlider.prototype.componentDidUpdate = function () {
        this.initFlexSlider();
    };
    FlexSlider.prototype.initFlexSlider = function () {
        var flexSliderElement = $('.flexslider');
        flexSliderElement.flexslider({ animation: "fade" });
    };
    FlexSlider.prototype.render = function () {
        var fsItems = this.props.data.map(function (item, index) {
            var imgUrl = Server_1.paths.baseImg + item.imgUrl;
            return (React.createElement("li", {key: 'fs' + index}, React.createElement("a", {href: "#"}, React.createElement("img", {alt: "", src: imgUrl})), React.createElement("div", {className: "flex-caption"}, React.createElement("div", {className: "desc"}, React.createElement("h1", null, React.createElement(react_router_1.Link, {to: "/app/view_new?id=" + item.id}, item.title)), React.createElement("p", {dangerouslySetInnerHTML: { __html: item.content }})))));
        });
        return (React.createElement("div", {className: "flexslider mb25"}, React.createElement("ul", {className: "slides no-bullet inline-list m0"}, fsItems)));
    };
    return FlexSlider;
}(React.Component));
exports.FlexSlider = FlexSlider;
