"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var DateUtils_1 = require('../lib/DateUtils');
var FlexSlider_1 = require('./FlexSlider');
var Insights = (function (_super) {
    __extends(Insights, _super);
    function Insights(props) {
        _super.call(this, props);
        this.state = {
            insights: null
        };
    }
    Insights.prototype.componentDidMount = function () {
        this.getInsights();
        this.initFlexSlider();
    };
    Insights.prototype.componentDidUpdate = function () {
        this.initFlexSlider();
    };
    Insights.prototype.initFlexSlider = function () {
        var flexSliderElement = $('.flexslider');
        flexSliderElement.flexslider({ animation: "fade" });
    };
    Insights.prototype.componentWillReceiveProps = function (nextProps) {
        this.getInsights();
    };
    Insights.prototype.getInsights = function () {
        Server_1.getP('/insights', {})
            .then(this.onInsights.bind(this))
            .catch(showError.show);
    };
    Insights.prototype.onInsights = function (res) {
        this.state.insights = res;
        this.setState(this.state);
    };
    Insights.prototype.getOtherPosts = function (postsData) {
        var otherPostsItems = postsData.map(function (item, index) {
            var imgUrl = Server_1.paths.baseImg + item.imgUrl;
            return (React.createElement("li", {key: 'op' + index}, React.createElement("a", {href: "#"}, React.createElement("img", {src: imgUrl, alt: ""})), React.createElement("h3", {className: "post-title"}, React.createElement(react_router_1.Link, {to: "/app/view_new?id=" + item.id}, item.title)), React.createElement("span", {className: "date"}, React.createElement("a", {href: "#"}, DateUtils_1.dateFormatInsight(item.date)))));
        });
        return (React.createElement("div", {className: "other-posts"}, React.createElement("ul", {className: "no-bullet"}, otherPostsItems)));
    };
    Insights.prototype.getArticle = function (category, posts) {
        var otherPosts = this.getOtherPosts(posts.slice(1, posts.length));
        var firstPost = posts[0];
        var imgUrl = Server_1.paths.baseImg + firstPost.imgUrl;
        return (React.createElement("article", {className: "six column"}, React.createElement("h4", {className: "cat-title"}, React.createElement("a", {href: "#"}, category)), React.createElement("div", {className: "post-image"}, React.createElement("a", {href: "#"}, React.createElement("img", {src: imgUrl, alt: ""}))), React.createElement("div", {className: "post-container"}, React.createElement("h2", {className: "post-title"}, React.createElement(react_router_1.Link, {to: "/app/view_new?id=" + firstPost.id}, firstPost.title)), React.createElement("div", {className: "post-content"}, React.createElement("p", {dangerouslySetInnerHTML: { __html: firstPost.content }}))), React.createElement("div", {className: "post-meta"}, React.createElement("span", {className: "author"}, React.createElement("a", {href: "#"}, firstPost.author)), React.createElement("span", {className: "date"}, React.createElement("a", {href: "#"}, DateUtils_1.dateFormatInsight(firstPost.date)))), otherPosts));
    };
    Insights.prototype.getTab = function (id, tabPostsData) {
        var tabItems = tabPostsData.map(function (item, index) {
            var imgUrl = Server_1.paths.baseImg + item.imgUrl;
            return (React.createElement("li", {key: 'tab' + index}, React.createElement("a", {href: "#"}, React.createElement("img", {alt: "", src: imgUrl})), React.createElement("h3", null, React.createElement(react_router_1.Link, {to: "/app/view_new?id=" + item.id}, item.title)), React.createElement("div", {className: "post-date"}, DateUtils_1.dateFormatInsight(item.date))));
        });
        return (React.createElement("div", {id: id}, React.createElement("ul", null, tabItems)));
    };
    Insights.prototype.render = function () {
        var insights = this.state.insights;
        if (!insights) {
            return React.createElement("div", null);
        }
        var tags = null;
        var lastArticles = this.getArticle('Últimas', insights.section1Articles);
        var analysisArticles = this.getArticle('Análises', insights.section2Articles);
        var popularTab = this.getTab('popular-tab', insights.popular);
        var recentTab = this.getTab('recent-tab', insights.recent);
        var commentsTab = this.getTab('comments-tab', insights.recent);
        var sideBar = (React.createElement("aside", {id: "sidebar", className: "four column pull-right"}, React.createElement("ul", {className: "no-bullet"}, React.createElement("li", {className: "widget tabs-widget clearfix"}, React.createElement("ul", {className: "tab-links no-bullet clearfix"}, React.createElement("li", {className: "active"}, React.createElement("a", {href: "#recent-tab"}, "Recentes")), React.createElement("li", null, React.createElement("a", {href: "#popular-tab"}, "Populares"))), popularTab, recentTab, commentsTab), tags)));
        return (React.createElement("div", {className: "orbit"}, React.createElement("section", {className: "container row clearfix"}, React.createElement("h2", null, React.createElement("b", null, "Insights")), React.createElement("section", {className: "inner-container clearfix"}, React.createElement("section", {id: "content", className: "eight column row pull-left"}, React.createElement(FlexSlider_1.FlexSlider, {data: insights.flexSlider}), React.createElement("section", {className: "row"}, lastArticles, analysisArticles)), sideBar))));
    };
    return Insights;
}(React.Component));
exports.Insights = Insights;
