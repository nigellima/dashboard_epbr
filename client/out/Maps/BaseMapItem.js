"use strict";
var Flash = require('../Components/Flash');
var BaseMapItem = (function () {
    function BaseMapItem(mapObj) {
        this.mapObj = mapObj;
    }
    BaseMapItem.prototype.setGMapItem = function (gMapItem) {
        this.gMapItem = gMapItem;
    };
    BaseMapItem.prototype.setBillboardFn = function (billboardFn) {
        this.billboardFn = billboardFn;
        this.gMapItem.addListener('click', this.showBillboard.bind(this));
    };
    BaseMapItem.prototype.showBillboard = function (event) {
        this.billboardFn()
            .then(this.onBillboardContent.bind(this, event.latLng))
            .catch(function (error) { Flash.create('warning', error); });
    };
    BaseMapItem.prototype.onBillboardContent = function (latLng, contentString) {
        this.mapObj.infoWindow.setContent(contentString);
        this.mapObj.infoWindow.setPosition(latLng);
        this.mapObj.infoWindow.open(this.mapObj.gMap);
    };
    BaseMapItem.prototype.setVisibility = function (visible) {
        this.gMapItem.setMap(visible ? this.mapObj.gMap : null);
    };
    return BaseMapItem;
}());
exports.BaseMapItem = BaseMapItem;
