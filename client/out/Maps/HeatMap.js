"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseMapItem_1 = require('./BaseMapItem');
var Google_1 = require('../lib/Google');
var HeatMap = (function (_super) {
    __extends(HeatMap, _super);
    function HeatMap(mapObj, coordinates) {
        _super.call(this, mapObj);
        var points = coordinates.map(function (item) {
            return new Google_1.googleRef.maps.LatLng(item.coordinates.lat, item.coordinates.lng);
        });
        var gHeatMap = new Google_1.googleRef.maps.visualization.HeatmapLayer({
            data: points,
            map: this.mapObj.gMap,
            maxIntensity: 50,
        });
        this.setGMapItem(gHeatMap);
    }
    return HeatMap;
}(BaseMapItem_1.BaseMapItem));
exports.HeatMap = HeatMap;
