"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseMapItem_1 = require('./BaseMapItem');
var Google_1 = require('../lib/Google');
var KmlLayer = (function (_super) {
    __extends(KmlLayer, _super);
    function KmlLayer(mapObj, fileName) {
        _super.call(this, mapObj);
        var ctaLayer = new Google_1.googleRef.maps.KmlLayer({
            url: fileName,
            map: this.mapObj.gMap
        });
        this.setGMapItem(ctaLayer);
    }
    return KmlLayer;
}(BaseMapItem_1.BaseMapItem));
exports.KmlLayer = KmlLayer;
