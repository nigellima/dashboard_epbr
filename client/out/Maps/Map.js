"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Google_1 = require('../lib/Google');
var Tableau_1 = require('../Components/Tableau');
var Map = (function (_super) {
    __extends(Map, _super);
    function Map(props) {
        _super.call(this, props);
        this.state = {};
    }
    Map.prototype.componentDidMount = function () {
        this.initMap();
    };
    Map.prototype.initMap = function () {
        this.mapObj = {
            infoWindow: new Google_1.googleRef.maps.InfoWindow,
            gMap: new Google_1.googleRef.maps.Map(document.getElementById('map'), this.props.initialState)
        };
        this.props.receiveMapObj(this.mapObj);
    };
    Map.prototype.render = function () {
        var style = {
            width: 100 + '%',
            height: 700
        };
        return (React.createElement("div", null, React.createElement("div", null, React.createElement(Tableau_1.Tableau, {vizUrl: 'https://public.tableau.com/views/DashboardFPSOepbrmap/DashboardEPmap?:embed=y&:display_count=yes&publish=yes', style: style}), React.createElement("br", null))));
    };
    return Map;
}(React.Component));
exports.Map = Map;
