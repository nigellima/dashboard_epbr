"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseMapItem_1 = require('./BaseMapItem');
var Google_1 = require('../lib/Google');
var Marker = (function (_super) {
    __extends(Marker, _super);
    function Marker(mapObj, position, iconImage, title) {
        _super.call(this, mapObj);
        var gMarker = new Google_1.googleRef.maps.Marker({
            position: position,
            map: this.mapObj.gMap,
            title: title,
            icon: iconImage
        });
        this.setGMapItem(gMarker);
    }
    return Marker;
}(BaseMapItem_1.BaseMapItem));
exports.Marker = Marker;
