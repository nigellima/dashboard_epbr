"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseMapItem_1 = require('./BaseMapItem');
var Google_1 = require('../lib/Google');
var Polygon = (function (_super) {
    __extends(Polygon, _super);
    function Polygon(mapObj, title, options) {
        _super.call(this, mapObj);
        this.polygon = options.paths;
        var gPolygon = new Google_1.googleRef.maps.Polygon(options);
        gPolygon.setMap(this.mapObj.gMap);
        this.setGMapItem(gPolygon);
    }
    Polygon.prototype.getDimensions = function () {
        var minLat = 1000.0;
        var minLng = 1000.0;
        var maxLat = -1000.0;
        var maxLng = -1000.0;
        for (var _i = 0, _a = this.polygon; _i < _a.length; _i++) {
            var point = _a[_i];
            minLat = Math.min(minLat, point.lat);
            minLng = Math.min(minLng, point.lng);
            maxLat = Math.max(maxLat, point.lat);
            maxLng = Math.max(maxLng, point.lng);
        }
        return {
            width: maxLng - minLng,
            height: maxLat - minLat,
            center: {
                lat: (maxLat + minLat) / 2,
                lng: (maxLng + minLng) / 2
            }
        };
    };
    return Polygon;
}(BaseMapItem_1.BaseMapItem));
exports.Polygon = Polygon;
