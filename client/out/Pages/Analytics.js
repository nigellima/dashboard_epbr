"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Server_1 = require('../lib/Server');
var showError = require('../lib/ShowError');
var ArrayUtils = require('./../lib/ArrayUtils');
var FiltersGroup_1 = require('../Components/FiltersGroup');
var BarChart_1 = require('./../Charts/BarChart');
var PieChart_1 = require('./../Charts/PieChart');
var LineChart_1 = require('./../Charts/LineChart');
var chartTypes = [
    {
        name: 'bar',
        label: 'Barras'
    },
    {
        name: 'pie',
        label: 'Pizza'
    },
    {
        name: 'line',
        label: 'Linha'
    }
];
var Analytics = (function (_super) {
    __extends(Analytics, _super);
    function Analytics(props) {
        _super.call(this, props);
        this.state = {
            sources: [],
            selectedSourceName: null,
            groupField: null,
            valueField: null,
            result: {
                items: [],
                othersValue: 0
            },
            chartType: chartTypes[0].name,
            filters: [],
            tableParams: null,
        };
    }
    Analytics.prototype.componentDidMount = function () {
        var req = {};
        Server_1.getP('/analytics/sources', req)
            .then(this.onSources.bind(this))
            .catch(showError.show);
    };
    Analytics.prototype.onSources = function (res) {
        this.state.sources = res.sources;
        this.state.selectedSourceName = this.state.sources[0].sourceName;
        var selectedSource = this.getSelectedSource();
        this.state.groupField = selectedSource.groupFields[0].name;
        this.state.valueField = selectedSource.valueFields[0].name;
        this.setState(this.state);
        this.getResult();
        this.getParamsForFilters();
    };
    Analytics.prototype.getParamsForFilters = function () {
        var req = {
            queryName: this.state.selectedSourceName
        };
        Server_1.getP('/queries_fields', req)
            .then(this.onTableParamFieldsForFilters.bind(this))
            .catch(showError.show);
    };
    Analytics.prototype.onTableParamFieldsForFilters = function (res) {
        var fields = res.fields;
        this.state.filters = [];
        this.state.tableParams = {
            fields: fields,
            tableauUrl: res.tableauUrl,
            label: res.title,
            source: this.state.selectedSourceName
        };
        this.setState(this.state);
        return null;
    };
    Analytics.prototype.onSourceChange = function (event) {
        this.state.selectedSourceName = event.target.value;
        var selectedSource = this.getSelectedSource();
        if (!selectedSource) {
            return null;
        }
        this.state.groupField = selectedSource.groupFields[0].name;
        this.state.valueField = selectedSource.valueFields[0].name;
        this.state.filters = [];
        this.getResult();
        this.getParamsForFilters();
        this.setState(this.state);
    };
    Analytics.prototype.selectedFieldChanged = function (propName, event) {
        this.state[propName] = event.target.value;
        this.getResult();
    };
    Analytics.prototype.getResult = function () {
        var req = {
            source: this.state.selectedSourceName,
            groupField: this.state.groupField,
            valueField: this.state.valueField,
            maxNumItems: 10,
            filters: this.state.filters
        };
        Server_1.getP('/analytics/count_values', { data: JSON.stringify(req) })
            .then(this.onResult.bind(this))
            .catch(showError.show);
    };
    Analytics.prototype.onResult = function (res) {
        this.state.result = res.result;
        this.setState(this.state);
    };
    Analytics.prototype.onChartTypeChange = function (event) {
        this.state.chartType = event.target.value;
        this.setState(this.state);
    };
    Analytics.prototype.getSourcesCombo = function () {
        var sourcesOptions = this.state.sources.map(function (source, index) {
            return React.createElement("option", {value: source.sourceName, key: index}, source.label);
        });
        return (React.createElement("select", {className: "form-control", onChange: this.onSourceChange.bind(this)}, sourcesOptions));
    };
    Analytics.prototype.getSelectedSource = function () {
        var _this = this;
        return ArrayUtils.find(this.state.sources, function (val) {
            return val.sourceName == _this.state.selectedSourceName;
        });
    };
    Analytics.prototype.getFieldCombo = function (fields, propName) {
        var _this = this;
        var selectedSource = this.getSelectedSource();
        if (!selectedSource) {
            return null;
        }
        var options = fields.map(function (gfield, index) {
            return (React.createElement("option", {value: gfield.name, selected: gfield.name == _this.state[propName], key: index}, gfield.label));
        });
        return (React.createElement("select", {className: "form-control", onChange: this.selectedFieldChanged.bind(this, propName), defaultValue: this.state.selectedSourceName}, options));
    };
    Analytics.prototype.getGroupFieldsCombo = function () {
        var selectedSource = this.getSelectedSource();
        if (!selectedSource) {
            return null;
        }
        return this.getFieldCombo(selectedSource.groupFields, 'groupField');
    };
    Analytics.prototype.getValueFieldsCombo = function () {
        var selectedSource = this.getSelectedSource();
        if (!selectedSource) {
            return null;
        }
        return this.getFieldCombo(selectedSource.valueFields, 'valueField');
    };
    Analytics.prototype.getChartTypesCombo = function () {
        var _this = this;
        var sourcesOptions = chartTypes.map(function (type, index) {
            return (React.createElement("option", {value: type.name, key: index, selected: type.name == _this.state.chartType}, type.label));
        });
        return (React.createElement("select", {className: "form-control", onChange: this.onChartTypeChange.bind(this)}, sourcesOptions));
    };
    Analytics.prototype.getChart = function () {
        if (this.state.chartType == chartTypes[0].name) {
            return React.createElement(BarChart_1.BarChart, {analyticsData: this.state.result});
        }
        else if (this.state.chartType == chartTypes[1].name) {
            return React.createElement(PieChart_1.PieChart, {analyticsData: this.state.result});
        }
        else if (this.state.chartType == chartTypes[2].name) {
            return React.createElement(LineChart_1.LineChart, {analyticsData: this.state.result});
        }
    };
    Analytics.prototype.onFiltersChange = function (filters) {
        console.log('onFiltersChange', filters);
        this.state.filters = filters;
        this.getResult();
        this.setState(this.state);
    };
    Analytics.prototype.render = function () {
        var filtersGroupHTML = null;
        if (this.state.tableParams) {
            filtersGroupHTML = (React.createElement("div", {className: "panel panel-default"}, React.createElement("div", {className: "panel-body"}, React.createElement(FiltersGroup_1.FiltersGroup, {tableParams: this.state.tableParams, onChange: this.onFiltersChange.bind(this)}))));
        }
        return (React.createElement("div", null, filtersGroupHTML, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-lg-3 col-md-3"}, "Fonte: ", this.getSourcesCombo(), React.createElement("br", null), "Agrupar por: ", this.getGroupFieldsCombo(), React.createElement("br", null), "Propriedade: ", this.getValueFieldsCombo(), React.createElement("br", null), "Tipo: ", this.getChartTypesCombo()), React.createElement("div", {className: "col-lg-9 col-md-9"}, this.getChart()))));
    };
    return Analytics;
}(React.Component));
exports.Analytics = Analytics;
