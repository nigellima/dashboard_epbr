"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Flash = require('./../Components/Flash');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var ChangePassword = (function (_super) {
    __extends(ChangePassword, _super);
    function ChangePassword(props) {
        _super.call(this, props);
        this.state = {
            currentPassword: '',
            newPassword1: '',
            newPassword2: ''
        };
    }
    ChangePassword.prototype.changePassword = function () {
        var _a = this.state, currentPassword = _a.currentPassword, newPassword1 = _a.newPassword1, newPassword2 = _a.newPassword2;
        if (newPassword1 != newPassword2) {
            Flash.create('warning', 'As novas senhas diferem.');
            return;
        }
        server.changePassword(currentPassword, newPassword1)
            .then(this.onChangeSuccess.bind(this))
            .catch(showError.show);
    };
    ChangePassword.prototype.onChangeSuccess = function (result) {
        if (result.msg == 'OK') {
            Flash.create('success', 'A senha foi modificada com sucesso.');
            react_router_1.browserHistory.push('/app/');
        }
        else if (result.errorMsg) {
            Flash.create('danger', result.errorMsg);
        }
    };
    ChangePassword.prototype.render = function () {
        var _this = this;
        return (React.createElement("div", {className: "row"}, React.createElement("div", {className: "form-horizontal"}, React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", for: "current_password"}, "Senha atual:"), React.createElement("input", {className: "col-sm-10 form-control", type: "password", value: this.state.currentPassword, onChange: function (e) { _this.state.currentPassword = e.target.value; _this.setState(_this.state); }})), React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", for: "new_password1"}, "Nova senha:"), React.createElement("input", {className: "col-sm-10 form-control", type: "password", value: this.state.newPassword1, onChange: function (e) { _this.state.newPassword1 = e.target.value; _this.setState(_this.state); }})), React.createElement("div", {className: "form-group"}, React.createElement("label", {className: "control-label col-sm-2", for: "new_password2"}, "Repita a nova senha:"), React.createElement("input", {className: "col-sm-10 form-control", type: "password", value: this.state.newPassword2, onChange: function (e) { _this.state.newPassword2 = e.target.value; _this.setState(_this.state); }})), React.createElement("div", {className: "form-group"}, React.createElement("div", {className: "col-sm-offset-2 col-sm-10"}, React.createElement("button", {className: "btn btn-default", onClick: this.changePassword.bind(this)}, "Mudar senha"))))));
    };
    return ChangePassword;
}(React.Component));
exports.ChangePassword = ChangePassword;
