"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var DashboardCounter_1 = require('../Components/DashboardCounter');
var Dashboard = (function (_super) {
    __extends(Dashboard, _super);
    function Dashboard(props) {
        _super.call(this, props);
        this.state = {
            dashboardData: {
                numBids: 0,
                numContracts: 0,
                numPersons: 0,
                numProjects: 0
            }
        };
    }
    Dashboard.prototype.componentDidMount = function () {
        server.getP('/dashboard_data', {})
            .then(this.onDashboardData.bind(this))
            .catch(showError.show);
    };
    Dashboard.prototype.onDashboardData = function (res) {
        this.state.dashboardData = res;
        this.setState(this.state);
    };
    Dashboard.prototype.render = function () {
        var projects = (React.createElement("div", {className: " col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center"}, React.createElement("img", {src: "images/icon_fabric.png", alt: ""}), React.createElement("h3", null, React.createElement(DashboardCounter_1.DashboardCounter, {count: this.state.dashboardData.numProjects}), React.createElement("span", null, "PROJETOS")), React.createElement("h4", null, "Projetos ativos"), React.createElement(react_router_1.Link, {className: "button", to: "/app/paginated_table_view?source=Projects"}, "Explore")));
        var bids = (React.createElement("div", {className: " col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center"}, React.createElement("img", {src: "images/icon_dashboard_2.png", alt: ""}), React.createElement("h3", null, React.createElement(DashboardCounter_1.DashboardCounter, {count: this.state.dashboardData.numBids}), React.createElement("span", null, "SERVIÇOS")), React.createElement("h4", null, "Licitações"), React.createElement(react_router_1.Link, {className: "button", to: "/app/paginated_table_view?source=Bids"}, "Explore")));
        var contracts = (React.createElement("div", {className: " col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center"}, React.createElement("img", {src: "images/icon_dashboard_3.png", alt: ""}), React.createElement("h3", null, React.createElement(DashboardCounter_1.DashboardCounter, {count: this.state.dashboardData.numContracts}), React.createElement("span", null, "E&P")), React.createElement("h4", null, "Contratos"), React.createElement(react_router_1.Link, {className: "button", to: "/app/paginated_table_view?source=Contracts"}, "Explore")));
        var persons = (React.createElement("div", {className: " col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center"}, React.createElement("img", {src: "images/icon_dashboard_4.png", alt: ""}), React.createElement("h3", null, React.createElement(DashboardCounter_1.DashboardCounter, {count: this.state.dashboardData.numPersons}), React.createElement("span", null, "CONTATOS")), React.createElement("h4", null, "Pessoas"), React.createElement(react_router_1.Link, {className: "button", to: "/app/paginated_table_view?source=Persons"}, "Explore")));
        var overview = (React.createElement("div", {className: "row"}, projects, bids, contracts, persons));
        return (React.createElement("div", {className: "container"}, React.createElement("div", {className: "main-charts"}, React.createElement("div", {className: "page-header"}, React.createElement("h2", null, "Overview")), overview)));
    };
    return Dashboard;
}(React.Component));
exports.Dashboard = Dashboard;
