"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var react_router_1 = require('react-router');
var DataList = (function (_super) {
    __extends(DataList, _super);
    function DataList(props) {
        _super.call(this, props);
        this.state = {};
    }
    DataList.prototype.render = function () {
        var exploration = (React.createElement("div", {className: "col-md-6"}, React.createElement("span", {className: "col-md-2"}, "Exploração"), React.createElement("div", {className: "col-md-10"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Blocks", className: "button"}, "Blocos")), React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=DrillingRigs", className: "button"}, "Sondas"))), React.createElement("br", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Wells", className: "button"}, "Poços")), React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Seismics", className: "button"}, "Sísmica")))), React.createElement("hr", null)));
        var production = (React.createElement("div", {className: "col-md-6"}, React.createElement("span", {className: "col-md-2"}, "Produção"), React.createElement("div", {className: "col-md-10"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-4"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=FPSOs", className: "button"}, "FPSOs")), React.createElement("div", {className: "col-md-4"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=FixedProductionUnits", className: "button"}, "UPEs Fixas")), React.createElement("div", {className: "col-md-4"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=SemiSubmersibleProductionUnits", className: "button"}, "UPEs FPSOs Semi"))), React.createElement("br", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=oilFielsdProduction", className: "button"}, "Campos em Produção")), React.createElement("div", {className: "col-md-6"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=oilFieldsDevelopment", className: "button"}, "Campos em desenvolvimento")))), React.createElement("hr", null)));
        return (React.createElement("div", {className: "data-list"}, React.createElement("div", {className: "row"}, exploration, production), React.createElement("hr", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-1"}), React.createElement("div", {className: "col-md-5"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Bids", className: "button"}, "Licitações")), React.createElement("div", {className: "col-md-1"}), React.createElement("div", {className: "col-md-5"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Contracts", className: "button"}, "Contratos"))), React.createElement("hr", null), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-1"}), React.createElement("div", {className: "col-md-5"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Companies", className: "button"}, "Empresas")), React.createElement("div", {className: "col-md-1"}), React.createElement("div", {className: "col-md-5"}, React.createElement(react_router_1.Link, {to: "/app/paginated_table_view?source=Persons", className: "button"}, "Pessoas")))));
    };
    return DataList;
}(React.Component));
exports.DataList = DataList;
