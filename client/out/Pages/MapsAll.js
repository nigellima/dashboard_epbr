"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var Map_1 = require('../Maps/Map');
var MapUtils_1 = require('../lib/MapUtils');
var Google_1 = require('../lib/Google');
var Polygon = require('../Maps/Polygon');
var Marker_1 = require('../Maps/Marker');
var HeatMap_1 = require('../Maps/HeatMap');
var KmlLayer_1 = require('../Maps/KmlLayer');
var MapsAll = (function (_super) {
    __extends(MapsAll, _super);
    function MapsAll(props) {
        _super.call(this, props);
        this.state = {
            initialState: {
                zoom: 7,
                center: MapUtils_1.rioDeJaneiroCoords,
                mapTypeId: Google_1.googleRef.maps.MapTypeId.HYBRID
            }
        };
        this.blockMPolygons = [];
        this.blocksVisible = true;
        this.oilFieldMPolygons = [];
        this.oilFieldsVisible = true;
        this.productionUnitMMarkers = [];
        this.productionUnitsVisible = true;
        this.drillingRigMMarkers = [];
        this.drillingRigVisible = true;
        this.gasPipelinesVisible = false;
        this.wellHeatMapVisible = false;
    }
    MapsAll.prototype.componentDidMount = function () {
        var _this = this;
        server.getP('/maps/blocks', {})
            .then(function (res) { _this.addBlocksToMap(res.blocks); })
            .catch(showError.show);
        server.getP('/maps/oil_fields', {})
            .then(function (res) { _this.addOilFieldsToMap(res.oilFields); })
            .catch(showError.show);
        server.getP('/maps/production_units', {})
            .then(function (res) { _this.addProductionUnitsToMap(res.productionUnits); })
            .catch(showError.show);
        server.getP('/maps/drilling_rigs', {})
            .then(this.addDrillingRigsToMap.bind(this))
            .catch(showError.show);
        server.getP('/maps/wells', {})
            .then(this.addWellsToMap.bind(this))
            .catch(showError.show);
        this.gasPipeLayer = new KmlLayer_1.KmlLayer(this.mapObj, 'http://app.insideroil.com/maps/Gasodutos.kml');
        this.gasPipeLayer.setVisibility(false);
    };
    MapsAll.prototype.addBlocksToMap = function (blocks) {
        var _this = this;
        function blockBillboard(block) {
            var url = '/app/view_record?source=Block&id=' + block.id;
            return '<b>Bloco: </b><a href="' + url + '">' + block.name + '</a>';
        }
        this.blockMPolygons.length = 0;
        blocks.map(function (block) {
            var polygons = JSON.parse(block.polygons);
            if (!polygons) {
                return;
            }
            polygons.map(function (polygon) {
                var title = 'Bloco: ' + block.name;
                var options = {
                    paths: polygon,
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 3,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                };
                var mPolygon = new Polygon.Polygon(_this.mapObj, title, options);
                mPolygon.setBillboardFn(MapUtils_1.showBillboard.bind(_this, block, 'Block', blockBillboard));
                _this.blockMPolygons.push(mPolygon);
            });
        });
    };
    MapsAll.prototype.addOilFieldsToMap = function (oilFields) {
        var _this = this;
        function oilFieldBillboard(oilField) {
            var url = '/app/view_record?source=OilField&id=' + oilField.id;
            return '<b>Campo: </b><a href="' + url + '">' + oilField.name + '</a>';
        }
        this.oilFieldMPolygons.length = 0;
        oilFields.map(function (oilField) {
            var polygons = JSON.parse(oilField.polygons);
            if (!polygons) {
                return;
            }
            polygons.map(function (polygon) {
                var title = 'Campo: ' + oilField.name;
                var options = {
                    paths: polygon,
                    strokeColor: '#FFFF00',
                    strokeOpacity: 0.8,
                    strokeWeight: 3,
                    fillColor: '#FFFF00',
                    fillOpacity: 0.35,
                };
                var mPolygon = new Polygon.Polygon(_this.mapObj, title, options);
                mPolygon.setBillboardFn(MapUtils_1.showBillboard.bind(_this, oilField, 'OilField', oilFieldBillboard));
                _this.oilFieldMPolygons.push(mPolygon);
            });
        });
    };
    MapsAll.prototype.addProductionUnitsToMap = function (productionUnits) {
        var _this = this;
        function productionUnitBillboard(productionUnit) {
            var url = '/app/view_record?source=ProductionUnit&id=' + productionUnit.id;
            return '<b>Unidade de produção: </b><a href="' + url + '">' + productionUnit.name + '</a>';
        }
        this.productionUnitMMarkers.length = 0;
        var platformImage = 'images/platform.png';
        productionUnits.map(function (productionUnit) {
            var coordinates = JSON.parse(productionUnit.coordinates);
            if (!coordinates) {
                return;
            }
            var title = 'Unidade de produção: ' + productionUnit.name;
            var mMarker = new Marker_1.Marker(_this.mapObj, coordinates, platformImage, title);
            mMarker.setBillboardFn(MapUtils_1.showBillboard.bind(_this, productionUnit, 'ProductionUnit', productionUnitBillboard));
            _this.productionUnitMMarkers.push(mMarker);
        });
    };
    MapsAll.prototype.addDrillingRigsToMap = function (res) {
        var _this = this;
        function getModel(drillingRig) {
            return drillingRig.type == 'offshore' ? 'DrillingRigOffshore' : 'DrillingRigOnshore';
        }
        function drillingRigBillboard(drillingRig) {
            var url = '/app/view_record?source=' + getModel(drillingRig) + '&id=' + drillingRig.id;
            return '<b>Sonda: </b><a href="' + url + '">' + drillingRig.name + '</a>';
        }
        this.drillingRigMMarkers.length = 0;
        var platformImage = 'images/drilling_rig.png';
        res.drillingRigs.map(function (drillingRig) {
            var coordinates = JSON.parse(drillingRig.coordinates);
            if (!coordinates) {
                return;
            }
            var title = 'Sonda: ' + drillingRig.name;
            var mMarker = new Marker_1.Marker(_this.mapObj, coordinates, platformImage, title);
            mMarker.setBillboardFn(MapUtils_1.showBillboard.bind(_this, drillingRig, getModel(drillingRig), drillingRigBillboard));
            _this.drillingRigMMarkers.push(mMarker);
        });
    };
    MapsAll.prototype.addWellsToMap = function (res) {
        this.wellHeatMap = new HeatMap_1.HeatMap(this.mapObj, res.wells);
        this.wellHeatMap.setVisibility(false);
    };
    MapsAll.prototype.changeMapsItemsVisibility = function (mObjects, visibilityFieldName, event) {
        this[visibilityFieldName] = !this[visibilityFieldName];
        for (var _i = 0, mObjects_1 = mObjects; _i < mObjects_1.length; _i++) {
            var mObject = mObjects_1[_i];
            mObject.setVisibility(this[visibilityFieldName]);
        }
    };
    MapsAll.prototype.changeMapsItemVisibility = function (itemName, visibilityKeyName, event) {
        var mapItem = this[itemName];
        this[visibilityKeyName] = !this[visibilityKeyName];
        mapItem.setVisibility(this[visibilityKeyName]);
    };
    MapsAll.prototype.render = function () {
        var _this = this;
        var style = {
            width: '100%',
            height: 700
        };
        return (React.createElement("div", null, React.createElement(Map_1.Map, {initialState: this.state.initialState, receiveMapObj: function (mo) { return _this.mapObj = mo; }, style: style})));
    };
    return MapsAll;
}(React.Component));
exports.MapsAll = MapsAll;
