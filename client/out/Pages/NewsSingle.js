"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var DateUtils = require('../lib/DateUtils');
var Tableau_1 = require('../Components/Tableau');
var NewsSingle = (function (_super) {
    __extends(NewsSingle, _super);
    function NewsSingle(props) {
        _super.call(this, props);
        this.state = {
            record: { production_unit: {} }
        };
        this.id = props.location.query.id;
    }
    NewsSingle.prototype.componentDidMount = function () {
        this.getNews();
    };
    NewsSingle.prototype.componentWillReceiveProps = function (nextProps) {
        this.id = nextProps.location.query.id;
        this.getNews();
    };
    NewsSingle.prototype.getNews = function () {
        var req = {
            optionsName: 'SingleNews',
            id: this.id
        };
        server.getP('/get_record', req)
            .then(this.onNewsData.bind(this))
            .catch(function (err) { console.log(err); showError.show; });
    };
    NewsSingle.prototype.onNewsData = function (newsData) {
        this.state.record = newsData.record;
        this.setState(this.state);
    };
    NewsSingle.prototype.render = function () {
        var record = this.state.record;
        var tableauHTML = null;
        if (record.tableau_url) {
            var tableauUrls = record.tableau_url.split('\n');
            tableauHTML = tableauUrls.map(function (url, i) {
                return (React.createElement("div", null, React.createElement(Tableau_1.Tableau, {key: i, vizUrl: url}), React.createElement("br", null)));
            });
        }
        return (React.createElement("div", {className: "news-single"}, React.createElement("h3", null, record.title), React.createElement("div", {className: "content", dangerouslySetInnerHTML: { __html: record.content }}), tableauHTML, React.createElement("div", {className: "col-md-12 col-no-padding"}, React.createElement("div", {className: "col-md-6 col-no-padding"}, React.createElement("div", {className: "item-related"}, "Posted:", React.createElement("span", null, DateUtils.dateFormat(record.created_at))), React.createElement("div", {className: "item-related"})))));
    };
    return NewsSingle;
}(React.Component));
exports.NewsSingle = NewsSingle;
