"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Tableau_1 = require('../Components/Tableau');
var Observatory = (function (_super) {
    __extends(Observatory, _super);
    function Observatory(props) {
        _super.call(this, props);
        this.sources = {
            investments: "https://public.tableau.com/views/InvestimentosdaPetrobras-histrico_0/InvestimentodaPetrobras?:embed=y&:display_count=yes&:toolbar=no",
            wells: "https://public.tableau.com/views/DemandadepoosOffshore/Painel1",
            presalt: 'https://public.tableau.com/shared/C5YCPWWMN?:toolbar=no&:display_count=yes'
        };
        var id = props.location.query.id;
        this.state = {
            tableauUrl: this.sources[id]
        };
    }
    Observatory.prototype.componentWillReceiveProps = function (nextProps) {
        var id = nextProps.location.query.id;
        this.state.tableauUrl = this.sources[id];
        this.setState(this.state);
    };
    Observatory.prototype.render = function () {
        return (React.createElement(Tableau_1.Tableau, {vizUrl: this.state.tableauUrl, style: { width: 800, height: 700 }}));
    };
    return Observatory;
}(React.Component));
exports.Observatory = Observatory;
