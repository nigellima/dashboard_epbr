"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var Card_1 = require('../Components/Card');
var QueryDataIncrementalLoading_1 = require('../lib/QueryDataIncrementalLoading');
var showError = require('../lib/ShowError');
var CompaniesCards = (function (_super) {
    __extends(CompaniesCards, _super);
    function CompaniesCards(props) {
        _super.call(this, props);
        this.state = {
            records: []
        };
    }
    CompaniesCards.prototype.componentDidMount = function () {
        window.addEventListener('scroll', this.handleScroll.bind(this));
        this.queryDataIncrementalLoading = new QueryDataIncrementalLoading_1.QueryDataIncrementalLoading('companyCards', 12);
        this.queryDataIncrementalLoading.getData()
            .then(this.onData.bind(this))
            .catch(showError.show);
    };
    CompaniesCards.prototype.handleScroll = function (event) {
        var heightTreshold = 1000;
        var diff = event.srcElement.body.scrollHeight -
            (event.srcElement.body.scrollTop + window.innerHeight);
        if (diff < heightTreshold) {
            this.queryDataIncrementalLoading.getData()
                .then(this.onData.bind(this))
                .catch(showError.show);
        }
    };
    CompaniesCards.prototype.onData = function (records) {
        this.state.records = records;
        this.setState(this.state);
    };
    CompaniesCards.prototype.searchTextChanged = function (event) {
        clearTimeout(this.timeoutVar);
        this.timeoutVar = setTimeout(this.search.bind(this, event.target.value), 400);
    };
    CompaniesCards.prototype.search = function (searchValue) {
        var filter = {
            field: 'companies.name',
            like: searchValue
        };
        this.queryDataIncrementalLoading.search(filter)
            .then(this.onData.bind(this))
            .catch(showError.show);
    };
    CompaniesCards.prototype.render = function () {
        var header = (React.createElement("div", {className: "table-options"}, React.createElement("div", {className: "col-md-4 col-sm-6"}, React.createElement("div", {className: "table-options-name"}, React.createElement("h1", null, "Empresas"))), React.createElement("div", {className: "col-md-8 col-sm-6"}, React.createElement("div", {className: "table-options-pages"}, "Busca:", React.createElement("input", {className: "header-input", type: "text", onChange: this.searchTextChanged.bind(this), style: { marginLeft: 15 }})))));
        var cards = this.state.records.map(function (item, index) {
            return React.createElement(Card_1.Card, {key: index, data: item}, " ");
        });
        return (React.createElement("div", {className: "main-grid default-options"}, header, React.createElement("div", {className: "cards"}, React.createElement("div", {id: "carousel"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: "carousel slide fade-quote-carousel", "data-ride": "carousel", "data-interval": "100000"}, React.createElement("div", {className: "carousel-inner"}, React.createElement("div", {className: "active item"}, React.createElement("div", {className: "row"}, cards)))))))));
    };
    return CompaniesCards;
}(React.Component));
exports.CompaniesCards = CompaniesCards;
