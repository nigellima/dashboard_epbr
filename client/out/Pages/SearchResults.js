"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var SearchResults = (function (_super) {
    __extends(SearchResults, _super);
    function SearchResults(props) {
        _super.call(this, props);
        this.state = {
            results: []
        };
    }
    SearchResults.prototype.componentDidMount = function () {
        this.fetchServerResults(this.props.location.query.search);
    };
    SearchResults.prototype.componentWillReceiveProps = function (nextProps) {
        this.fetchServerResults(nextProps.location.query.search);
    };
    SearchResults.prototype.fetchServerResults = function (searchStr) {
        var req = {
            searchValue: searchStr,
            countLimit: 100
        };
        server.getP('/search', req)
            .then(this.onServerSearchResult.bind(this))
            .catch(showError.show);
    };
    SearchResults.prototype.onServerSearchResult = function (res) {
        this.state.results = res.values;
        this.setState(this.state);
    };
    SearchResults.prototype.render = function () {
        var resultsHTML = this.state.results.map(function (r, i) {
            var url = '/app/view_record?source=' + r.model + '&id=' + r.id;
            return React.createElement("p", {key: 'l' + i}, r.modelLabel + ': ', React.createElement(react_router_1.Link, {to: url}, r.name));
        });
        return (React.createElement("div", null, React.createElement("h2", null, React.createElement("b", null, "Resultados da busca para \"", this.props.location.query.search, "\"")), React.createElement("br", null), resultsHTML));
    };
    return SearchResults;
}(React.Component));
exports.SearchResults = SearchResults;
