"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ShowQueryData_1 = require('../Components/ShowQueryData');
var TargetSales = (function (_super) {
    __extends(TargetSales, _super);
    function TargetSales(props) {
        _super.call(this, props);
        this.state = {
            typesAndStages: []
        };
    }
    TargetSales.prototype.componentDidMount = function () {
        var options = {
            filters: [],
            queryName: 'projectTypesAndStages'
        };
        server.getQueryData(options)
            .then(this.onTypesAndStages.bind(this))
            .catch(showError.show);
    };
    TargetSales.prototype.onTypesAndStages = function (res) {
        this.state.typesAndStages = res.records;
        this.setState(this.state);
        return null;
    };
    TargetSales.prototype.getStageHTML = function (stage) {
        var items = this.state.typesAndStages.filter(function (r) { return r.stage == stage; });
        return items.map(function (item) {
            var query = {
                queryName: 'projectsTargetSales',
                title: item.segment_type,
                filters: {
                    fase: stage,
                    type: item.segment_type
                },
            };
            return (React.createElement(ShowQueryData_1.ShowQueryData, {key: item.stage + item.segment_type, model: query, objId: -1}));
        });
    };
    TargetSales.prototype.render = function () {
        if (!this.state.typesAndStages)
            return null;
        return (React.createElement("div", null, React.createElement("h2", null, React.createElement("b", null, "Target Sales")), React.createElement("div", {className: "row"}, React.createElement("div", {className: "col-md-6"}, React.createElement("h3", null, "CAPEX"), React.createElement("br", null), this.getStageHTML('CAPEX')), React.createElement("div", {className: "col-md-6"}, React.createElement("h3", null, "OPEX"), React.createElement("br", null), this.getStageHTML('OPEX'))), React.createElement("br", null), "Os projetos são acompanhados da sua licitação até a entrada em operação"));
    };
    return TargetSales;
}(React.Component));
exports.TargetSales = TargetSales;
