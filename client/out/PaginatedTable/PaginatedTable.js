"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ModelViewService = require('../lib/ModelViewUtils');
var react_router_1 = require('react-router');
var PaginatedTableHeader_1 = require('./PaginatedTableHeader');
var StringUtils = require('../lib/StringUtils');
var TableUtils_1 = require('../lib/TableUtils');
var BytesUtils_1 = require('../lib/BytesUtils');
var FileSaver = require('file-saver');
var PaginatedTable = (function (_super) {
    __extends(PaginatedTable, _super);
    function PaginatedTable(props) {
        _super.call(this, props);
        this.state = {
            headerParams: null,
            dataTable: null,
            searchStr: ''
        };
        window['paginatedTableRef'] = this;
        this.updated = false;
        this.filtersInAjax = [];
    }
    PaginatedTable.prototype.componentWillReceiveProps = function (nextProps) {
        if (!nextProps.tableParams) {
            return;
        }
        this.filtersInAjax = nextProps.filters;
        this.state.dataTable.draw();
    };
    PaginatedTable.prototype.shouldComponentUpdate = function (nextProps) {
        var result = !this.updated;
        this.updated = true;
        return result;
    };
    PaginatedTable.prototype.componentDidMount = function () {
        this.initHeader(this.props);
        this.setState(this.state);
    };
    PaginatedTable.prototype.componentDidUpdate = function () {
        this.initHeader(this.props);
        this.initTable(this.props);
    };
    PaginatedTable.prototype.initHeader = function (props) {
        var tableParams = props.tableParams;
        this.state.headerParams = {
            label: tableParams.label
        };
    };
    PaginatedTable.prototype.initTable = function (props) {
        var dataTableElement = $('#mainTable');
        var tableParams = props.tableParams;
        var _a = TableUtils_1.genColumns(tableParams), columns = _a.columns, currencyColumnsIndexes = _a.currencyColumnsIndexes;
        columns.push({ title: "Info", data: 'view_record' });
        this.state.dataTable = dataTableElement.DataTable({
            columns: columns,
            language: ModelViewService.datatablesPtBrTranslation,
            processing: true,
            serverSide: true,
            searching: false,
            dom: 'rtip',
            ajax: this.ajaxFn.bind(this, props),
            columnDefs: [
                { className: "text-right", targets: currencyColumnsIndexes },
            ]
        });
    };
    PaginatedTable.prototype.followLink = function (sourceName, id) {
        var opts = {
            source: sourceName,
            id: id
        };
        var queryStr = "/app/view_record" + StringUtils.formatUrlParams(opts);
        react_router_1.browserHistory.push(queryStr);
    };
    PaginatedTable.prototype.getQueryParams = function () {
        var orderColumns = [];
        for (var i = 0; i < this.dataQueryData.order.length; i++) {
            var columnIndex = this.dataQueryData.order[i].column;
            var orderObj = {
                fieldName: this.dataQueryData.columns[columnIndex].data,
                dir: this.dataQueryData.order[i].dir
            };
            orderColumns.push(orderObj);
        }
        return {
            pagination: {
                first: this.dataQueryData.start,
                itemsPerPage: this.dataQueryData.length
            },
            order: orderColumns,
            filters: this.filtersInAjax,
            searchStr: this.state.searchStr
        };
    };
    PaginatedTable.prototype.ajaxFn = function (props, data, callback, settings) {
        this.dataQueryData = data;
        var req = {
            queryName: props.tableParams.source,
            queryParams: this.getQueryParams()
        };
        server.getTableData(req)
            .then(this.onTableData.bind(this, callback))
            .catch(showError.show);
    };
    PaginatedTable.prototype.onTableData = function (callback, serverResult) {
        var dataSet = [];
        var url = new URL(window.location.href);
        var c = url.searchParams.get("source");
        var source = '';
        if (c === 'FPSOs')
            source = 'ProductionUnit';
        else if ('BIDs')
            source = 'Bid';
        for (var i = 0; i < serverResult.records.length; i++) {
            var record = serverResult.records[i];
            var paramsStr = 'source=' + source + '&id=' + record.id;
            record.view_record = '<a href="/app/view_record?' + paramsStr + '">View Record</a>';
            dataSet.push(record);
        }
        var result = {
            aaData: dataSet,
            recordsTotal: serverResult.count,
            recordsFiltered: serverResult.count
        };
        callback(result);
        return null;
    };
    PaginatedTable.prototype.filterChanged = function (searchStr) {
        this.state.searchStr = searchStr;
        this.state.dataTable.draw();
    };
    PaginatedTable.prototype.getExcelFile = function () {
        var req = {
            queryName: this.props.tableParams.source,
            queryParams: this.getQueryParams()
        };
        server.getP('/get_query_excel', req)
            .then(function (xlsxBinary) {
            var ba = BytesUtils_1.StrToByteArray(xlsxBinary);
            var blob = new Blob([ba], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            FileSaver.saveAs(blob, "arquivo.xlsx");
        })
            .catch(showError.show);
    };
    PaginatedTable.prototype.render = function () {
        if (!this.state.headerParams) {
            return React.createElement("div", null);
        }
        var tableHeader = (React.createElement(PaginatedTableHeader_1.PaginatedTableHeader, {headerParams: this.state.headerParams, filterChanged: this.filterChanged.bind(this)}));
        return (React.createElement("div", {className: "main-table table-responsive bootstrap-table", style: { height: 600 }}, tableHeader, React.createElement("table", {id: "mainTable", className: "table", cellSpacing: "0", width: "100%"}), React.createElement("button", {className: "btn btn-default", onClick: this.getExcelFile.bind(this)}, "Export to Excel File")));
    };
    return PaginatedTable;
}(React.Component));
exports.PaginatedTable = PaginatedTable;
