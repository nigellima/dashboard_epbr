"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var PaginatedTableHeader = (function (_super) {
    __extends(PaginatedTableHeader, _super);
    function PaginatedTableHeader(props) {
        _super.call(this, props);
        this.state = {
            delayTimer: {},
            searchText: '',
        };
    }
    PaginatedTableHeader.prototype.componentDidMount = function () {
    };
    PaginatedTableHeader.prototype.componentWillReceiveProps = function (nextProps) {
    };
    PaginatedTableHeader.prototype.filterChanged = function () {
        this.props.filterChanged(this.state.searchText);
    };
    PaginatedTableHeader.prototype.searchTextChanged = function (event) {
        this.state.searchText = event.target.value;
        this.setState(this.state);
        clearTimeout(this.state.delayTimer);
        this.state.delayTimer = setTimeout(this.filterChanged.bind(this), 400);
        event.persist();
    };
    ;
    PaginatedTableHeader.prototype.render = function () {
        return (React.createElement("div", {className: "table-options"}, React.createElement("div", {className: "col-md-4 col-sm-6"}, React.createElement("div", {className: "table-options-name"}, React.createElement("h1", null, this.props.headerParams.label))), React.createElement("div", {className: "col-md-8 col-sm-6"}, React.createElement("div", {className: "table-options-pages"}, "Busca:", React.createElement("input", {className: "header-input", style: { marginLeft: 15, marginRight: 15 }, type: "text", value: this.state.searchText, onChange: this.searchTextChanged.bind(this)})))));
    };
    return PaginatedTableHeader;
}(React.Component));
exports.PaginatedTableHeader = PaginatedTableHeader;
