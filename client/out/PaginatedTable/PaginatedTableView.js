"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var PaginatedTable_1 = require('./PaginatedTable');
var Tableau_1 = require('../Components/Tableau');
var FiltersGroup_1 = require('../Components/FiltersGroup');
var PaginatedTableView = (function (_super) {
    __extends(PaginatedTableView, _super);
    function PaginatedTableView(props) {
        _super.call(this, props);
        this.source = props.location.query.source;
        this.state = {
            tableParams: null,
            show: false,
            filters: []
        };
    }
    PaginatedTableView.prototype.componentDidMount = function () {
        this.getFields();
    };
    PaginatedTableView.prototype.componentWillReceiveProps = function (nextProps) {
        this.source = nextProps.location.query.source;
        this.getFields();
    };
    PaginatedTableView.prototype.getFields = function () {
        this.state.show = false;
        this.setState(this.state);
        var req = { queryName: this.source };
        server.getP('/queries_fields', req)
            .then(this.onFields.bind(this))
            .catch(showError.show);
    };
    PaginatedTableView.prototype.onFields = function (res) {
        var fields = res.fields;
        this.state.tableParams = {
            fields: fields,
            tableauUrl: res.tableauUrl,
            label: res.title,
            source: this.source
        };
        this.setState(this.state);
        return null;
    };
    PaginatedTableView.prototype.onFiltersChange = function (filters) {
        this.state.filters = filters;
        this.setState(this.state);
    };
    PaginatedTableView.prototype.render = function () {
        if (!this.state.show) {
            this.state.show = true;
            return React.createElement("div", null);
        }
        var tableau = null;
        if (this.state.tableParams.tableauUrl) {
            tableau = (React.createElement("div", null, React.createElement(Tableau_1.Tableau, {vizUrl: this.state.tableParams.tableauUrl}), React.createElement("br", null)));
        }
        var paginatedTableHTML = null;
        if (this.state.tableParams) {
            paginatedTableHTML = (React.createElement(PaginatedTable_1.PaginatedTable, {tableParams: this.state.tableParams, filters: this.state.filters}));
        }
        return (React.createElement("div", null, tableau, React.createElement(FiltersGroup_1.FiltersGroup, {tableParams: this.state.tableParams, onChange: this.onFiltersChange.bind(this)}), React.createElement("br", null), paginatedTableHTML));
    };
    return PaginatedTableView;
}(React.Component));
exports.PaginatedTableView = PaginatedTableView;
