"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ObjectNews_1 = require('../Components/ObjectNews');
var ErrorReport_1 = require('../Components/ErrorReport');
var ViewRecord = require('./ViewRecord');
var Map_1 = require('../Maps/Map');
var MapUtils_1 = require('../lib/MapUtils');
var Google_1 = require('../lib/Google');
var Polygon = require('../Maps/Polygon');
var BlockView = (function (_super) {
    __extends(BlockView, _super);
    function BlockView(props) {
        _super.call(this, props);
        var id = props.location.query.id;
        var source = 'Block';
        this.blockMPolygons = [];
        this.state = {
            id: id,
            source: source,
            recordData: [],
            allReferencedObjects: [],
            objectLabel: '',
            url: props.location.basename + props.location.pathname + props.location.search,
            initialMapState: {
                zoom: 9,
                center: MapUtils_1.rioDeJaneiroCoords,
                mapTypeId: Google_1.googleRef.maps.MapTypeId.HYBRID
            },
            extraRecordData: {
                tableauUrls: [],
                embedStrs: []
            },
            updatedAt: ''
        };
    }
    BlockView.prototype.componentDidMount = function () {
        var _this = this;
        _super.prototype.componentDidMount.call(this);
        server.getP('/maps/blocks', {})
            .then(function (res) { _this.addBlocksToMap(res.blocks); })
            .catch(showError.show);
    };
    BlockView.prototype.addBlocksToMap = function (blocks) {
        var _this = this;
        function blockBillboard(block) {
            var url = '/app/view_record?source=Block&id=' + block.id;
            return '<b>Bloco: </b><a href="' + url + '">' + block.name + '</a>';
        }
        this.blockMPolygons.length = 0;
        blocks.map(function (block) {
            var polygons = JSON.parse(block.polygons);
            if (!polygons) {
                return;
            }
            polygons.map(function (polygon) {
                var title = 'Bloco: ' + block.name;
                var options = {
                    paths: polygon,
                    strokeColor: '#FFFF00',
                    strokeOpacity: 0.3,
                    strokeWeight: 2,
                    fillColor: '#FFFF00',
                    fillOpacity: 0.1,
                };
                if (block.id == _this.state.id) {
                    options = {
                        paths: polygon,
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 3,
                        fillColor: '#FF0000',
                        fillOpacity: 0.45,
                    };
                }
                var mPolygon = new Polygon.Polygon(_this.mapObj, title, options);
                if (block.id == _this.state.id) {
                    var polygonDims = mPolygon.getDimensions();
                    var gCenterPoint = new Google_1.googleRef.maps.LatLng(polygonDims.center.lat, polygonDims.center.lng);
                    _this.mapObj.gMap.panTo(gCenterPoint);
                }
                mPolygon.setBillboardFn(MapUtils_1.showBillboard.bind(_this, block, 'Block', blockBillboard));
                _this.blockMPolygons.push(mPolygon);
            });
        });
    };
    BlockView.prototype.render = function () {
        var _this = this;
        var mapStyle = {
            width: '100%',
            height: '100%'
        };
        return (React.createElement("div", null, React.createElement("div", {className: "row"}, this.getInfoBox(), React.createElement("div", {className: "col-md-6 main-boxes"}, React.createElement(Map_1.Map, {initialState: this.state.initialMapState, receiveMapObj: function (mo) { return _this.mapObj = mo; }, style: mapStyle}))), React.createElement("br", null), React.createElement(ErrorReport_1.ErrorReport, {objectLabel: this.state.objectLabel, url: this.state.url}), React.createElement("hr", null), this.getTableausHTML(), this.getEmbedsHTML(), this.getRefObjectsElements(this.state.allReferencedObjects), React.createElement(ObjectNews_1.ObjectNews, {modelName: this.state.source, objId: this.state.id})));
    };
    return BlockView;
}(ViewRecord.ViewRecord));
exports.BlockView = BlockView;
