"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ObjectNews_1 = require('../Components/ObjectNews');
var TimeSeriesChart_1 = require('../Charts/TimeSeriesChart');
var ErrorReport_1 = require('../Components/ErrorReport');
var ViewRecord = require('./ViewRecord');
var Map_1 = require('../Maps/Map');
var MapUtils_1 = require('../lib/MapUtils');
var Google_1 = require('../lib/Google');
var Polygon = require('../Maps/Polygon');
var OilFieldView = (function (_super) {
    __extends(OilFieldView, _super);
    function OilFieldView(props) {
        _super.call(this, props);
        var id = props.location.query.id;
        var source = 'OilField';
        this.oilFieldMPolygons = [];
        this.state = {
            id: id,
            source: source,
            recordData: [],
            allReferencedObjects: [],
            objectLabel: '',
            url: props.location.basename + props.location.pathname + props.location.search,
            prodQueryParams: { oilField: id },
            productionChartParams: {
                yLabel: 'Produção (bbl/dia)',
                seriesList: [
                    {
                        fieldName: 'oil_production',
                        label: 'Óleo'
                    },
                    {
                        fieldName: 'water_production',
                        label: 'Água'
                    },
                ],
                xAxis: 'date_prod'
            },
            initialMapState: {
                zoom: 8,
                center: MapUtils_1.rioDeJaneiroCoords,
                mapTypeId: Google_1.googleRef.maps.MapTypeId.HYBRID
            },
            extraRecordData: {
                tableauUrls: [],
                embedStrs: []
            },
            updatedAt: ''
        };
    }
    OilFieldView.prototype.componentDidMount = function () {
        var _this = this;
        _super.prototype.componentDidMount.call(this);
        server.getP('/maps/oil_fields', {})
            .then(function (res) { _this.addOilFieldsToMap(res.oilFields); })
            .catch(showError.show);
    };
    OilFieldView.prototype.addOilFieldsToMap = function (oilFields) {
        var _this = this;
        function oilFieldBillboard(oilField) {
            var url = '/app/view_record?source=OilField&id=' + oilField.id;
            return '<b>Campo: </b><a href="' + url + '">' + oilField.name + '</a>';
        }
        this.oilFieldMPolygons.length = 0;
        oilFields.map(function (oilField) {
            var polygons = JSON.parse(oilField.polygons);
            if (!polygons) {
                return;
            }
            polygons.map(function (polygon) {
                var title = 'Campo: ' + oilField.name;
                var options = {
                    paths: polygon,
                    strokeColor: '#FFFF00',
                    strokeOpacity: 0.3,
                    strokeWeight: 2,
                    fillColor: '#FFFF00',
                    fillOpacity: 0.1,
                };
                if (oilField.id == _this.state.id) {
                    options = {
                        paths: polygon,
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 3,
                        fillColor: '#FF0000',
                        fillOpacity: 0.45,
                    };
                }
                var mPolygon = new Polygon.Polygon(_this.mapObj, title, options);
                if (oilField.id == _this.state.id) {
                    var polygonDims = mPolygon.getDimensions();
                    var gCenterPoint = new Google_1.googleRef.maps.LatLng(polygonDims.center.lat, polygonDims.center.lng);
                    _this.mapObj.gMap.panTo(gCenterPoint);
                }
                mPolygon.setBillboardFn(MapUtils_1.showBillboard.bind(_this, oilField, 'OilField', oilFieldBillboard));
                _this.oilFieldMPolygons.push(mPolygon);
            });
        });
    };
    OilFieldView.prototype.render = function () {
        var _this = this;
        var mapStyle = {
            width: '100%',
            height: '100%'
        };
        return (React.createElement("div", null, React.createElement("div", {className: "row"}, this.getInfoBox(), React.createElement("div", {className: "col-md-6 main-boxes"}, React.createElement(Map_1.Map, {initialState: this.state.initialMapState, receiveMapObj: function (mo) { return _this.mapObj = mo; }, style: mapStyle}))), React.createElement("br", null), React.createElement(ErrorReport_1.ErrorReport, {objectLabel: this.state.objectLabel, url: this.state.url}), React.createElement("hr", null), this.getTableausHTML(), this.getEmbedsHTML(), this.getRefObjectsElements(this.state.allReferencedObjects), React.createElement(TimeSeriesChart_1.TimeSeriesChart, {queryName: "ProductionByField", qParams: this.state.prodQueryParams, chartParams: this.state.productionChartParams}), React.createElement(ObjectNews_1.ObjectNews, {modelName: this.state.source, objId: this.state.id})));
    };
    return OilFieldView;
}(ViewRecord.ViewRecord));
exports.OilFieldView = OilFieldView;
