"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var ViewRecordFields = require('./ViewRecordFields');
var ObjectNews_1 = require('../Components/ObjectNews');
var ErrorReport_1 = require('../Components/ErrorReport');
var ViewRecord = require('./ViewRecord');
var ArrayUtils_1 = require('../lib/ArrayUtils');
var ProjectView = (function (_super) {
    __extends(ProjectView, _super);
    function ProjectView(props) {
        _super.call(this, props);
        this.state.source = 'Project';
        this.state.companies = [];
    }
    ProjectView.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        var req = { model: 'Company' };
        server.getP('/combo_values', req)
            .then(this.onCompanies.bind(this))
            .catch(showError.show);
    };
    ProjectView.prototype.onCompanies = function (res) {
        this.state.companies = res.values;
        this.setState(this.state);
    };
    ProjectView.prototype.render = function () {
        var objsField = ArrayUtils_1.find(this.state.recordData, function (r) { return r.name == 'objects'; });
        var objFieldsHTML = null;
        if (objsField) {
            var objValues = objsField.value;
            objFieldsHTML = objValues.map(function (o) {
                var platHTML = ViewRecordFields.objLinkHTML(o.model, o.id.toString(), o.name);
                return ViewRecordFields.completeHTML(o.description, platHTML);
            });
        }
        var contracteds = [];
        var jsonField = ArrayUtils_1.find(this.state.recordData, function (r) { return r.name == 'json_field'; });
        if (jsonField) {
            var jsonValues = JSON.parse(jsonField.value);
            var _loop_1 = function(i) {
                var queries = [
                    {
                        queryName: 'contractsOfContractedInProject',
                        title: 'Contratos da contratada no projeto',
                        filters: {
                            index: i,
                            id: this_1.state.id
                        },
                    },
                    {
                        queryName: 'personsOfContractedInProject',
                        title: 'Pessoas da contratada',
                        filters: {
                            index: i,
                            id: this_1.state.id
                        },
                    },
                ];
                var contractor_id = jsonValues.contractors[i].contractor_id;
                var company = ArrayUtils_1.find(this_1.state.companies, function (c) { return c.id == contractor_id; });
                var contractorName = company ? company.label : 'Empresa';
                var contractedHTML = (React.createElement("div", {key: 'index' + i}, React.createElement("h3", null, contractorName + ' - ' + jsonValues.contractors[i].scope), this_1.getRefObjectsElements(queries)));
                contracteds.push(contractedHTML);
            };
            var this_1 = this;
            for (var i = 0; i < jsonValues.contractors.length; i++) {
                _loop_1(i);
            }
        }
        return (React.createElement("div", null, React.createElement("div", {className: "row"}, this.getInfoBox(), React.createElement("div", {className: "col-md-6 main-boxes"}, React.createElement("img", {src: this.getImgUrl(), style: { width: 600 }}))), React.createElement("br", null), React.createElement(ErrorReport_1.ErrorReport, {objectLabel: this.state.objectLabel, url: this.state.url}), React.createElement("hr", null), this.getTableausHTML(), this.getEmbedsHTML(), this.getRefObjectsElements(this.state.allReferencedObjects), contracteds, React.createElement(ObjectNews_1.ObjectNews, {modelName: this.state.source, objId: this.state.id})));
    };
    return ProjectView;
}(ViewRecord.ViewRecord));
exports.ProjectView = ProjectView;
