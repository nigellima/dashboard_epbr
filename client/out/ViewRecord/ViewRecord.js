"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var server = require('../lib/Server');
var showError = require('../lib/ShowError');
var react_router_1 = require('react-router');
var ViewRecordFields_1 = require('./ViewRecordFields');
var ShowQueryData_1 = require('../Components/ShowQueryData');
var ObjectNews_1 = require('../Components/ObjectNews');
var ErrorReport_1 = require('../Components/ErrorReport');
var moment = require('moment');
var ViewRecord = (function (_super) {
    __extends(ViewRecord, _super);
    function ViewRecord(props) {
        _super.call(this, props);
        var _a = props.location.query, id = _a.id, source = _a.source;
        this.state = {
            id: id,
            source: source,
            recordData: [],
            allReferencedObjects: [],
            objectLabel: '',
            url: props.location.basename + props.location.pathname + props.location.search,
            extraRecordData: {
                tableauUrls: [],
                embedStrs: []
            },
            updatedAt: ''
        };
        var customSources = {
            'OilField': "/app/oil_field",
            'Block': "/app/block",
            'GasPipeline': "/app/gas_pipeline",
            'News': "/app/view_new",
            'Project': "/app/view_project",
        };
        var customSource = customSources[source];
        if (customSource) {
            react_router_1.browserHistory.replace(customSource + '?id=' + id);
        }
    }
    ViewRecord.prototype.getFixedRefObjects = function (source, id) {
        return [
            {
                queryName: 'PersonsByProject',
                title: 'Pessoas',
                filters: {
                    project_id: id,
                    dataSource: source
                },
            },
            {
                queryName: 'BidsByObject',
                title: 'Licitações',
                filters: {
                    obj_id: id,
                    dataSource: source
                }
            },
            {
                queryName: 'contractsByObject',
                title: 'Contratos',
                filters: {
                    obj_id: id,
                    dataSource: source
                }
            },
            {
                queryName: 'objectRelatedProjects',
                title: 'Oportunidades',
                filters: {
                    id: id,
                    modelName: source
                }
            },
        ];
    };
    ViewRecord.prototype.componentDidMount = function () {
        var _a = this.state, id = _a.id, source = _a.source;
        server.viewRecord(source, id)
            .then(this.showValues.bind(this))
            .catch(showError.show);
    };
    ViewRecord.prototype.componentWillReceiveProps = function (nextProps) {
        var _a = nextProps.location.query, id = _a.id, source = _a.source;
        this.state.id = id;
        this.state.source = source;
        server.viewRecord(source, id)
            .then(this.showValues.bind(this))
            .catch(showError.show);
    };
    ViewRecord.prototype.showValues = function (viewData) {
        this.state.recordData = viewData.record;
        this.state.updatedAt = viewData.updatedAt;
        this.state.objectLabel = viewData.record[0].value;
        this.state.extraRecordData = viewData.extraRecordData;
        var customReferencedObjs = viewData.referencedObjects ? viewData.referencedObjects : [];
        var fixedRefObjects = this.getFixedRefObjects(this.state.source, this.state.id);
        this.state.allReferencedObjects = [];
        Array.prototype.push.apply(this.state.allReferencedObjects, customReferencedObjs);
        Array.prototype.push.apply(this.state.allReferencedObjects, fixedRefObjects);
        this.setState(this.state);
        return null;
    };
    ViewRecord.prototype.getRefObjectsElements = function (objects) {
        var _this = this;
        var referencedObjects = objects.map(function (referencedObject) {
            return (React.createElement(ShowQueryData_1.ShowQueryData, {key: referencedObject.queryName, model: referencedObject, objId: _this.state.id}));
        });
        return referencedObjects;
    };
    ViewRecord.prototype.getTableausHTML = function () {
        return (React.createElement("div", null));
    };
    ViewRecord.prototype.getEmbedsHTML = function () {
        return this.state.extraRecordData.embedStrs.map(function (eurl, i) {
            return (React.createElement("div", null, React.createElement("p", {dangerouslySetInnerHTML: { __html: eurl }}), React.createElement("br", null)));
        });
    };
    ViewRecord.prototype.getImgUrl = function () {
        return server.paths.baseImg + this.state.source + '/' +
            'img_' + this.state.id + '_original.jpg';
    };
    ViewRecord.prototype.getInfoBox = function () {
        return (React.createElement("div", {className: "col-md-6"}, React.createElement(ViewRecordFields_1.ViewRecordFields, {recordData: this.state.recordData, source: this.state.source, objId: this.state.id}), "Updated at ", moment(this.state.updatedAt).format("DD/MM/YYYY")));
    };
    ViewRecord.prototype.render = function () {
        return (React.createElement("div", null, React.createElement("div", {className: "row"}, this.getInfoBox(), React.createElement("div", {className: "col-md-6 main-boxes"}, React.createElement(ObjectNews_1.ObjectNews, {modelName: this.state.source, objId: this.state.id}), React.createElement("br", null))), React.createElement("br", null), React.createElement(ErrorReport_1.ErrorReport, {objectLabel: this.state.objectLabel, url: this.state.url}), React.createElement("hr", null), this.getTableausHTML(), this.getEmbedsHTML(), this.getRefObjectsElements(this.state.allReferencedObjects)));
    };
    return ViewRecord;
}(React.Component));
exports.ViewRecord = ViewRecord;
