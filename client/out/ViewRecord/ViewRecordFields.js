"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ModelViewService = require('../lib/ModelViewUtils');
var react_router_1 = require('react-router');
var StringUtils_1 = require('../lib/StringUtils');
function completeHTML(label, innerHTML) {
    var labelHTML = (React.createElement("div", {className: "col-md-4", key: label}, label));
    return (React.createElement("li", {key: label}, labelHTML, React.createElement("div", {className: "col-md-8"}, innerHTML)));
}
exports.completeHTML = completeHTML;
function objLinkHTML(modelName, id, value) {
    var url = "/app/view_record?source=" + modelName + "&id=" + id;
    return React.createElement(react_router_1.Link, {to: url}, value);
}
exports.objLinkHTML = objLinkHTML;
var ViewRecordFields = (function (_super) {
    __extends(ViewRecordFields, _super);
    function ViewRecordFields(props) {
        _super.call(this, props);
        this.state = {
            record: []
        };
    }
    ViewRecordFields.prototype.componentDidMount = function () {
        this.onRecordDataChange(this.props);
    };
    ViewRecordFields.prototype.componentWillReceiveProps = function (nextProps) {
        this.onRecordDataChange(nextProps);
    };
    ViewRecordFields.prototype.onRecordDataChange = function (props) {
        var recordData = props.recordData;
        var fieldValues = [];
        for (var i = 0; i < recordData.length; i++) {
            if (!recordData[i].value ||
                StringUtils_1.strContains(recordData[i].label, 'ignorar') ||
                StringUtils_1.strContains(recordData[i].label, 'admin') ||
                recordData[i].isPhoto) {
                continue;
            }
            if (recordData[i].isMultiFieldText) {
                var obj = JSON.parse(recordData[i].value);
                for (var key in obj) {
                    var item = obj[key];
                    var newFieldInfo = { type: 'VARCHAR' };
                    newFieldInfo.label = key;
                    newFieldInfo.value = item;
                    if (!newFieldInfo.value ||
                        StringUtils_1.strContains(newFieldInfo.label, 'ignorar') ||
                        StringUtils_1.strContains(newFieldInfo.label, 'admin')) {
                        continue;
                    }
                    fieldValues.push(newFieldInfo);
                }
            }
            else {
                recordData[i].value = ModelViewService.formatByType(recordData[i]);
                fieldValues.push(recordData[i]);
            }
        }
        this.state.record = fieldValues;
        this.setState(this.state);
    };
    ViewRecordFields.prototype.insertBR = function (input) {
        if (!(typeof input == 'string')) {
            return [React.createElement("div", {key: 'text' + 0}, input, " ", React.createElement("br", null))];
        }
        return input.split('\n').map(function (text, index) {
            return React.createElement("div", {key: 'text' + index}, text, " ", React.createElement("br", null));
        });
    };
    ViewRecordFields.prototype.render = function () {
        var _this = this;
        if (this.state.record.length == 0) {
            return React.createElement("div", null);
        }
        var first = this.state.record[0];
        var filteredFields = this.state.record.slice(1, this.state.record.length);
        var fields = filteredFields.map(function (field) {
            var fieldHtml = null;
            if (field.ref) {
                fieldHtml = objLinkHTML(field.model, field.value, field.name);
            }
            else if (field.isLink) {
                fieldHtml = React.createElement("a", {href: field.value, target: "_blank"}, field.value);
            }
            else if (field.isHTML) {
                fieldHtml = React.createElement("div", {dangerouslySetInnerHTML: { __html: field.value }});
            }
            else if (field.isList) {
                var listItems = field.value.map(function (item, index) {
                    return React.createElement("div", {key: 'i' + index}, item, React.createElement("br", null));
                });
                fieldHtml = React.createElement("div", null, listItems, " ");
            }
            else if (field.isProjectList) {
                var listItems = field.value.map(function (item, index) {
                    var url = "/app/view_record?source=" + item.model + "&id=" + item.id;
                    return (React.createElement("div", {key: 'pl' + index}, React.createElement(react_router_1.Link, {to: url}, item.name), item.description ? React.createElement("span", null, " - Descrição: ", item.description) : null));
                });
                fieldHtml = React.createElement("div", null, listItems, " ");
            }
            else if (field.isConcessionaries) {
                var listItems = field.value.map(function (concessionary, i) {
                    var url = "/app/view_record?source=Company&id=" + concessionary.id;
                    var labelHtml = ": " + concessionary.prop * 100 + "%";
                    return (React.createElement("div", {key: i + concessionary.name}, React.createElement(react_router_1.Link, {to: url}, concessionary.name), labelHtml));
                });
                fieldHtml = React.createElement("div", null, listItems, " ");
            }
            else {
                fieldHtml = React.createElement("span", null, " ", _this.insertBR(field.value), " ");
            }
            return completeHTML(field.label, fieldHtml);
        });
        fields = fields.concat(this.props.additionalFields);
        return (React.createElement("div", {className: "main-boxes"}, React.createElement("div", {className: "box-wrapper"}, React.createElement("div", {className: "box-head"}, React.createElement("div", null, React.createElement("b", null, first.value))), React.createElement("div", {className: "box-body"}, React.createElement("ul", null, fields)))));
    };
    return ViewRecordFields;
}(React.Component));
exports.ViewRecordFields = ViewRecordFields;
