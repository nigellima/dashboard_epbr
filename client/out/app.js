"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require('react');
var ReactDOM = require('react-dom');
var react_router_1 = require('react-router');
var server = require('./lib/Server');
var session = require('./lib/session');
var TopMenu_1 = require('./Components/TopMenu');
var SecondMenuBar_1 = require('./Components/SecondMenuBar');
var AdminList_1 = require('./Admin/AdminList');
var AdminGrid_1 = require('./Admin/AdminGrid');
var AdminEdit_1 = require('./Admin/AdminEdit');
var NewsEdit_1 = require('./Admin/NewsEdit');
var ProjectEdit_1 = require('./Admin/ProjectEdit');
var Publisher_1 = require('./Admin/Publisher/Publisher');
var RequestsViewer_1 = require('./Admin/RequestsViewer');
var ViewRecord_1 = require('./ViewRecord/ViewRecord');
var OilFieldView_1 = require('./ViewRecord/OilFieldView');
var BlockView_1 = require('./ViewRecord/BlockView');
var ProjectView_1 = require('./ViewRecord/ProjectView');
var Flash_1 = require('./Components/Flash');
var ChangePassword_1 = require('./Pages/ChangePassword');
var MapsAll_1 = require('./Pages/MapsAll');
var Dashboard_1 = require('./Pages/Dashboard');
var NewsSingle_1 = require('./Pages/NewsSingle');
var PaginatedTableView_1 = require('./PaginatedTable/PaginatedTableView');
var DataList_1 = require('./Pages/DataList');
var Insights_1 = require('./Insights/Insights');
var Observatory_1 = require('./Pages/Observatory');
var PersonsByCompany_1 = require('./Pages/PersonsByCompany');
var NewsTicker_1 = require('./Components/NewsTicker');
var Analytics_1 = require('./Pages/Analytics');
var TargetSales_1 = require('./Pages/TargetSales');
var SearchResults_1 = require('./Pages/SearchResults');
var ReactGA = require('react-ga');
ReactGA.initialize('UA-80990869-2');
var InsiderOilApp = (function (_super) {
    __extends(InsiderOilApp, _super);
    function InsiderOilApp(props) {
        _super.call(this, props);
        this.state = {
            isAdmin: false,
            username: '',
            url: props.location.query.url
        };
        var token = props.location.query.token;
        if (token) {
            session.login(token);
        }
        else {
            token = session.getToken();
            if (!token) {
                window.location.replace('/login');
                return;
            }
        }
        server.getUserDetails(function (response) {
            this.setState({
                username: response.login,
                isAdmin: response.admin
            });
            ReactGA.set({ userId: response.id });
        }.bind(this), function (result) {
            if (result.status == 401) {
                window.location.replace('/login');
            }
        });
        if (props.location.pathname == 'app/' || props.location.pathname == 'app/index.html') {
            react_router_1.browserHistory.replace('/app/paginated_table_view?source=FPSOs');
        }
    }
    InsiderOilApp.prototype.componentWillMount = function () {
        if (this.state.url) {
            react_router_1.browserHistory.push(this.state.url);
        }
    };
    InsiderOilApp.prototype.render = function () {
        var footer = (React.createElement("footer", {id: "footer"}, React.createElement("div", {className: "container-1450"}, React.createElement("div", {className: "row"}, React.createElement("div", {className: " col-lg-2 col-md-2 col-sm-3 col-xs-12"}, "Dashboard by EPBR"), React.createElement("div", {className: " col-lg-10 col-md-10 col-sm-9 col-xs-12"}, React.createElement("ul", null, React.createElement("li", null, "©2018. E&P Brasil - Serviços de Informação")))))));
        return (React.createElement("div", null, React.createElement(NewsTicker_1.NewsTicker, null), React.createElement("div", {className: "container-1450"}, React.createElement(TopMenu_1.TopMenu, {username: this.state.username}), React.createElement(SecondMenuBar_1.SecondMenuBar, {isAdmin: this.state.isAdmin})), React.createElement("div", {className: "container-1450"}, React.createElement(Flash_1.Flash, {timeout: 5000}), this.props.children), React.createElement("br", null), footer));
    };
    return InsiderOilApp;
}(React.Component));
function logPageView() {
    var completeURL = window.location.pathname + window.location.search;
    ReactGA.set({ page: completeURL });
    ReactGA.pageview(completeURL);
}
ReactDOM.render(React.createElement(react_router_1.Router, {history: react_router_1.browserHistory, onUpdate: logPageView}, React.createElement(react_router_1.Route, {path: "/", component: InsiderOilApp}), React.createElement(react_router_1.Route, {path: "/app", component: InsiderOilApp}, React.createElement(react_router_1.Route, {path: "dashboard", component: Dashboard_1.Dashboard}), React.createElement(react_router_1.Route, {path: "admin", component: AdminList_1.AdminList}), React.createElement(react_router_1.Route, {path: "model_view", component: AdminGrid_1.AdminGrid}), React.createElement(react_router_1.Route, {path: "edit_item", component: AdminEdit_1.AdminEdit}), React.createElement(react_router_1.Route, {path: "create_news", component: NewsEdit_1.NewsEdit}), React.createElement(react_router_1.Route, {path: "create_project", component: ProjectEdit_1.ProjectEdit}), React.createElement(react_router_1.Route, {path: "view_record", component: ViewRecord_1.ViewRecord}), React.createElement(react_router_1.Route, {path: "oil_field", component: OilFieldView_1.OilFieldView}), React.createElement(react_router_1.Route, {path: "block", component: BlockView_1.BlockView}), React.createElement(react_router_1.Route, {path: "view_project", component: ProjectView_1.ProjectView}), React.createElement(react_router_1.Route, {path: "maps_all", component: MapsAll_1.MapsAll}), React.createElement(react_router_1.Route, {path: "change_password", component: ChangePassword_1.ChangePassword}), React.createElement(react_router_1.Route, {path: "view_new", component: NewsSingle_1.NewsSingle}), React.createElement(react_router_1.Route, {path: "paginated_table_view", component: PaginatedTableView_1.PaginatedTableView}), React.createElement(react_router_1.Route, {path: "data_list", component: DataList_1.DataList}), React.createElement(react_router_1.Route, {path: "insights", component: Insights_1.Insights}), React.createElement(react_router_1.Route, {path: "insights_publisher", component: Publisher_1.Publisher}), React.createElement(react_router_1.Route, {path: "observatory", component: Observatory_1.Observatory}), React.createElement(react_router_1.Route, {path: "companies", component: PersonsByCompany_1.CompaniesCards}), React.createElement(react_router_1.Route, {path: "charts", component: Analytics_1.Analytics}), React.createElement(react_router_1.Route, {path: "target_sales", component: TargetSales_1.TargetSales}), React.createElement(react_router_1.Route, {path: "requests_viewer", component: RequestsViewer_1.RequestsViewer}), React.createElement(react_router_1.Route, {path: "search_results", component: SearchResults_1.SearchResults})), React.createElement(react_router_1.Route, {path: "/app/index.html", component: InsiderOilApp})), document.getElementById('insideroilapp'));
