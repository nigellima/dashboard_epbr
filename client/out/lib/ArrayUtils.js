"use strict";
function find(list, func) {
    for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
        var val = list_1[_i];
        if (func(val)) {
            return val;
        }
    }
    return null;
}
exports.find = find;
