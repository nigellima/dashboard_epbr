"use strict";
var Promise = require('bluebird');
function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}
exports.arrayBufferToBase64 = arrayBufferToBase64;
function base64ToArray(base64) {
    var base64str = atob(base64);
    var bytes = new Array(base64str.length);
    for (var i = 0; i < base64str.length; i++) {
        bytes[i] = base64str.charCodeAt(i);
    }
    return bytes;
}
exports.base64ToArray = base64ToArray;
function removeBase64Header(dsBase64) {
    return dsBase64.substring(dsBase64.search(';base64,') + 8, dsBase64.length);
}
exports.removeBase64Header = removeBase64Header;
function StrToByteArray(str) {
    var buf = new ArrayBuffer(str.length);
    var bufView = new Uint8Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}
exports.StrToByteArray = StrToByteArray;
function ReadFileToBase64Str(file) {
    function onFileLoad(reader, resolve) {
        var dsBase64 = reader.result;
        var trimmedBase64 = removeBase64Header(dsBase64);
        resolve(trimmedBase64);
        return null;
    }
    return new Promise(function (resolve, reject) {
        var reader = new FileReader();
        reader.onloadend = onFileLoad.bind(this, reader, resolve);
        reader.readAsDataURL(file);
        return null;
    });
}
exports.ReadFileToBase64Str = ReadFileToBase64Str;
