"use strict";
var moment = require('moment');
require('moment/locale/pt-br');
function dateFormat(dateStr) {
    if (!dateStr)
        return '';
    var result = moment(dateStr).utcOffset(0).format('DD/MM/YYYY');
    return result;
}
exports.dateFormat = dateFormat;
function dateTimeFormat(dateTimeStr) {
    if (!dateTimeStr)
        return '';
    var result = moment(dateTimeStr).format('DD/MM/YYYY HH:mm');
    return result;
}
exports.dateTimeFormat = dateTimeFormat;
function dateFormatInsight(dateStr) {
    var result = moment(dateStr).format('DD MMM YYYY');
    return result;
}
exports.dateFormatInsight = dateFormatInsight;
