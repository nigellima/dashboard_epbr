"use strict";
var Promise = require('bluebird');
exports.googleRef = google;
var isChartsLoaded = false;
function loadCharts() {
    if (isChartsLoaded) {
        return Promise.resolve();
    }
    return new Promise(function (resolve) {
        exports.googleRef.charts.load('current', { packages: ['corechart', 'bar', 'line'], 'language': 'pt-br' });
        exports.googleRef.charts.setOnLoadCallback(function () {
            isChartsLoaded = true;
            resolve();
        });
    });
}
exports.loadCharts = loadCharts;
