"use strict";
var Promise = require('bluebird');
var server = require('./Server');
exports.rioDeJaneiroCoords = { lat: -23.0, lng: -43.0 };
function showBillboard(object, modelName, billboardHTMLfn) {
    return new Promise(function (resolve, reject) {
        var req = {
            queryName: 'NewsByObject',
            filters: {
                modelName: modelName,
                id: object.id
            }
        };
        server.getQueryData(req)
            .then(function (res) {
            var result = billboardHTMLfn(object);
            if (res.records.length > 0) {
                result += '<hr/><b>Notícias:</b>';
            }
            for (var _i = 0, _a = res.records; _i < _a.length; _i++) {
                var newsRecord = _a[_i];
                var newsUrl = '/app/view_record?source=News&id=' + newsRecord.id;
                result += '<br/><a href="' + newsUrl + '">' + newsRecord.title + '</a>';
            }
            resolve(result);
        }).catch(reject);
    });
}
exports.showBillboard = showBillboard;
