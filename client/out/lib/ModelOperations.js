"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var react_router_1 = require('react-router');
var server = require('./Server');
var BaseModelOperations = (function () {
    function BaseModelOperations(modelName) {
        this.modelName = modelName;
    }
    BaseModelOperations.prototype.editRecord = function (id) {
        var queryStr = "/app/edit_item?modelName=" + this.modelName + '&id=' + id;
        react_router_1.browserHistory.push(queryStr);
    };
    BaseModelOperations.prototype.createItem = function () {
        var queryStr = "/app/edit_item?modelName=" + this.modelName;
        react_router_1.browserHistory.push(queryStr);
    };
    BaseModelOperations.prototype.deleteItem = function (id, onDelete, onError) {
        server.deleteItem(this.modelName, id, onDelete, onError);
    };
    return BaseModelOperations;
}());
var NewsOperations = (function (_super) {
    __extends(NewsOperations, _super);
    function NewsOperations() {
        _super.apply(this, arguments);
    }
    NewsOperations.prototype.editRecord = function (id) {
        var queryStr = "/app/create_news?id=" + id;
        react_router_1.browserHistory.push(queryStr);
    };
    NewsOperations.prototype.createItem = function () {
        react_router_1.browserHistory.push("/app/create_news");
    };
    return NewsOperations;
}(BaseModelOperations));
var ProjectOperations = (function (_super) {
    __extends(ProjectOperations, _super);
    function ProjectOperations() {
        _super.apply(this, arguments);
    }
    ProjectOperations.prototype.editRecord = function (id) {
        var queryStr = "/app/create_project?id=" + id;
        react_router_1.browserHistory.push(queryStr);
    };
    ProjectOperations.prototype.createItem = function () {
        react_router_1.browserHistory.push("/app/create_project");
    };
    return ProjectOperations;
}(BaseModelOperations));
function getModelOperations(modelName) {
    var customModels = {
        'News': NewsOperations,
        'Project': ProjectOperations,
    };
    var moClass = customModels[modelName];
    if (!moClass) {
        moClass = BaseModelOperations;
    }
    return new moClass(modelName);
}
exports.getModelOperations = getModelOperations;
