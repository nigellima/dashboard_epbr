"use strict";
var DateService = require('./DateUtils');
exports.datatablesPtBrTranslation = {};
function formatExcelUploadResult(response) {
    var statusStr = response.status.replace(/\n/g, '<br>');
    if (response.recordsStatus) {
        for (var i = 0; i < response.recordsStatus.length; i++) {
            statusStr += '<br>' + response.recordsStatus[i];
        }
    }
    return statusStr;
}
exports.formatExcelUploadResult = formatExcelUploadResult;
function getColumns(viewParams, types) {
    var columns = [];
    for (var i = 0; i < viewParams.gridFields.length; i++) {
        var fieldName = viewParams.gridFields[i];
        if (fieldName == 'id')
            continue;
        var fieldLabel = viewParams.fields[fieldName].label;
        var columnObj = {
            title: fieldLabel,
            data: fieldName
        };
        if (types[fieldName] == "DATE")
            columnObj.render = { display: DateService.dateFormat };
        columns.push(columnObj);
    }
    return columns;
}
exports.getColumns = getColumns;
function formatCurrency(value) {
    if (!value)
        return '';
    var opts = {
        style: 'decimal',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    };
    return value.toLocaleString('pt-BR', opts);
}
exports.formatCurrency = formatCurrency;
function formatNumber(value) {
    if (!value)
        return '';
    var opts = {
        style: 'decimal',
        maximumFractionDigits: 2
    };
    return value.toLocaleString('pt-BR', opts);
}
exports.formatNumber = formatNumber;
function formatFnByType(field) {
    if (!field)
        return function (value) { return ''; };
    if (field.type == 'DATE') {
        return DateService.dateFormat;
    }
    else if (field.type == 'DATETIME') {
        return DateService.dateTimeFormat;
    }
    else if (field.isCurrency || field.type == 'CURRENCY') {
        return formatCurrency;
    }
    else if (field.type == 'FLOAT' || field.type == 'DOUBLE') {
        return formatNumber;
    }
    else {
        return function (value) { return value; };
    }
}
exports.formatFnByType = formatFnByType;
function formatByType(field) {
    var fn = formatFnByType(field);
    return fn(field.value);
}
exports.formatByType = formatByType;
