"use strict";
var Server_1 = require('./Server');
var Promise = require('bluebird');
var QueryDataIncrementalLoading = (function () {
    function QueryDataIncrementalLoading(queryName, itemsPerPage) {
        this.queryName = queryName;
        this.itemsPerPage = itemsPerPage;
        this.waitingData = false;
        this.filters = [];
        this.count = 9999999;
        this.records = [];
    }
    QueryDataIncrementalLoading.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.waitingData || (_this.records.length >= _this.count)) {
                return;
            }
            var req = {
                queryName: _this.queryName,
                queryParams: {
                    order: [],
                    filters: _this.filters,
                    pagination: {
                        first: _this.records.length,
                        itemsPerPage: _this.itemsPerPage
                    }
                }
            };
            _this.waitingData = true;
            Server_1.getP('/get_table_data', req)
                .then(_this.onData.bind(_this, resolve))
                .catch(reject);
        });
    };
    QueryDataIncrementalLoading.prototype.search = function (filter) {
        this.count = 9999999;
        this.records = [];
        if (filter.like == '') {
            this.filters = [];
        }
        else {
            this.filters = [filter];
        }
        return this.getData();
    };
    QueryDataIncrementalLoading.prototype.onData = function (resolve, data) {
        this.count = data.count;
        this.records = this.records.concat(data.records);
        this.waitingData = false;
        resolve(this.records);
        return null;
    };
    return QueryDataIncrementalLoading;
}());
exports.QueryDataIncrementalLoading = QueryDataIncrementalLoading;
