"use strict";
var session = require('./session');
var jquery = require('jquery');
var Promise = require('bluebird');
function ajax(url, data, type, onSuccess, onError) {
    data.token = session.getToken();
    var ajaxOpt = {
        url: url,
        data: data,
        type: type,
    };
    if (onSuccess) {
        ajaxOpt.success = onSuccess;
    }
    if (onError) {
        ajaxOpt.error = onError;
    }
    jquery.ajax(ajaxOpt);
}
function get(url, data, onSuccess, onError) {
    ajax(url, data, 'GET', onSuccess, onError);
}
function getP(url, data) {
    function onError(reject, error) {
        reject(error);
        if (error.status == 401) {
            window.location.replace('/login');
        }
    }
    return new Promise(function (resolve, reject) {
        ajax(url, data, 'GET', function (result) { resolve(result); }, onError.bind(this, reject));
    });
}
exports.getP = getP;
function putP(url, data) {
    return new Promise(function (resolve, reject) {
        ajax(url, data, 'PUT', function (result) { resolve(result); }, function (error) { reject(error); });
    });
}
exports.putP = putP;
function postP(url, data) {
    return new Promise(function (resolve, reject) {
        ajax(url, data, 'POST', function (result) { resolve(result); }, function (error) { reject(error); });
    });
}
exports.postP = postP;
function put(url, data, onSuccess, onError) {
    ajax(url, data, 'PUT', onSuccess, onError);
}
function post(url, data, onSuccess, onError) {
    ajax(url, data, 'POST', onSuccess, onError);
}
function deleteHTTP(url, data, onSuccess, onError) {
    ajax(url, data, 'DELETE', onSuccess, onError);
}
function getUserDetails(onData, onError) {
    get('/user/details', {}, onData, onError);
}
exports.getUserDetails = getUserDetails;
function getQueryData(params) {
    return getP('/get_query_data', params);
}
exports.getQueryData = getQueryData;
function getTableData(req) {
    return getP('/get_table_data', req);
}
exports.getTableData = getTableData;
function getTimeSeries(options) {
    return getP('/time_series', options);
}
exports.getTimeSeries = getTimeSeries;
function getModelFields(model) {
    var params = { model: model, };
    return getP('/model_fields/', params);
}
exports.getModelFields = getModelFields;
function deleteItem(modelName, id, onDelete, onError) {
    var params = {
        model: modelName,
        id: id,
    };
    deleteHTTP('/delete_item/', params, onDelete, onError);
}
exports.deleteItem = deleteItem;
function getSearchResult(searchValue) {
    var params = { searchValue: searchValue };
    return getP('/search', params);
}
exports.getSearchResult = getSearchResult;
function viewRecord(dataSource, id) {
    var params = { dataSource: dataSource, id: id };
    console.log(params);
    return getP('/view_record/', params);
}
exports.viewRecord = viewRecord;
function sourceList(onData, onError) {
    get('/sources_list/', {}, onData, onError);
}
exports.sourceList = sourceList;
function changePassword(oldPassword, newPassword) {
    var params = { oldPassword: oldPassword, newPassword: newPassword };
    return putP('/user/change_password', params);
}
exports.changePassword = changePassword;
function downloadExcelFile(dataSource, onData, onError) {
    var params = { dataSource: dataSource };
    get('/download_excel', params, onData, onError);
}
exports.downloadExcelFile = downloadExcelFile;
function importFromURL(dataSource, onData, onError) {
    var params = { dataSource: dataSource };
    put('/import_from_url', params, onData, onError);
}
exports.importFromURL = importFromURL;
exports.paths = {
    baseImg: 'https://s3-sa-east-1.amazonaws.com/insider-oil/images/'
};
