"use strict";
function format() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
    var num = arguments.length;
    var oStr = arguments[0];
    for (var i = 1; i < num; i++) {
        var pattern = "\\{" + (i - 1) + "\\}";
        var re = new RegExp(pattern, "g");
        oStr = oStr.replace(re, arguments[i]);
    }
    return oStr;
}
exports.format = format;
function strContains(input, substr) {
    return input.indexOf(substr) > -1;
}
exports.strContains = strContains;
function formatUrlParams(params) {
    var result = '?';
    for (var key in params) {
        result += key + '=' + params[key] + '&';
    }
    result = result.substr(0, result.length - 1);
    return result;
}
exports.formatUrlParams = formatUrlParams;
