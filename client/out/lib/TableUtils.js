"use strict";
var ModelViewService = require('./ModelViewUtils');
var StringUtils = require('./StringUtils');
function genColumns(tableParams) {
    var columns = [];
    var currencyColumnsIndexes = [];
    for (var i = 0; i < tableParams.fields.length; i++) {
        var field = tableParams.fields[i];
        var columnObj = {
            title: field.label,
            data: '',
            render: {}
        };
        if (field.ref) {
            columnObj.data = field.ref.valueField,
                columnObj.render = { display: this.getFormatLinkFn(field) };
        }
        else {
            columnObj.data = field.fieldName;
            columnObj.render = { display: ModelViewService.formatFnByType(field) };
            if (field.type == 'CURRENCY') {
                currencyColumnsIndexes.push(i);
            }
        }
        columns.push(columnObj);
    }
    return { columns: columns, currencyColumnsIndexes: currencyColumnsIndexes };
}
exports.genColumns = genColumns;
function getFormatLinkFn(column) {
    return function (value, type, row) {
        if (!value)
            return '';
        var paramsStr = StringUtils.format("'{0}', {1}", row[column.ref.modelField], row[column.ref.idField]);
        var linkStr = '<a href="" onclick="window.paginatedTableRef.followLink(' +
            paramsStr + '); return false;">' + value + '</a>';
        return linkStr;
    };
}
exports.getFormatLinkFn = getFormatLinkFn;
