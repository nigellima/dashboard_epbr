"use strict";
var cookie = require('js-cookie');
var jquery = require('jquery');
var showError = require('./ShowError');
var react_router_1 = require('react-router');
function login(token) {
    cookie.set('token', token);
}
exports.login = login;
function logout() {
    var token = cookie.get('token');
    cookie.set('token', '');
    jquery.ajax({
        url: '/login/logout',
        type: 'PUT',
        data: { token: token },
        success: logoutOk,
        error: logoutError
    });
    function logoutOk(response) {
        react_router_1.browserHistory.push('/');
    }
    function logoutError(xhr, ajaxOptions, thrownError) {
        showError.show("Error on logout: " + xhr.responseText);
    }
}
exports.logout = logout;
;
function getToken() {
    return cookie.get('token');
}
exports.getToken = getToken;
