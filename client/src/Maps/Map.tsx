import * as React from 'react';
import * as showError from '../lib/ShowError';
import { googleRef } from '../lib/Google';
import { Tableau } from '../Components/Tableau'; 

export interface IMapObj {
    gMap:any;
    infoWindow:any;
}

interface IAppProps {
    initialState: any;
    receiveMapObj: any;
    style?: any;
}

interface IAppState {
}

export class Map extends React.Component<IAppProps, IAppState> {

    private mapObj: IMapObj;

    constructor(props: IAppProps) {
        super(props);

        this.state = {};
    }

    private componentDidMount() {
        this.initMap();
    }    

    private initMap() {
        this.mapObj = {
            infoWindow: new googleRef.maps.InfoWindow,
            gMap: new googleRef.maps.Map(
                                    document.getElementById('map'), 
                                    this.props.initialState)
        }
        this.props.receiveMapObj(this.mapObj);
    }

    public render(): React.ReactElement<any> {
        const style = {
            width: 100 + '%',
            height: 700
        }
        return (
            <div>
                {/* <div id="map" style={style} /> */}

                {/* <iframe src="https://epbr.com.br/mapas/dashboard/index.html#6/-22.269/-40.671" style={style}></iframe>  */}
                <div>
                    <Tableau 
                        vizUrl='https://public.tableau.com/views/DashboardFPSOepbrmap/DashboardEPmap?:embed=y&:display_count=yes&publish=yes'
                        style={style}
                    />
                    <br/>
                </div>
            </div>
        );
    }
}