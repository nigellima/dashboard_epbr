'use strict';
import Sequelize = require('sequelize');

module.exports = {
  up: function (queryInterface: Sequelize.QueryInterface, SequelizeVar: Sequelize.Sequelize) {
    const tableOpts: Sequelize.DefineAttributes = {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      main_fluid: {
        type: Sequelize.STRING,
        allowNull: false
      },
      // compression: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // eHouse: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // power_generation: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // automation_scope: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      fpso_award: {
        type: Sequelize.STRING,
        allowNull: false
      },
      oil_field: {
        type: Sequelize.STRING,
        allowNull: false
      },
      block: {
        type: Sequelize.STRING,
        allowNull: false
      },
      owner: {
        type: Sequelize.STRING,
        allowNull: false
      },
      former_owner: {
        type: Sequelize.STRING,
        allowNull: false
      },
      awarded_contractor: {
        type: Sequelize.STRING,
        allowNull: false
      },
      // hull_owner: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // eng_company: {
      //   type: Sequelize.STRING,
      //   allowNull: false
      // },
      // oil_field_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'oil_fields',
      //     key: 'id'
      //   }
      // },
      // block_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'blocks',
      //     key: 'id'
      //   }
      // },
      // owner_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'companies',
      //     key: 'id'
      //   }
      // },
      // former_owner_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'companies',
      //     key: 'id'
      //   }
      // },
      // awarded_contractor_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'companies',
      //     key: 'id'
      //   }
      // },
      // hull_owner_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'companies',
      //     key: 'id'
      //   }
      // },
      // eng_company_id: {
      //   type: Sequelize.INTEGER,
      //   allowNull: true,
      //   references: {
      //     model: 'companies',
      //     key: 'id'
      //   }
      // },
      type: {
        type: Sequelize.ENUM,
        values: ['FPSO'],
        defaultValue: 'FPSO'
      },
      hull_type: {
        type: Sequelize.ENUM(),
        values: ['New Build', 'Converted'],
        defaultValue: 'New Build'
      },
      status: {
        type: Sequelize.ENUM('Operational',
                              'Installed',
                              'Awarded',
                              'Ongoing BID',
                              'FID/Petrobras Businnes Plan',
                              'Market Forecast',
                              'Planned Decommissioning',
                              'Under Decommissioning',
                              'Decommissioned',
                              'Planned by Operator'),
        allowNull: true
      },
      coordinates: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      general_info: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      oil_processing_capacity: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      gas_processing_capacity: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      co2_percentage: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      liquids_processing_capacity: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      local_content_percentage: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      first_oil: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      contractors_deadline: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    };

    return queryInterface.createTable('production_units', tableOpts);
  },

  down: function (queryInterface: Sequelize.QueryInterface, SequelizeVar: Sequelize.Sequelize) {
    return queryInterface.dropTable('production_units');
  }
};
