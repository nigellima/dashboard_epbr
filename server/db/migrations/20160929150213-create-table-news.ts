'use strict';
import Sequelize = require('sequelize');

module.exports = {
    up: function (queryInterface: Sequelize.QueryInterface, SequelizeVar: Sequelize.Sequelize) {
        const fields:Sequelize.DefineAttributes = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: Sequelize.STRING,
                allowNull: false
            },
            content: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            tableau_url: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            production_unit_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'production_units',
                    key: 'id'
                }
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
        };
        console.log('news up');
	      return queryInterface.createTable("news", fields);
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("news");
    }
};
