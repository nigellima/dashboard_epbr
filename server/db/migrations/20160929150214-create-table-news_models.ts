'use strict';
import Sequelize = require('sequelize');

module.exports = {
    up: function (queryInterface: Sequelize.QueryInterface, SequelizeVar: Sequelize.Sequelize) {
        const fields:Sequelize.DefineAttributes = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            model_ref_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            news_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'news',
                    key: 'id'
                },
                onDelete: 'CASCADE'
            },
            model_name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
        };
	      return queryInterface.createTable("news_models", fields);
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("news_models");
    }
};
