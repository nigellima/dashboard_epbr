'use strict';
import Sequelize = require('sequelize');
import { await } from '../../lib/await';
import ModelUtils = require('../../lib/ModelUtils');
import { IFrontEndProject } from '../../../common/Interfaces';

function updateBid(bid) {
    const object:IFrontEndProject[] = bid.dataValues.object;
    if(object == null || object.length != 1) {
        bid.model_name = null;
        bid.obj_id = null;
        return;   
    }
    
    bid.model_name = object[0].model;
    bid.obj_id = object[0].id;
}

function defineHooks(db) {
	db.Bid.hook('beforeCreate', updateBid);
	db.Bid.hook('beforeUpdate', updateBid);
}


module.exports = function (sequelize, DataTypes: Sequelize.DataTypes) {
    
    const Bid = sequelize.define('Bid', { 
        operator: {
            type: DataTypes.STRING,
            allowNull: true
        },
        scope: {
            type: DataTypes.STRING,
            allowNull: true
        },
        published: {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        subject: {
            type: DataTypes.STRING,
            allowNull: true
        },
        update: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        deadline: {
            type: Sequelize.STRING,
            allowNull: true
        },
        companies: {
            type: Sequelize.STRING,
            allowNull: true
        },
    },
        {
            underscored: true,
            tableName: 'bids',
            classMethods: {
                associate: function (models) {
                    const ProductionUnitOpts:Sequelize.AssociationOptionsBelongsTo = {
                        as: 'production_unit', 
                        foreignKey: {  allowNull: true }
                    };
                    Bid.belongsTo(models.ProductionUnit, ProductionUnitOpts );
                },
			    defineHooks: defineHooks
            }
        }
    );
    return Bid;
};