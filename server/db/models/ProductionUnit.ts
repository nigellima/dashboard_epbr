'use strict';
import * as Sequelize from 'sequelize'; 
import { saveOriginalImage, getCoordFieldObj } from '../../lib/ModelUtils';

function savePhoto(productionUnit) {
	  saveOriginalImage(productionUnit.dataValues.photo, 'ProductionUnit', productionUnit.id);
}

module.exports = function(sequelize:Sequelize.Sequelize, DataTypes:Sequelize.DataTypes) {
    const ProductionUnit = sequelize.define('ProductionUnit', {
        name: {
          type: DataTypes.STRING,
          allowNull: false
        }, 
        type: {
            type: DataTypes.ENUM('FPSO'),
            defaultValue: 'FPSO',
            allowNull: false
        },
        hull_type: {
            type: DataTypes.ENUM('New Build', 'Converted'),
            defaultValue: 'New Build',
            allowNull: false
        },
        status: {
            type: DataTypes.ENUM('Operational',
                                'Installed',
                                'Awarded',
                                'Ongoing BID',
                                'FID/Petrobras Businnes Plan',
                                'Market Forecast',
                                'Planned Decommissioning',
                                'Under Decommissioning',
                                'Decommissioned',
                                'Planned by Operator'),
            allowNull: true
        },
        coordinates: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        coords_admin: getCoordFieldObj('coordinates'),
        general_info: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        main_fluid: {
          type: DataTypes.STRING,
          allowNull: true
        },
        fpso_award: {
            type: DataTypes.STRING,
            allowNull: true
        },
        oil_processing_capacity: {
            type: DataTypes.FLOAT,
	        allowNull: true
        },
        co2_percentage: {
            type: DataTypes.FLOAT,
	        allowNull: true
        },
        gas_processing_capacity: {
            type: DataTypes.FLOAT,
	        allowNull: true
        },
        liquids_processing_capacity: { //oil_storage_capacity
            type: DataTypes.FLOAT,
	        allowNull: true
        },  
        local_content_percentage: {
          type: DataTypes.FLOAT,
          allowNull: true
        },
        contractors_deadline: { //start
          type: DataTypes.DATEONLY,
          allowNull: true
        },
        first_oil: {
            type: Sequelize.STRING,
            allowNull: true
        },  
        oil_field: {
            type: DataTypes.STRING,
            allowNull: true
        },
        block: {
            type: DataTypes.STRING,
            allowNull: true
        },
        owner: {
            type: DataTypes.STRING,
            allowNull: true
        },
        former_owner: {
            type: DataTypes.STRING,
            allowNull: true
        },
        awarded_contractor: {
            type: DataTypes.STRING,
            allowNull: true
        },
		// photo: {
        //     type: DataTypes.VIRTUAL,
        //     get: function() {
        //         return 'image';
        //     },
        //     allowNull: true
		// },
    }, 
    {  
        underscored: true,
        tableName: 'production_units',
        classMethods: {
            associate: function(models) {
                // const oilFieldOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'oil_field', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.OilField, oilFieldOpts );

                // const blockOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'block', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Block, blockOpts );

                // const ownerOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'owner', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Company, ownerOpts );

                // const formerOwnerOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'former_owner', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Company, formerOwnerOpts );

                // const awardedOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'awarded_contractor', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Company, awardedOpts );

                // const hullOwnerOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'hull_owner', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Company, hullOwnerOpts );

                // const EngineeringCompanyOpts:Sequelize.AssociationOptionsBelongsTo = {
                //     as: 'eng_company', 
                //     foreignKey: {  allowNull: true }
                // };
                // ProductionUnit.belongsTo(models.Company, EngineeringCompanyOpts );
            },
        },
        hooks: {
            afterCreate: savePhoto,
            beforeUpdate: savePhoto
        }
    }
  );
  return ProductionUnit;
};