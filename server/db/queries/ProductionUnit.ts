import { 
    IBaseDataSourceParams, 
    IBaseQueryField, 
    IQueryParams 
} from '../../../common/Interfaces';
import * as TableQueries from './TableQueries';
import * as QueryGenerator from './QueryGenerator';

const productionUnit:TableQueries.ITableQuery = {
    title: 'Production Units',
    queryStrFn: (queryParams: IQueryParams) => {
        console.log('getting fpsos');
        const options:QueryGenerator.IQueryOpts = {
            table: {
                name: 'production_units',
                fields: [
                    'id',
                    ['name', 'name'],
                    'first_oil',
                    'status',
                    'block',
                    'oil_field',
                    'owner',
                    'oil_processing_capacity',
                    'gas_processing_capacity',
                    'awarded_contractor',
                    'former_owner',
                    'main_fluid',
                    'co2_percentage',
                    'local_content_percentage',
                    'contractors_deadline',
                    'fpso_award',
                    'hull_type',
                ]
            },
            joinTables: [
                // {
                //     name: 'oil_fields',
                //     fields: [
                //         ['id', 'of_id'],
                //         ['name', 'of_name'],
                //     ],
                //     joinField: 'production_units.oil_field_id'
                // },
                // {
                //     name: 'blocks',
                //     fields: [
                //         ['id', 'b_id'],
                //         ['name', 'b_name'],
                //     ],
                //     joinField: 'production_units.block_id'
                // },
                // {
                //     name: ['companies', 'owner'],
                //     fields: [
                //         ['id', 'ow_id'],
                //         ['name', 'ow_name'],
                //     ],
                //     joinField: 'production_units.owner_id'
                // },
            ],
            extraFields: [
                ['"ProductionUnit"', 'model'],
                ['"OilField"', 'of_model'],
                ['"Block"', 'b_model'],
            ],
            where: queryParams.filters,
            order: queryParams.order
        };
        
        return QueryGenerator.generate(options);
    },
    fields: [
        {
            label: 'Unit Name',
            fieldName: 'name',
            type: 'VARCHAR'
        },
        {
            label: 'Status',
            fieldName: 'status',
            type: 'ENUM',
            hasFilter: true
        },
        {
            label: 'Field Name',
            fieldName: 'oil_field',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'Exploration Block',
            fieldName: 'block',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'Field Ownership',
            fieldName: 'owner',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'FPSO award (expected)',
            fieldName: 'fpso_award',
            hasFilter: true
        },
        // {
        //     label: 'Former Field Ownership',
        //     fieldName: 'former_owner',
        //     type: 'VARCHAR',
        //     hasFilter: true
        // },
        // {
        //     label: 'Field\'s Main Fluid',
        //     fieldName: 'main_fluid',
        //     type: 'VARCHAR',
        //     hasFilter: true
        // },
        // {
        //     label: '% of CO2',
        //     fieldName: 'co2_percentage',
        //     type: 'FLOAT',
        //     hasFilter: true
        // },
        // {
        //     label: 'Oil Processing Capacity (b/d) ',
        //     fieldName: 'oil_processing_capacity',
        //     type: 'FLOAT',
        //     hasFilter: true
        // },
        // {
        //     label: 'Liquids Processing Capacity (b/d)',
        //     fieldName: 'liquids_processing_capacity',
        //     type: 'FLOAT',
        //     hasFilter: true
        // },
        // {
        //     label: 'Gas Compression Capacity (m³/d)',
        //     fieldName: 'gas_processing_capacity',
        //     type: 'FLOAT',
        //     hasFilter: true
        // },
        // {
        //     label: 'Predictable Quarter for First Oil',
        //     fieldName: 'first_oil',
        //     type: 'DATE',
        //     hasFilter: true
        // },
        // {
        //     label: 'Local Content for Development or Production (%)',
        //     fieldName: 'local_content_percentage',
        //     type: 'FLOAT',
        //     hasFilter: true
        // },
        // {
        //     label: 'Contractor-bidders deadline for FPSO RFQ delivery ',
        //     fieldName: 'contractors_deadline',
        //     hasFilter: true
        // },
        
        {
            label: 'Awarded Contractor',
            fieldName: 'awarded_contractor',
            hasFilter: true
        },
        // {
        //     label: 'Hull type',
        //     fieldName: 'hull_type',
        //     hasFilter: true
        // },
        // {
        //     label: 'Hull Owner',
        //     fieldName: 'hull_owner',
        //     hasFilter: true
        // },
        // {
        //     label: 'Engineering Company for Pre-FEED and FEED',
        //     fieldName: 'eng_company',
        //     hasFilter: true
        // },
        // {
        //     label: 'Compression',
        //     fieldName: 'compression',
        //     hasFilter: true
        // },
        // {
        //     label: 'E-House',
        //     fieldName: 'eHouse',
        //     hasFilter: true
        // },
        // {
        //     label: 'Power Generation',
        //     fieldName: 'power_generation',
        //     hasFilter: true
        // },
        // {
        //     label: 'Automation Scope',
        //     fieldName: 'automation_scope',
        //     hasFilter: true
        // },
    ]
};

export const FPSOs:TableQueries.ITableQuery = {
    title: 'FPSOs',
    queryStrFn: (queryParams: IQueryParams) => {
        queryParams.filters.push(
            {
                field: 'type',
                equal: '"FPSO"'
            }
        );
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/DashboardFPSOepbr/DashboardEPBrazillianFPSOs?:embed=y&:display_count=yes&publish=yes',
};
    
export const FixedProductionUnits:TableQueries.ITableQuery = {
    title: 'Plataformas fixas',
    queryStrFn: (queryParams: IQueryParams) => {
        queryParams.filters.push(
            {
                field: 'type',
                equal: '"FIXED"'
            }
        );
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/Fixas/Painel1?:embed=y&:display_count=yes&:toolbar=no'
};
    
export const SemiSubmersibleProductionUnits:TableQueries.ITableQuery = {
    title: 'Semi-subversíveis',
    queryStrFn: (queryParams: IQueryParams) => {
        queryParams.filters.push(
            {
                field: 'type',
                equal: '"SEMI"'
            }
        );
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/Semi/Painel1?:embed=y&:display_count=yes&:toolbar=no',
};