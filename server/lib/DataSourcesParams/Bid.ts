import { IBaseDataSourceParams } from '../../../common/Interfaces';

const Bid: IBaseDataSourceParams = {
    fields: {
        operator: {
            label: "Operator"
        },
        scope: {
            label: "Scope"
        },
        published: {
            label: "Published"
        },
        production_unit_id: {
            label: 'Production Unit'
        },
        subject: {
            label: 'Subject'
        },
        update: {
            label: 'Update'
        },
        deadline: {
            label: 'Deadline'
        },
        companies: {
            label: 'Companies'
        },
        production_unit_name: {
            label: 'Production Unit'
        },
    },
    labelField: "production_unit_id",
    gridFields: ["production_unit_id", "operator", "scope", 'published', 'update', 'deadline', 'subject', 'companies'],
    tableLabel: "BID",
    labelSingular: 'BID',
    excelParams: {
        keyField: 'processo',
        fields: {
            'operator': 'operator',
            'scope': 'scope',
            'published': 'published',
            'subject': 'subject',
            'update': 'update',
            'deadline': 'deadline',
            'companies': 'companies',
            'production_unit_name': 'production_unit_name',
            
        }
    },
}

export = Bid;