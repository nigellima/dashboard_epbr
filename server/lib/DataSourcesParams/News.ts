import { IBaseDataSourceParams } from '../../../common/Interfaces';

const News: IBaseDataSourceParams = {
    fields: {
        title: {
            label: 'Title'
        },
        content: {
            label: 'Content',
            isHTML: true
        },
        production_unit_id: {
            label: 'Production Unit'
        },
        production_unit_name: {
            label: 'Production Unit'
        },
        created_at: {
        	label: 'Created at'
        }
    },
    labelField: 'title',
    gridFields: ['title', 'production_unit_name', 'created_at'],
    tableLabel: 'Insights',
    labelSingular: 'Insight',
}

export = News;