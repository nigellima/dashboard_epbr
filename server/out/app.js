'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
const winston = require("winston");
var umzug = require('./lib/InitUmzug');
var app = express();
const db = require("./db/models");
umzug.up();
if (app.get('env') == 'production') {
    winston.remove(winston.transports.Console);
    winston.level = 'error';
    const options = {
        level: 'error',
        silent: true
    };
    const errorLogger = winston.add(winston.transports.Console, options);
    errorLogger.on('logging', function (transport, level, msg) {
        db.models.ErrorLog.create({ error: msg });
        console.error(msg);
    });
}
else if (app.get('env') == 'development') {
    winston.level = 'debug';
    winston.add(winston.transports.File, { filename: 'log/development.log' });
}
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'jade');
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.raw({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '20mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));
app.use('/app/ext_libs', express.static(path.join(__dirname, '/../../client/node_modules')));
app.use('/app', express.static(path.join(__dirname, '/../../client/dist')));
routes(app);
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
if (app.get('env') === 'development') {
    app.use(function (err, req, res) {
        winston.error(err.stack);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
process.on('uncaughtException', function (err) {
    winston.error(err.stack);
});
module.exports = app;
//# sourceMappingURL=app.js.map