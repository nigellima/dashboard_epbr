'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
var Sync = require('sync');
const await_1 = require("../lib/await");
const ControllerUtils = require("../lib/ControllerUtils");
const dbUtils = require("../lib/dbUtils");
const dsParams = require("../lib/DataSourcesParams");
const DataSourceOperations = require("../lib/DataSourceOperations/index");
const ComboQueries = require("../db/queries/ComboQueries");
const ImportKML = require("../lib/ImportKml");
const ModelUtils_1 = require("../lib/ModelUtils");
function getFieldTypes(fields) {
    const types = {};
    for (var i = 0; i < fields.length; i++) {
        types[fields[i].name] = fields[i].type;
    }
    return types;
}
function getViewParams(req, res, next) {
    const query = req.query;
    const modelName = query.table;
    const dataSource = dbUtils.getDataSource(modelName);
    const viewParams = dsParams[dataSource.name];
    viewParams.gridFields.push('id');
    const dsOperations = DataSourceOperations[modelName];
    const fields = dsOperations.getModelFields(modelName);
    var types = getFieldTypes(fields);
    const ioRes = { viewParams, types };
    res.json(ioRes);
}
exports.getViewParams = getViewParams;
function getTableData(req, res, next) {
    Sync(function () {
        const query = req.query;
        const modelName = query.table;
        const filters = query.filters;
        const fieldNames = query.fieldNames;
        const order = query.order;
        const pagination = query.pagination;
        const dataSource = dbUtils.getDataSource(modelName);
        if (!dataSource) {
            ControllerUtils.getErrorFunc(res, 500, "Modelo não encontrado")({});
            return;
        }
        const viewParams = dsParams[dataSource.name];
        const filterObj = {};
        if (filters) {
            for (var filter of filters) {
                filterObj[filter.field] = { $like: '%' + filter.value + '%' };
            }
        }
        var orderObj = [];
        if (order) {
            orderObj = order.map((orderItem) => {
                return [orderItem.fieldName, orderItem.dir.toUpperCase()];
            });
        }
        var showFields = fieldNames;
        if (!showFields)
            showFields = viewParams.gridFields;
        const findOpts = {
            where: filterObj,
            order: orderObj,
        };
        if (pagination) {
            findOpts.offset = parseInt(pagination.first);
            findOpts.limit = parseInt(pagination.itemsPerPage);
        }
        const records = await_1.await(dbUtils.findAllCustom(dataSource, findOpts));
        dbUtils.simplifyArray(dataSource, records);
        const filteredRecords = dbUtils.filterShowFields(records, showFields);
        const count = await_1.await(dataSource.count({ where: filterObj }));
        const ioRes = {
            records: filteredRecords,
            count
        };
        res.json(ioRes);
    }, ControllerUtils.getErrorFunc(res, 500, "Erro"));
}
exports.getTableData = getTableData;
;
function modelFields(req, res, next) {
    var modelName = req.query.model;
    const dsOperations = DataSourceOperations[modelName];
    var fields = dsOperations.getModelFields(modelName);
    res.json({ fields: fields });
}
exports.modelFields = modelFields;
function recordValues(req, res, next) {
    Sync(function () {
        const query = req.query;
        var dsName = query.model;
        const dataSource = dbUtils.getDataSource(dsName);
        const dsOps = DataSourceOperations[dsName];
        const record = await_1.await(dataSource.findById(query.id));
        const dsOperations = DataSourceOperations[dsName];
        var fields = dsOperations.getModelFields(dsName);
        const response = {
            values: record,
            fields: fields,
            extraRecordData: await_1.await(dbUtils.loadExtraData(dsName, query.id))
        };
        res.json(response);
    }, ControllerUtils.getErrorFunc(res, 404, "Registro não encontrado"));
}
exports.recordValues = recordValues;
function createItem(req, res, next) {
    Sync(function () {
        const body = JSON.parse(req.body.data);
        var newItemData = body.newItemData;
        var modelName = body.model;
        var model = dbUtils.getDataSource(modelName);
        if (modelName === 'ProductionUnit') {
            if (newItemData.type == null)
                newItemData.type = 'FPSO';
            if (newItemData.hull_type == null)
                newItemData.hull_type = 'New Build';
            if (newItemData.status == null)
                newItemData.hull_type = 'Operational';
        }
        if (modelName === 'Bid') {
            if (newItemData.status == null || newItemData.status == undefined)
                newItemData.status = 'Open';
            if (newItemData.scope == null || newItemData.scope == undefined)
                newItemData.scope = 'Chartering';
        }
        db.sequelize.transaction(function (t) {
            return model.create(newItemData)
                .then(onCreate)
                .catch(function (err) {
                t.rollback();
                ControllerUtils.getErrorFunc(res, 400, "Não foi possível criar o registro.")(err);
            });
        });
        function onCreate(newRecord) {
            return __awaiter(this, void 0, void 0, function* () {
                yield dbUtils.saveExtraData(modelName, newRecord.id, body.extraRecordData);
                ControllerUtils.getOkFunc(res, "Registro criado com sucesso.")();
            });
        }
    }, ControllerUtils.getErrorFunc(res, 400, "Não foi possível criar o registro."));
}
exports.createItem = createItem;
function saveItem(req, res, next) {
    Sync(function () {
        const body = JSON.parse(req.body.data);
        var dsName = body.model;
        var recordData = body.record;
        var dataSource = dbUtils.getDataSource(dsName);
        const dsOps = DataSourceOperations[dsName];
        const record = await_1.await(dataSource.findById(recordData.id));
        await_1.await(ModelUtils_1.saveRecordUpdates(dsName, record, recordData));
        dsOps.addAttributesToRecord(record, recordData, dataSource);
        await_1.await(record.save());
        await_1.await(dbUtils.saveExtraData(dsName, recordData.id, body.extraRecordData));
        ControllerUtils.getOkFunc(res, "Registro salvo com sucesso.")();
    }, ControllerUtils.getErrorFunc(res, 400, "Não foi possível salvar o registro."));
}
exports.saveItem = saveItem;
function deleteItem(req, res) {
    Sync(function () {
        function hasReferencedObj(modelName) {
            const modelRefsOptions = {
                where: {
                    model_name: modelName,
                    model_ref_id: id
                }
            };
            const newsRefs = await_1.await(db.models.NewsModels.find(modelRefsOptions));
            const personRefs = await_1.await(db.models.PersonProjects.find(modelRefsOptions));
            return newsRefs || personRefs;
        }
        let id = req.body.id;
        let modelName = req.body.model;
        var model = dbUtils.getDataSource(modelName);
        const errorFunc = ControllerUtils.getErrorFunc(res, 404, "Não foi possível apagar o registro.");
        try {
            if (hasReferencedObj(modelName)) {
                errorFunc(`Existe uma referência a este objeto, portanto não pode ser deletado.`);
                return;
            }
            model.destroy({ where: { id: id } })
                .then(ControllerUtils.getOkFunc(res, 'Registro apagado com sucesso'))
                .catch(errorFunc);
        }
        catch (error) {
            errorFunc(error);
        }
    }, ControllerUtils.getErrorFunc(res, 404, "Não foi possível apagar o registro."));
}
exports.deleteItem = deleteItem;
function getComboValues(req, res) {
    const query = req.query;
    const modelName = query.model;
    const model = dbUtils.getDataSource(modelName);
    const viewParams = dsParams[modelName];
    var labelField = 'name';
    if (viewParams)
        labelField = viewParams.labelField;
    if (model) {
        const options = {
            attributes: ['id', labelField],
            order: [labelField]
        };
        model.findAll(options)
            .then(onValues)
            .catch(ControllerUtils.getErrorFunc(res, 500, "Não foi possí­vel carregar os registros."));
    }
    else {
        const queryStrGenerator = ComboQueries[modelName];
        const queryStr = queryStrGenerator();
        const simpleQueryType = { type: db.Sequelize.QueryTypes.SELECT };
        db.sequelize.query(queryStr, simpleQueryType)
            .then(onValues)
            .catch(ControllerUtils.getErrorFunc(res, 500, "Não foi possí­vel carregar os registros."));
    }
    function onValues(values) {
        var valuesArray = [];
        for (var value of values) {
            valuesArray.push({
                id: value.id,
                label: value[labelField]
            });
        }
        ;
        const result = {
            values: valuesArray
        };
        res.json(result);
    }
}
exports.getComboValues = getComboValues;
function sourcesList(req, res) {
    var list = {
        Basin: 'Bacias',
        Boat: 'Barcos de apoio',
        Block: 'Blocos',
        OilField: 'Campos',
        Contract: 'Contratos',
        MaintenanceDate: 'Datas de manutenção',
        ComercialDeclaration: 'Declarações de comercialidade',
        Company: 'Empresas',
        Fleet: 'Frota Transpetro',
        GasPipeline: 'Gasodutos',
        HydrocarbonEvidence: 'Indicíos de hidrocarbonetos',
        Bid: 'BIDs',
        ExcelImportLog: 'Log da importação de Excel',
        GasMovement: 'Movimentação de gasoduto',
        OilPipeline: 'Oleodutos',
        Person: 'Pessoas',
        Well: 'Poços',
        Production: 'Produção de poços',
        Project: 'Projetos',
        ErrorReport: 'Reports de erros ',
        Refinery: 'Refinarias',
        IndustrySegment: 'Segmentos',
        Seismic: 'Sísmica',
        DrillingRigOffshore: 'Sondas offshore',
        DrillingRigOnshore: 'Sondas onshore',
        Terminal: 'Terminais',
        ProductionUnit: 'Production Unit',
        User: 'Users',
    };
    res.json(list);
}
exports.sourcesList = sourcesList;
function uploadKml(req, res, next) {
    const model = req.body.model;
    const kmlStr = JSON.parse(req.body.data);
    const importFuncs = {
        Block: ImportKML.importBlocks,
        OilField: ImportKML.importOilFields
    };
    const importFunc = importFuncs[model];
    if (!importFunc) {
        throw 'Modelo não encontrado: ' + model;
    }
    importFunc(kmlStr)
        .then((status) => res.json({ status: status }))
        .catch(ControllerUtils.getErrorFunc(res, 500, "Erro"));
}
exports.uploadKml = uploadKml;
//# sourceMappingURL=AdminController.js.map