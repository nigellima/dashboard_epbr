'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ControllerUtils = require("../lib/ControllerUtils");
const Analytics = require("../lib/Analytics");
function getSources(req, res, next) {
    try {
        const resObj = { sources: Analytics.getSources() };
        res.json(resObj);
    }
    catch (err) {
        ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
    }
}
exports.getSources = getSources;
function getCountValues(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const query = JSON.parse(req.query.data);
            console.log(query.filters);
            const result = yield Analytics.getResult(query.source, query.groupField, query.valueField, query.maxNumItems, query.filters);
            const resObj = { result };
            res.json(resObj);
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getCountValues = getCountValues;
//# sourceMappingURL=AnalyticsController.js.map