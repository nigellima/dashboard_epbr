"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var Sync = require('sync');
var request = require('request');
const ControllerUtils = require("../lib/ControllerUtils");
const dbUtils = require("../lib/dbUtils");
const ExportExcel = require("../lib/excel/ExportExcel");
const importExcel = require("../lib/excel/importExcel");
const dsParams = require("../lib/DataSourcesParams");
const winston = require("winston");
const ExportExcelQuery = require("../lib/excel/ExportExcelQuery");
function downloadExcel(req, res, next) {
    Sync(function () {
        const dataSourceName = req.query.dataSource;
        const binaryWorkbook = ExportExcel.main(dataSourceName);
        res.set({ "Content-Disposition": 'attachment; filename="arquivo.xlsx"' });
        res.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.send(binaryWorkbook);
    });
}
exports.downloadExcel = downloadExcel;
function importExcelFromURL(req, res) {
    const dataSourceName = req.body.params.dataSource;
    const viewParams = dsParams[dataSourceName];
    const url = viewParams.urlSource;
    const errorFunc = ControllerUtils.getErrorFunc(res, 500, "Erro ao importar Excel da URL");
    const options = {
        url: url,
        encoding: null
    };
    request(options, function (error, excelResponse, body) {
        if (error || excelResponse.statusCode != 200) {
            errorFunc(error);
            return;
        }
        importExcel(body, dataSourceName).then(onOk).catch(errorFunc);
    });
    function onOk(result) {
        res.json({ status: result.status, recordsStatus: result.invalidRecordsStatus });
    }
}
exports.importExcelFromURL = importExcelFromURL;
function uploadFile(req, res, next) {
    var buf = JSON.parse(req.body.data);
    var model = req.body.model;
    try {
        importExcel(buf, model).then(onOk).catch(onError);
    }
    catch (err) {
        res.status(400).json({ errorMsg: err });
    }
    function onOk(result) {
        res.json({ status: result.status, recordsStatus: result.invalidRecordsStatus });
    }
    function onError(err) {
        winston.error(err);
        res.status(400).json({ errorMsg: err });
    }
    function onFinish() {
        winston.info('Done parsing form!');
    }
    ;
}
exports.uploadFile = uploadFile;
function downloadExcelFromQuery(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const reqQuery = req.query;
            reqQuery.queryParams.pagination = null;
            const result = yield dbUtils.getTableQueryData(reqQuery);
            const binaryWorkbook = ExportExcelQuery.exportToExcel(result.records, result.fields, req.user.paying);
            res.set({ "Content-Disposition": 'attachment; filename="arquivo.xlsx"' });
            res.set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            res.send(binaryWorkbook);
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.downloadExcelFromQuery = downloadExcelFromQuery;
//# sourceMappingURL=ExcelController.js.map