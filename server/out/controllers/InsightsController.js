'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const ControllerUtils = require("../lib/ControllerUtils");
const News_1 = require("../lib/News");
var Sync = require('sync');
const await_1 = require("../lib/await");
const sectionNames = {
    flexSlider: 'flexSlider',
    section1Articles: 'section1Articles',
    section2Articles: 'section2Articles',
    section3Articles: 'section3Articles',
    section4Articles: 'section4Articles',
};
function getInsights(req, res, next) {
    Sync(function () {
        const insightsRecentOpts = {
            limit: 5,
            order: [['created_at', 'DESC']],
            include: [{
                    model: db.models.User,
                    as: 'author',
                    attributes: ['name']
                }]
        };
        const recentInsights = await_1.await(db.models.News.findAll(insightsRecentOpts));
        const recentInsightsRes = recentInsights.map((insight) => {
            return {
                id: insight.id,
                title: insight.title,
                content: insight.content,
                author: insight.author.name,
                imgUrl: News_1.formatImgUrl(insight.id, 'small'),
                date: insight.created_at
            };
        });
        function getResInsights(sectionName, size, firstSize) {
            const queryOpts = {
                attributes: [],
                include: [{
                        model: db.models.News,
                        as: 'insight',
                        attributes: [
                            'id',
                            'title',
                            'content',
                            'created_at'
                        ],
                        include: [{
                                model: db.models.User,
                                as: 'author',
                                attributes: ['name']
                            }]
                    }],
                order: ['order'],
                where: { section: sectionName }
            };
            const insights = await_1.await(db.models.InsightsPublisher.findAll(queryOpts));
            const insightsRes = insights.map((insight) => {
                return {
                    id: insight.insight.id,
                    title: insight.insight.title,
                    content: insight.insight.content,
                    author: insight.insight.author.name,
                    imgUrl: News_1.formatImgUrl(insight.insight.id, size),
                    date: insight.insight.created_at
                };
            });
            if (firstSize && insightsRes.length > 0) {
                insightsRes[0].imgUrl = News_1.formatImgUrl(insightsRes[0].id, firstSize);
            }
            return insightsRes;
        }
        const insightsRes = {
            section1Articles: getResInsights(sectionNames.section1Articles, 'small', 'medium'),
            section2Articles: getResInsights(sectionNames.section2Articles, 'small', 'medium'),
            section3Articles: getResInsights(sectionNames.section3Articles, 'small', 'medium'),
            section4Articles: getResInsights(sectionNames.section4Articles, 'small', 'medium'),
            popular: recentInsightsRes,
            recent: recentInsightsRes,
            flexSlider: getResInsights(sectionNames.flexSlider, 'large'),
        };
        res.json(insightsRes);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.getInsights = getInsights;
function saveInsights(req, res, next) {
    Sync(function () {
        const data = JSON.parse(req.body.data);
        const InsightsPublisher = db.models.InsightsPublisher;
        const sections = [
            { items: data.flexSlider, name: sectionNames.flexSlider },
            { items: data.section1Articles, name: sectionNames.section1Articles },
            { items: data.section2Articles, name: sectionNames.section2Articles },
            { items: data.section3Articles, name: sectionNames.section3Articles },
            { items: data.section4Articles, name: sectionNames.section4Articles },
        ];
        const recordItems = [];
        for (var section of sections) {
            section.items.map((insight_id, index) => {
                recordItems.push({
                    order: index,
                    section: section.name,
                    insight_id
                });
            });
        }
        db.sequelize.transaction(function (t) {
            const destroyOptions = { where: { id: { $gte: 0 } } };
            return InsightsPublisher.destroy(destroyOptions).then(() => {
                return InsightsPublisher.bulkCreate(recordItems);
            }).then(() => {
                const response = { msg: 'OK' };
                res.json(response);
            });
        });
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.saveInsights = saveInsights;
//# sourceMappingURL=InsightsController.js.map