'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ControllerUtils = require("../lib/ControllerUtils");
const dbUtils_1 = require("../lib/dbUtils");
const Maps_1 = require("../lib/Maps");
function getBlocks(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const fields = ['id', 'name', 'polygons'];
            const blocks = yield dbUtils_1.simpleQuery('blocks', fields);
            res.json({ blocks });
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getBlocks = getBlocks;
function getOilFields(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const options = {
                table: {
                    name: 'oil_fields',
                    fields: ['id', 'name', 'polygons']
                },
                extraFields: [],
                joinTables: [],
                where: [{
                        field: 'polygons',
                        isNotNull: true
                    }],
                order: []
            };
            const oilFields = yield dbUtils_1.execQuery(options);
            res.json({ oilFields });
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getOilFields = getOilFields;
function getProductionUnits(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const options = {
                table: {
                    name: 'production_units',
                    fields: ['id', 'name', 'coordinates']
                },
                extraFields: [],
                joinTables: [],
                where: [{
                        field: 'coordinates',
                        isNotNull: true
                    }],
                order: []
            };
            const productionUnits = yield dbUtils_1.execQuery(options);
            res.json({ productionUnits });
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getProductionUnits = getProductionUnits;
function getWells(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const options = {
                table: {
                    name: 'wells',
                    fields: ['id', 'name', 'lat', 'lng']
                },
                extraFields: [],
                joinTables: [],
                where: [{
                        field: 'lat',
                        isNotNull: true
                    }],
                order: []
            };
            const wells = yield dbUtils_1.execQuery(options);
            const processedWells = wells.map((well) => {
                return {
                    id: well.id,
                    name: well.name,
                    coordinates: {
                        lat: well.lat,
                        lng: well.lng
                    }
                };
            });
            res.json({ wells: processedWells });
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getWells = getWells;
function getDrillingRigs(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const optsDron = {
                table: {
                    name: 'drilling_rigs_onshore',
                    fields: ['id', 'name', 'coordinates']
                },
                extraFields: [],
                joinTables: [],
                where: [{
                        field: 'coordinates',
                        isNotNull: true
                    }],
                order: []
            };
            const drillingRigsOnshore = yield dbUtils_1.execQuery(optsDron);
            const optsDroff = {
                table: {
                    name: 'drilling_rigs_onshore',
                    fields: ['id', 'name', 'coordinates']
                },
                extraFields: [],
                joinTables: [],
                where: [{
                        field: 'coordinates',
                        isNotNull: true
                    }],
                order: []
            };
            const drillingRigsOffshore = yield dbUtils_1.execQuery(optsDroff);
            const allDrillingRigs = [];
            for (var dr of drillingRigsOnshore) {
                allDrillingRigs.push({
                    type: 'onshore',
                    id: dr.id,
                    name: dr.name,
                    coordinates: dr.coordinates
                });
            }
            for (var dr of drillingRigsOffshore) {
                allDrillingRigs.push({
                    type: 'offshore',
                    id: dr.id,
                    name: dr.name,
                    coordinates: dr.coordinates
                });
            }
            res.json({ drillingRigs: allDrillingRigs });
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getDrillingRigs = getDrillingRigs;
function getItemsInsideMap(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const query = req.query;
            const items = yield Maps_1.getItemsInsideArea(query.geoLimits);
            const result = { items };
            res.json(result);
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getItemsInsideMap = getItemsInsideMap;
//# sourceMappingURL=MapsController.js.map