'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
const search = require("../lib/search");
const dsParams = require("../lib/DataSourcesParams");
function main(req, res) {
    const query = req.query;
    const searchValue = query.searchValue;
    search.searchLike(searchValue, query.countLimit).then(onResults).catch(function (error) {
        winston.error(error.stack);
    });
    function onResults(queryResults) {
        var results = [];
        for (var n = 0; n < queryResults.length; n++) {
            const result = queryResults[n];
            const modelParams = dsParams[result.model];
            results.push({
                model: result.model,
                modelLabel: modelParams.tableLabel,
                name: result.name,
                id: result.id,
            });
        }
        const result = { values: results };
        res.json(result);
        return null;
    }
}
exports.main = main;
;
//# sourceMappingURL=SearchController.js.map