'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const ControllerUtils = require("../lib/ControllerUtils");
const dsParams = require("../lib/DataSourcesParams");
const QueryGenerator = require("../db/queries/QueryGenerator");
var Sync = require('sync');
const await_1 = require("../lib/await");
function getUpdateRecords() {
    const updatesOpts = {
        table: {
            name: 'updates_log',
            fields: [
                'obj_id',
                'updates',
                'created_at',
                'model',
            ]
        },
        joinTables: [],
        extraFields: [
            ['"UPDATE"', 'type']
        ],
        where: [{
                field: 'model',
                notIn: [
                    '"User"',
                    '"Association"',
                    '"ErrorLog"',
                    '"ErrorReport"',
                    '"ExcelImportLog"',
                    '"RequestLogModel"',
                    '"UpdateLog"',
                ]
            }],
        order: [],
    };
    const insightsOpts = {
        table: {
            name: 'news',
            fields: [
                ['id', 'obj_id'],
                ['title', 'updates'],
                'created_at',
            ]
        },
        joinTables: [],
        extraFields: [
            ['"News"', 'model'],
            ['"INSIGHT"', 'type'],
        ],
        where: [],
        order: [],
    };
    const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
    const updatesQueryStr = QueryGenerator.generate(updatesOpts);
    const insightsQueryStr = QueryGenerator.generate(insightsOpts);
    const orderStr = ' order by created_at desc ';
    const limitStr = ' limit 10 ';
    const queryStr = updatesQueryStr + ' union ' + insightsQueryStr + orderStr + limitStr;
    return await_1.await(db.sequelize.query(queryStr, simpleQueryType));
}
function getUpdateTickerItem(update) {
    const params = dsParams[update.model];
    const record = await_1.await(db.models[update.model].findById(update.obj_id));
    if (!record)
        return null;
    const objLabel = record[params.labelField];
    let title = params.labelSingular + ' ' + objLabel + ': ';
    const updatedFields = JSON.parse(update.updates);
    const updatedFieldLabels = [];
    for (let updatedField of updatedFields) {
        if (!params.fields[updatedField])
            continue;
        let label = params.fields[updatedField].label;
        if (label.indexOf('admin') > -1)
            continue;
        updatedFieldLabels.push(label);
    }
    if (updatedFieldLabels.length == 0) {
        return null;
    }
    title += updatedFieldLabels.join(', ');
    const link = '/app/view_record?source=' + update.model + '&id=' + update.obj_id;
    return {
        category: 'Atualização',
        title,
        link
    };
}
function getInsightTickerItem(update) {
    const insight = await_1.await(db.models.News.findById(update.obj_id));
    const title = update.updates;
    const link = '/app/view_new?id=' + update.obj_id;
    return {
        category: 'Insight',
        title,
        link
    };
}
function getUpdates(req, res, next) {
    Sync(function () {
        const updates = getUpdateRecords();
        const items = [];
        for (let update of updates) {
            if (update.type == 'UPDATE') {
                const item = getUpdateTickerItem(update);
                if (item) {
                    items.push(item);
                }
            }
            else if (update.type == 'INSIGHT') {
                const item = getInsightTickerItem(update);
                items.push(item);
            }
            else {
                throw 'tipo não previsto';
            }
        }
        const resObj = { items };
        res.json(resObj);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.getUpdates = getUpdates;
//# sourceMappingURL=TickerUpdatesController.js.map