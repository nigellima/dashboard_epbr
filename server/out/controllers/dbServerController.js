'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
var Sync = require('sync');
const libAwait = require("../lib/await");
const ControllerUtils = require("../lib/ControllerUtils");
const dbUtils = require("../lib/dbUtils");
const dsParams = require("../lib/DataSourcesParams");
const DataSourceOperations = require("../lib/DataSourceOperations/index");
const QueriesById = require("../db/queries/QueriesById");
const TableQueries = require("../db/queries/TableQueries");
const TimeSeriesQueries = require("../db/queries/TimeSeriesQueries");
const GetRecordOptions_1 = require("../lib/GetRecordOptions");
const Filters = require("../lib/Filters");
function sendErrorReport(req, res, next) {
    const body = req.body;
    const errorReportRecord = {
        url: body.url,
        description: body.description,
        reporter_id: req.user.id,
        status: 'Em aberto'
    };
    db.models.ErrorReport.create(errorReportRecord).then(result => {
        const resObj = { msg: 'OK' };
        res.json(resObj);
    }).catch(ControllerUtils.getErrorFunc(res, 500, "Erro"));
}
exports.sendErrorReport = sendErrorReport;
function viewRecord(req, res, next) {
    Sync(function () {
        const query = req.query;
        const dataSourceName = query.dataSource;
        if (req.user && !req.user.admin) {
            const forbidden = {
                "User": true,
                "Association": true,
                "ErrorLog": true,
                "ErrorReport": true,
                "ExcelImportLog": true,
                "RequestLogModel": true,
                "UpdateLog": true,
            };
            if (forbidden[dataSourceName]) {
                ControllerUtils.getErrorFunc(res, 401, "Sem permissão")("Sem permissão");
                return;
            }
        }
        const id = query.id;
        const dataSource = dbUtils.getDataSource(dataSourceName);
        const options = {};
        options.include = [{ all: true }];
        const record = libAwait.await(dataSource.findById(id, options));
        const dsOperations = DataSourceOperations[dataSourceName];
        const recordValues = dsOperations.recordToViewValues(dataSourceName, record);
        const viewParams = dsParams[dataSource.name];
        const result = {
            record: recordValues,
            referencedObjects: viewParams.referencedObjectsOnView,
            extraRecordData: libAwait.await(dbUtils.loadExtraData(dataSourceName, id)),
            updatedAt: record.updated_at
        };
        res.json(result);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar o registro."));
}
exports.viewRecord = viewRecord;
function getRecord(req, res, next) {
    Sync(function () {
        const query = req.query;
        const recordOptions = GetRecordOptions_1.getRecordOptions[query.optionsName];
        const record = libAwait.await(recordOptions.model.findById(query.id, recordOptions.seqOptions));
        const result = { record };
        res.json(result);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar o registro."));
}
exports.getRecord = getRecord;
function getQueryData(req, res) {
    Sync(function () {
        console.log(req.query);
        const query = req.query;
        const queryName = query.queryName;
        const filters = query.filters;
        const queryById = QueriesById.queries[queryName];
        const queryStr = queryById.queryStrFn(filters);
        const simpleQueryType = { type: db.Sequelize.QueryTypes.SELECT };
        db.sequelize.query(queryStr, simpleQueryType).then((records) => {
            if (queryById.recordProcessor) {
                for (var record of records) {
                    queryById.recordProcessor(record);
                }
            }
            const result = {
                fields: queryById.fields,
                records: records
            };
            console.log('query result ' + records);
            res.json(result);
        }).catch(err => { console.log('erro na query ' + err); ControllerUtils.getErrorFunc(res, 500, "Erro"); });
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os registros."));
}
exports.getQueryData = getQueryData;
function getTableQueryData(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const reqQuery = req.query;
            const result = yield dbUtils.getTableQueryData(reqQuery);
            res.json(result);
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getTableQueryData = getTableQueryData;
function getTimeSeries(req, res) {
    Sync(function () {
        const queryParams = req.query.queryParams;
        const queryName = req.query.queryName;
        TimeSeriesQueries.getData(queryName, queryParams).then((results) => {
            const result = {
                records: results
            };
            res.json(result);
        }).catch(ControllerUtils.getErrorFunc(res, 500, "Erro"));
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.getTimeSeries = getTimeSeries;
function getDashboardData(req, res) {
    Sync(function () {
        const projQueryParams = {
            order: [],
            filters: [],
            pagination: {
                first: 0,
                itemsPerPage: 1
            }
        };
        const dashboardData = {
            numBids: libAwait.await(db.models.Bid.count()),
            numContracts: libAwait.await(db.models.Contract.count()),
            numPersons: libAwait.await(db.models.Person.count()),
            numProjects: 0
        };
        res.json(dashboardData);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.getDashboardData = getDashboardData;
function getTableQueriesFields(req, res) {
    Sync(function () {
        const queryParams = req.query;
        const query = TableQueries.queries[queryParams.queryName];
        const result = {
            fields: [],
            title: query.title,
            tableauUrl: query.tableauUrl
        };
        result.fields = query.fields;
        res.json(result);
    }, ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados."));
}
exports.getTableQueriesFields = getTableQueriesFields;
function getFilterSource(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const query = req.query;
            const values = yield Filters.getFilterResult(query.queryName, query.fieldName);
            const result = { values };
            res.json(result);
        }
        catch (err) {
            ControllerUtils.getErrorFunc(res, 500, "Não foi possível recuperar os dados.")(err);
        }
    });
}
exports.getFilterSource = getFilterSource;
//# sourceMappingURL=dbServerController.js.map