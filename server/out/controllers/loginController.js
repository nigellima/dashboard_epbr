"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const session = require("../lib/session");
function loginOk(res, token) {
    res.redirect('/app/index.html?token=' + token);
}
function loginPage(req, res, next) {
    const token = req.cookies.token;
    session.userFromToken(token)
        .then(user => {
        if (user) {
            loginOk(res, token);
        }
        else {
            res.render('login', { title: 'Login' });
        }
    })
        .catch(err => {
        res.render('login', { title: 'Login' });
    });
}
exports.loginPage = loginPage;
;
function makeLogin(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let token = yield session.login(req.body.username, req.body.password);
            loginOk(res, token);
        }
        catch (err) {
            res.render('login', { title: 'Login', errorMsg: err });
        }
    });
}
exports.makeLogin = makeLogin;
;
function makeLoginREST(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let token = yield session.login(req.body.username, req.body.password);
            res.json({ token: token });
        }
        catch (err) {
            res.status(404).json({ msg: 'Erro no login' });
        }
    });
}
exports.makeLoginREST = makeLoginREST;
;
function logout(req, res, next) {
    session.logout(req.user);
    res.json({ msg: 'Usuário deslogado.' });
}
exports.logout = logout;
//# sourceMappingURL=loginController.js.map