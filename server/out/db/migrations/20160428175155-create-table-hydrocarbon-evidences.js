'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            well_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'wells',
                    key: 'id'
                }
            },
            notification_date: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            fluids: {
                type: Sequelize.STRING,
                allowNull: false
            },
            depth: {
                type: Sequelize.FLOAT,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('hydrocarbon_evidences', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('hydrocarbon_evidences');
    }
};
//# sourceMappingURL=20160428175155-create-table-hydrocarbon-evidences.js.map