'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            main_fluid: {
                type: Sequelize.STRING,
                allowNull: false
            },
            fpso_award: {
                type: Sequelize.STRING,
                allowNull: false
            },
            oil_field: {
                type: Sequelize.STRING,
                allowNull: false
            },
            block: {
                type: Sequelize.STRING,
                allowNull: false
            },
            owner: {
                type: Sequelize.STRING,
                allowNull: false
            },
            former_owner: {
                type: Sequelize.STRING,
                allowNull: false
            },
            awarded_contractor: {
                type: Sequelize.STRING,
                allowNull: false
            },
            type: {
                type: Sequelize.ENUM,
                values: ['FPSO'],
                defaultValue: 'FPSO'
            },
            hull_type: {
                type: Sequelize.ENUM(),
                values: ['New Build', 'Converted'],
                defaultValue: 'New Build'
            },
            status: {
                type: Sequelize.ENUM('Operational', 'Installed', 'Awarded', 'Ongoing BID', 'FID/Petrobras Businnes Plan', 'Market Forecast', 'Planned Decommissioning', 'Under Decommissioning', 'Decommissioned', 'Planned by Operator'),
                allowNull: true
            },
            coordinates: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            general_info: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            oil_processing_capacity: {
                type: Sequelize.FLOAT,
                allowNull: true
            },
            gas_processing_capacity: {
                type: Sequelize.FLOAT,
                allowNull: true
            },
            co2_percentage: {
                type: Sequelize.FLOAT,
                allowNull: true
            },
            liquids_processing_capacity: {
                type: Sequelize.FLOAT,
                allowNull: true
            },
            local_content_percentage: {
                type: Sequelize.FLOAT,
                allowNull: true
            },
            first_oil: {
                type: Sequelize.DATEONLY,
                allowNull: true
            },
            contractors_deadline: {
                type: Sequelize.DATEONLY,
                allowNull: true
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('production_units', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('production_units');
    }
};
//# sourceMappingURL=20160502172140-create-table-production_unit.js.map