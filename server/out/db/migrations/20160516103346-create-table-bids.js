'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            production_unit_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'production_units',
                    key: 'id'
                }
            },
            companies: {
                allowNull: false,
                type: Sequelize.STRING
            },
            deadline: {
                allowNull: false,
                type: Sequelize.STRING
            },
            scope: {
                allowNull: false,
                type: Sequelize.STRING
            },
            published: {
                allowNull: false,
                type: Sequelize.DATE
            },
            subject: {
                allowNull: false,
                type: Sequelize.STRING
            },
            update: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            operator: {
                type: Sequelize.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('bids', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('bids');
    }
};
//# sourceMappingURL=20160516103346-create-table-bids.js.map