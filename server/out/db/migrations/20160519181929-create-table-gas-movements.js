'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            product: {
                type: Sequelize.STRING,
                allowNull: false
            },
            period_year: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            period_month: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            value: {
                type: Sequelize.FLOAT,
                allowNull: false
            },
            gas_pipeline_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'gas_pipelines',
                    key: 'id'
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('gas_movements', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('gas_movements');
    }
};
//# sourceMappingURL=20160519181929-create-table-gas-movements.js.map