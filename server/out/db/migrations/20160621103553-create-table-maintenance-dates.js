'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            period: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            production_unit_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'production_units',
                    key: 'id'
                }
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('maintenance_dates', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('maintenance_dates');
    }
};
//# sourceMappingURL=20160621103553-create-table-maintenance-dates.js.map