'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            obj_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            contract_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'contracts',
                    key: 'id'
                },
                onDelete: 'CASCADE'
            },
            model_name: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.STRING,
                allowNull: true
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
        };
        return queryInterface.createTable('contract_projects', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('contract_projects');
    }
};
//# sourceMappingURL=20160712111049-create-table-contract-projects.js.map