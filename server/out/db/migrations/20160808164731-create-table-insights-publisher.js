'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const tableOpts = {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            insight_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'news',
                    key: 'id',
                },
                onDelete: 'CASCADE'
            },
            order: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            section: {
                type: Sequelize.STRING,
                allowNull: false
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        };
        return queryInterface.createTable('insights_publisher', tableOpts);
    },
    down: function (queryInterface, SequelizeVar) {
        return queryInterface.dropTable('insights_publisher');
    }
};
//# sourceMappingURL=20160808164731-create-table-insights-publisher.js.map