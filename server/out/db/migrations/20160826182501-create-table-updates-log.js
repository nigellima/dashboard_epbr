'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const fields = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            model: {
                type: Sequelize.STRING,
                allowNull: false
            },
            obj_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            type: {
                type: Sequelize.STRING,
                comment: 'type of the update. New, edit, etc.',
                allowNull: false
            },
            updates: {
                type: Sequelize.TEXT('medium'),
                comment: 'the content of the update',
                allowNull: true
            },
        };
        return queryInterface.createTable("updates_log", fields);
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("updates_log");
    }
};
//# sourceMappingURL=20160826182501-create-table-updates-log.js.map