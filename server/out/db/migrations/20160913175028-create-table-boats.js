'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const fields = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            type: {
                type: Sequelize.STRING,
                allowNull: true
            },
            info_json: {
                type: Sequelize.STRING,
                allowNull: true
            },
            owner_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'companies',
                    key: 'id',
                },
                onDelete: 'CASCADE'
            },
            operator_id: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'companies',
                    key: 'id',
                },
                onDelete: 'CASCADE'
            },
        };
        return queryInterface.createTable("boats", fields);
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("boats");
    }
};
//# sourceMappingURL=20160913175028-create-table-boats.js.map