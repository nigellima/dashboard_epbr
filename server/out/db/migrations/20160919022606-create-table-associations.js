'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const fields = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            type: {
                type: Sequelize.STRING,
                allowNull: false
            },
            src_model: {
                type: Sequelize.STRING,
                allowNull: false
            },
            dest_model: {
                type: Sequelize.STRING,
                allowNull: false
            },
            src_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            dest_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            json_field: {
                type: Sequelize.STRING,
                allowNull: true
            },
        };
        return queryInterface.createTable("associations", fields);
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("associations");
    }
};
//# sourceMappingURL=20160919022606-create-table-associations.js.map