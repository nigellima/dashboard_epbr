'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = {
    up: function (queryInterface, SequelizeVar) {
        const fields = {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            created_at: {
                type: Sequelize.DATE
            },
            updated_at: {
                type: Sequelize.DATE
            },
            model_name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            obj_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            desc: {
                type: Sequelize.STRING,
                allowNull: false
            },
            value: {
                type: Sequelize.STRING,
                allowNull: true
            },
        };
        return queryInterface.createTable("model_value_associations", fields);
    },
    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("model_value_associations");
    }
};
//# sourceMappingURL=20160929150212-create-table-model_value_associations.js.map