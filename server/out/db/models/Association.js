'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
    const Association = sequelize.define('Association', {
        type: {
            type: Sequelize.STRING,
            allowNull: false
        },
        src_model: {
            type: Sequelize.STRING,
            allowNull: false
        },
        dest_model: {
            type: Sequelize.STRING,
            allowNull: false
        },
        src_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        dest_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        json_field: {
            type: Sequelize.STRING,
            allowNull: true
        },
    }, {
        underscored: true,
        tableName: 'associations',
    });
    return Association;
};
//# sourceMappingURL=Association.js.map