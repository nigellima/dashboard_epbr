'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    var BlockConcessionary = sequelize.define('BlockConcessionary', {
        prop: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        underscored: true,
        tableName: 'block_concessionaries',
        classMethods: {
            associate: function (models) {
                const ofOpts = {
                    as: 'block',
                    foreignKey: { allowNull: false }
                };
                BlockConcessionary.belongsTo(models.OilField, ofOpts);
                const cOpts = {
                    as: 'company',
                    foreignKey: { allowNull: false }
                };
                BlockConcessionary.belongsTo(models.Company, cOpts);
            }
        }
    });
    return BlockConcessionary;
};
//# sourceMappingURL=BlockConcessionary.js.map