'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const ModelUtils_1 = require("../../lib/ModelUtils");
function savePhoto(boat) {
    ModelUtils_1.saveOriginalImage(boat.dataValues.photo, 'Boat', boat.id);
}
module.exports = function (sequelize, DataTypes) {
    const Boat = sequelize.define('Boat', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        info_json: {
            type: Sequelize.STRING,
            allowNull: true
        },
        photo: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
    }, {
        underscored: true,
        tableName: 'boats',
        classMethods: {
            associate: function (models) {
                const ownerOpts = {
                    as: 'owner',
                    foreignKey: { allowNull: true }
                };
                Boat.belongsTo(models.Company, ownerOpts);
                const operatorOpts = {
                    as: 'operator',
                    foreignKey: { allowNull: true }
                };
                Boat.belongsTo(models.Company, operatorOpts);
            },
        },
        hooks: {
            afterCreate: savePhoto,
            beforeUpdate: savePhoto
        }
    });
    return Boat;
};
//# sourceMappingURL=Boat.js.map