'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const ModelUtils_1 = require("../../lib/ModelUtils");
function savePhoto(company) {
    ModelUtils_1.saveOriginalImage(company.dataValues.logo, 'Company', company.id);
}
module.exports = function (sequelize, DataTypes) {
    var Company = sequelize.define('Company', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        logo: {
            type: Sequelize.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
        site: {
            type: DataTypes.STRING,
            allowNull: true
        },
        telephones_text: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        telephones: ModelUtils_1.getListFieldObj('telephones_text'),
        segments_text: {
            type: DataTypes.TEXT('tiny'),
            allowNull: true
        },
        segments: ModelUtils_1.getListFieldObj('segments_text'),
    }, {
        underscored: true,
        tableName: 'companies',
        classMethods: {
            associate: function (models) {
                Company.belongsTo(models.Person, { as: 'main_person' });
            }
        },
        hooks: {
            afterCreate: savePhoto,
            beforeUpdate: savePhoto
        }
    });
    return Company;
};
//# sourceMappingURL=Company.js.map