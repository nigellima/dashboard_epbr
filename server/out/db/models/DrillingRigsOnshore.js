'use strict';
const Sequelize = require("sequelize");
const ModelUtils_1 = require("../../lib/ModelUtils");
function savePhoto(drillingRig) {
    ModelUtils_1.saveOriginalImage(drillingRig.dataValues.photo, 'DrillingRigOnshore', drillingRig.id);
}
function defineModel(sequelize, DataTypes) {
    const DrillingRigOnshore = sequelize.define('DrillingRigOnshore', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false
        },
        start: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        end: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        status: {
            type: DataTypes.STRING,
            allowNull: true
        },
        day_rate: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        photo: {
            type: Sequelize.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
        coordinates: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        coords_admin: ModelUtils_1.getCoordFieldObj('coordinates'),
    }, {
        underscored: true,
        tableName: 'drilling_rigs_onshore',
        classMethods: {
            associate: function (models) {
                const opts = {
                    as: 'contractor',
                    foreignKey: { allowNull: false }
                };
                DrillingRigOnshore.belongsTo(models.Company, opts);
                const optsOp = {
                    as: 'operator',
                    foreignKey: { allowNull: true }
                };
                DrillingRigOnshore.belongsTo(models.Company, optsOp);
            }
        },
        hooks: {
            afterCreate: savePhoto,
            beforeUpdate: savePhoto
        }
    });
    return DrillingRigOnshore;
}
;
module.exports = defineModel;
//# sourceMappingURL=DrillingRigsOnshore.js.map