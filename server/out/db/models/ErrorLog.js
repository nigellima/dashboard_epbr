'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    var ErrorLog = sequelize.define('ErrorLog', {
        error: {
            type: DataTypes.TEXT('medium'),
            allowNull: true
        }
    }, {
        underscored: true,
        tableName: 'error_log'
    });
    return ErrorLog;
};
//# sourceMappingURL=ErrorLog.js.map