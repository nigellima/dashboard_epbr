'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
    const ErrorReport = sequelize.define('ErrorReport', {
        url: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: false
        },
    }, {
        underscored: true,
        tableName: 'error_reports',
        classMethods: {
            associate: function (models) {
                const reporterOpts = {
                    as: 'reporter',
                    foreignKey: { allowNull: false }
                };
                ErrorReport.belongsTo(models.User, reporterOpts);
                const responsibleOpts = {
                    as: 'responsible',
                    foreignKey: { allowNull: true }
                };
                ErrorReport.belongsTo(models.User, responsibleOpts);
            },
        }
    });
    return ErrorReport;
};
//# sourceMappingURL=ErrorReport.js.map