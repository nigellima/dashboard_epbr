'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const await_1 = require("../../lib/await");
function updateGasPipeRef(gasPipeline, prefix) {
    const modelField = prefix + '_model_name';
    const objField = prefix + '_obj_id';
    const object = gasPipeline.dataValues[prefix];
    if (object == null || object.length != 1) {
        gasPipeline[modelField] = null;
        gasPipeline[objField] = null;
        return;
    }
    gasPipeline[modelField] = object[0].model_name;
    gasPipeline[objField] = object[0].id;
}
function updateAllGasPipeRefs(gasPipeline) {
    updateGasPipeRef(gasPipeline, 'src_instalation');
    updateGasPipeRef(gasPipeline, 'src_concession');
    updateGasPipeRef(gasPipeline, 'dst_instalation');
    updateGasPipeRef(gasPipeline, 'dst_concession');
}
function defineHooks(db) {
    db.GasPipeline.hook('beforeCreate', updateAllGasPipeRefs);
    db.GasPipeline.hook('beforeUpdate', updateAllGasPipeRefs);
}
function loadRefObj(sequelize, gasPipeline, prefix) {
    const textField = prefix + '_text';
    if (gasPipeline[textField]) {
        return [];
    }
    const modelField = prefix + '_model_name';
    const objField = prefix + '_obj_id';
    const modelName = gasPipeline[modelField];
    const referencedModel = sequelize.models[modelName];
    const referencedObj = await_1.await(referencedModel.findById(gasPipeline[objField]));
    return [{
            id: gasPipeline[objField],
            model_id: gasPipeline[modelField],
            model: modelName,
            name: referencedObj.name
        }];
}
module.exports = function (sequelize, DataTypes) {
    const GasPipeline = sequelize.define('GasPipeline', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        state: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        diameter: {
            type: Sequelize.FLOAT,
            allowNull: true
        },
        extension: {
            type: Sequelize.FLOAT,
            allowNull: true
        },
        classification: {
            type: Sequelize.STRING,
            allowNull: true
        },
        src_instalation_text: {
            type: Sequelize.STRING,
            allowNull: true
        },
        src_concession_text: {
            type: Sequelize.STRING,
            allowNull: true
        },
        dst_instalation_text: {
            type: Sequelize.STRING,
            allowNull: true
        },
        dst_concession_text: {
            type: Sequelize.STRING,
            allowNull: true
        },
        src_instalation_obj_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        src_concession_obj_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        dst_instalation_obj_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        dst_concession_obj_id: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        src_instalation_model_name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        src_concession_model_name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        dst_instalation_model_name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        dst_concession_model_name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        src_instalation: {
            type: DataTypes.VIRTUAL,
            get: function () { return loadRefObj(sequelize, this, 'src_instalation'); }
        },
        src_concession: {
            type: DataTypes.VIRTUAL,
            get: function () { return loadRefObj(sequelize, this, 'src_concession'); }
        },
        dst_instalation: {
            type: DataTypes.VIRTUAL,
            get: function () { return loadRefObj(sequelize, this, 'dst_instalation'); }
        },
        dst_concession: {
            type: DataTypes.VIRTUAL,
            get: function () { return loadRefObj(sequelize, this, 'dst_concession'); }
        },
    }, {
        underscored: true,
        tableName: 'gas_pipelines',
        classMethods: {
            defineHooks: defineHooks
        }
    });
    return GasPipeline;
};
//# sourceMappingURL=GasPipeline.js.map