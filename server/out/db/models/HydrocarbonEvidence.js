'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    var HydrocarbonEvidence = sequelize.define('HydrocarbonEvidence', {
        notification_date: {
            type: DataTypes.DATEONLY,
            allowNull: false
        },
        fluids: {
            type: DataTypes.STRING,
            allowNull: false
        },
        depth: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        underscored: true,
        tableName: 'hydrocarbon_evidences',
        classMethods: {
            associate: function (models) {
                const opts = {
                    as: 'well',
                    foreignKey: { allowNull: false }
                };
                HydrocarbonEvidence.belongsTo(models.Well, opts);
            }
        }
    });
    return HydrocarbonEvidence;
};
//# sourceMappingURL=HydrocarbonEvidence.js.map