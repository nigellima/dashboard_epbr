'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
    const IndustrySegment = sequelize.define('IndustrySegment', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
    }, {
        underscored: true,
        tableName: 'industry_segments',
    });
    return IndustrySegment;
};
//# sourceMappingURL=IndustrySegment.js.map