'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
    const MaintenanceDate = sequelize.define('MaintenanceDate', {
        period: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
    }, {
        underscored: true,
        tableName: 'maintenance_dates',
        classMethods: {
            associate: function (models) {
                const gpOpts = {
                    as: 'production_unit',
                    foreignKey: { allowNull: false }
                };
                MaintenanceDate.belongsTo(models.ProductionUnit, gpOpts);
            },
        }
    });
    return MaintenanceDate;
};
//# sourceMappingURL=MaintenanceDate.js.map