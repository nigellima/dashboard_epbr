'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
module.exports = function (sequelize, DataTypes) {
    const ModelValueAssocation = sequelize.define('ModelValueAssocation', {
        model_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        obj_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        desc: {
            type: Sequelize.STRING,
            allowNull: false
        },
        value: {
            type: Sequelize.STRING,
            allowNull: true
        },
    }, {
        underscored: true,
        tableName: 'model_value_associations',
        classMethods: {
            associate: function (models) { },
        },
    });
    return ModelValueAssocation;
};
//# sourceMappingURL=ModelValueAssociation.js.map