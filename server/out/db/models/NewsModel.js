"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const newsLib = require("../../lib/News");
const AWS = require("../../lib/AWS");
const ImageProcessing_1 = require("../../lib/ImageProcessing");
const PromiseUtils_1 = require("../../lib/PromiseUtils");
const await_1 = require("../../lib/await");
var db = {};
const imageParams = [
    {
        width: 620,
        height: 350,
        size: 'large'
    },
    {
        width: 300,
        height: 220,
        size: 'medium'
    },
    {
        width: 60,
        height: 60,
        size: 'small'
    },
];
function setReferences(news, options) {
    await_1.await(db.NewsModels.destroy({ where: { news_id: news.id } }));
    const referencedObjects = newsLib.getModelReferences(news.content);
    for (let referencedObj of referencedObjects) {
        if (!db[referencedObj.model])
            throw 'Modelo ' + referencedObj.model + ' não encontrado.';
        const newsRefObj = {
            news_id: news.id,
            model_ref_id: referencedObj.id,
            model_name: referencedObj.model
        };
        await_1.await(db.NewsModels.create(newsRefObj, { transaction: options.transaction }));
    }
    const image = news.dataValues.image;
    if (image) {
        {
            const imgBuffer = new Buffer(image);
            const fileName = AWS.getImagesPath() + newsLib.formatImgUrl(news.id);
            AWS.saveImage(imgBuffer, fileName);
        }
        for (let imageParam of imageParams) {
            const imgBuffer = new Buffer(image);
            const resampledBuffer = await_1.await(ImageProcessing_1.resample(imgBuffer, imageParam.width, imageParam.height));
            const fileName = AWS.getImagesPath() + newsLib.formatImgUrl(news.id, imageParam.size);
            AWS.saveImage(resampledBuffer, fileName);
        }
    }
}
function syncifySaveHook(news, options) {
    return PromiseUtils_1.syncify(setReferences.bind(null, news, options));
}
function defineHooks(DB) {
    db = DB;
    db.News.hook('afterCreate', syncifySaveHook);
    db.News.hook('beforeUpdate', syncifySaveHook);
}
module.exports = function (sequelize, DataTypes) {
    var News = sequelize.define('News', {
        title: {
            type: DataTypes.STRING,
            allowNull: false
        },
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        tableau_url: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        image: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
    }, {
        underscored: true,
        tableName: 'news',
        classMethods: {
            associate: function (models) {
                const ProductionUnitOpts = {
                    as: 'production_unit',
                    foreignKey: { allowNull: true }
                };
                News.belongsTo(models.ProductionUnit, ProductionUnitOpts);
                News.hasMany(models.NewsModels, { onDelete: 'cascade' });
            },
            defineHooks: defineHooks
        }
    });
    return News;
};
//# sourceMappingURL=NewsModel.js.map