'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    var OilFieldConcessionary = sequelize.define('OilFieldConcessionary', {
        prop: {
            type: DataTypes.FLOAT,
            allowNull: false
        }
    }, {
        underscored: true,
        tableName: 'oil_field_concessionaries',
        classMethods: {
            associate: function (models) {
                const ofOpts = {
                    as: 'oil_field',
                    foreignKey: { allowNull: false }
                };
                OilFieldConcessionary.belongsTo(models.OilField, ofOpts);
                const cOpts = {
                    as: 'company',
                    foreignKey: { allowNull: false }
                };
                OilFieldConcessionary.belongsTo(models.Company, cOpts);
            }
        }
    });
    return OilFieldConcessionary;
};
//# sourceMappingURL=OilFieldConcessionary.js.map