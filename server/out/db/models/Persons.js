'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const ModelUtils_1 = require("../../lib/ModelUtils");
const ImageProcessing_1 = require("../../lib/ImageProcessing");
const AWS = require("../../lib/AWS");
const PromiseUtils_1 = require("../../lib/PromiseUtils");
const await_1 = require("../../lib/await");
function updatePersonProjects(db, person) {
    const projects = person.dataValues.projects;
    const options = { where: { person_id: person.id } };
    db.PersonProjects.destroy(options).then(() => {
        if (projects == null)
            return;
        const newProjectRecords = [];
        for (let project of projects) {
            var projectRecord = {
                person_id: person.id,
                model_name: project.model,
                model_ref_id: project.id,
                description: project.description
            };
            newProjectRecords.push(projectRecord);
        }
        return db.PersonProjects.bulkCreate(newProjectRecords);
    });
}
function saveImages(person) {
    if (!person.dataValues.photo)
        return;
    const imgBuffer = new Buffer(person.dataValues.photo);
    const resampledBuffer = await_1.await(ImageProcessing_1.resample(imgBuffer, 300, 300));
    const fileName = AWS.getImagesPath() + 'Person/cards/img_' + person.id + '.jpg';
    AWS.saveImage(resampledBuffer, fileName);
    ModelUtils_1.saveOriginalImage(person.dataValues.photo, 'Person', person.id);
}
function updateFieldsFunc(db, person) {
    updatePersonProjects(db, person);
    PromiseUtils_1.syncify(saveImages.bind(null, person));
    return null;
}
function defineHooks(db) {
    db.Person.hook('afterCreate', updateFieldsFunc.bind(this, db));
    db.Person.hook('beforeUpdate', updateFieldsFunc.bind(this, db));
}
module.exports = function (sequelize, DataTypes) {
    var Person = sequelize.define('Person', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        position: {
            type: DataTypes.STRING,
            allowNull: true
        },
        directorship: {
            type: DataTypes.STRING,
            allowNull: true
        },
        management_sector: {
            type: DataTypes.STRING,
            allowNull: true
        },
        info: {
            type: DataTypes.JSON,
            allowNull: true
        },
        info_str: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return this.info;
            },
            set: function (value) {
                if (value) {
                    this.info = JSON.parse(value);
                }
            }
        },
        email_text: {
            type: DataTypes.TEXT,
            allowNull: true,
            invisible: true
        },
        emails: ModelUtils_1.getListFieldObj('email_text'),
        linkedin: {
            type: DataTypes.STRING,
            allowNull: true
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        photo: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
        telephones_text: {
            type: DataTypes.TEXT,
            allowNull: true,
            invisible: true
        },
        telephones: ModelUtils_1.getListFieldObj('telephones_text'),
        projects: {
            type: DataTypes.VIRTUAL,
            get: function () {
                const projectsPromise = sequelize.models.PersonProjects.getProjects(sequelize, this.id);
                const projects = await_1.await(projectsPromise);
                return projects;
            }
        }
    }, {
        underscored: true,
        tableName: 'persons',
        validate: {
            nameNotNull: function () {
                if (!this.name)
                    throw new Error('Nome não pode ser nulo');
            }
        },
        classMethods: {
            associate: function (models) {
                Person.belongsTo(models.Company, {
                    as: 'company'
                });
            },
            defineHooks: defineHooks
        }
    });
    return Person;
};
//# sourceMappingURL=Persons.js.map