'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const ModelUtils_1 = require("../../lib/ModelUtils");
function savePhoto(productionUnit) {
    ModelUtils_1.saveOriginalImage(productionUnit.dataValues.photo, 'ProductionUnit', productionUnit.id);
}
module.exports = function (sequelize, DataTypes) {
    const ProductionUnit = sequelize.define('ProductionUnit', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        type: {
            type: DataTypes.ENUM('FPSO'),
            defaultValue: 'FPSO',
            allowNull: false
        },
        hull_type: {
            type: DataTypes.ENUM('New Build', 'Converted'),
            defaultValue: 'New Build',
            allowNull: false
        },
        status: {
            type: DataTypes.ENUM('Operational', 'Installed', 'Awarded', 'Ongoing BID', 'FID/Petrobras Businnes Plan', 'Market Forecast', 'Planned Decommissioning', 'Under Decommissioning', 'Decommissioned', 'Planned by Operator'),
            allowNull: true
        },
        coordinates: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        coords_admin: ModelUtils_1.getCoordFieldObj('coordinates'),
        general_info: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        main_fluid: {
            type: DataTypes.STRING,
            allowNull: true
        },
        fpso_award: {
            type: DataTypes.STRING,
            allowNull: true
        },
        oil_processing_capacity: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        co2_percentage: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        gas_processing_capacity: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        liquids_processing_capacity: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        local_content_percentage: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        contractors_deadline: {
            type: DataTypes.DATEONLY,
            allowNull: true
        },
        first_oil: {
            type: Sequelize.STRING,
            allowNull: true
        },
        oil_field: {
            type: DataTypes.STRING,
            allowNull: true
        },
        block: {
            type: DataTypes.STRING,
            allowNull: true
        },
        owner: {
            type: DataTypes.STRING,
            allowNull: true
        },
        former_owner: {
            type: DataTypes.STRING,
            allowNull: true
        },
        awarded_contractor: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }, {
        underscored: true,
        tableName: 'production_units',
        classMethods: {
            associate: function (models) {
            },
        },
        hooks: {
            afterCreate: savePhoto,
            beforeUpdate: savePhoto
        }
    });
    return ProductionUnit;
};
//# sourceMappingURL=ProductionUnit.js.map