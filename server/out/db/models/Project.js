'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const libAwait = require("../../lib/await");
const ModelUtils_1 = require("../../lib/ModelUtils");
exports.PROJECT_OBJS_TYPE = 'ProjectObjects';
function savePhoto(project) {
    return __awaiter(this, void 0, void 0, function* () {
        yield ModelUtils_1.saveOriginalImage(project.dataValues.photo, 'Project', project.id);
    });
}
function updateObjects(models, project) {
    return __awaiter(this, void 0, void 0, function* () {
        const Association = models['Association'];
        const delOpts = {
            where: {
                type: exports.PROJECT_OBJS_TYPE,
                src_id: project.id
            }
        };
        yield Association.destroy(delOpts);
        const objects = project.dataValues.objects;
        if (!objects)
            return;
        for (let object of objects) {
            let association = {
                type: exports.PROJECT_OBJS_TYPE,
                src_model: 'Project',
                src_id: project.id,
                dest_model: object.model,
                dest_id: object.id,
                json_field: object.description
            };
            yield Association.create(association);
        }
    });
}
function beforeUpdate(models, project) {
    return __awaiter(this, void 0, void 0, function* () {
        yield updateObjects(models, project);
        yield savePhoto(project);
    });
}
function afterCreate(models, project) {
    return __awaiter(this, void 0, void 0, function* () {
        yield updateObjects(models, project);
        yield savePhoto(project);
    });
}
function defineHooks(models) {
    models.Project.hook('afterCreate', afterCreate.bind(this, models));
    models.Project.hook('beforeUpdate', beforeUpdate.bind(this, models));
}
module.exports = function (sequelize, DataTypes) {
    const Project = sequelize.define('Project', {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        scope: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        value: {
            type: Sequelize.FLOAT,
            allowNull: true
        },
        json_field: {
            type: Sequelize.STRING,
            allowNull: true
        },
        updates: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        segment_type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        stage: {
            type: Sequelize.ENUM('CAPEX', 'OPEX'),
            allowNull: false
        },
        objects: {
            type: DataTypes.VIRTUAL,
            get: function () {
                const Association = sequelize.models['Association'];
                const queryOpts = {
                    where: {
                        type: exports.PROJECT_OBJS_TYPE,
                        src_id: this.id
                    }
                };
                const associations = libAwait.await(Association.findAll(queryOpts));
                return associations.map(association => {
                    const modOpt = { where: { name: association.dest_model } };
                    const obj = libAwait.await(sequelize.models[association.dest_model].findById(association.dest_id));
                    const fepObj = {
                        id: association.dest_id,
                        model: association.dest_model,
                        name: obj.name,
                        description: JSON.parse(association.json_field)
                    };
                    return fepObj;
                });
            },
        },
        photo: {
            type: DataTypes.VIRTUAL,
            get: function () {
                return 'image';
            },
        },
    }, {
        underscored: true,
        tableName: 'projects',
        classMethods: {
            associate: function (models) {
                {
                    const ownerOpts = {
                        as: 'owner',
                        foreignKey: { allowNull: true }
                    };
                    Project.belongsTo(models.Company, ownerOpts);
                }
            },
            defineHooks: defineHooks
        },
    });
    return Project;
};
//# sourceMappingURL=Project.js.map