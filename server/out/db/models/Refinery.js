'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const ModelUtils_1 = require("../../lib/ModelUtils");
module.exports = function (sequelize, DataTypes) {
    const Refinery = sequelize.define('Refinery', {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        telephones_text: {
            type: DataTypes.TEXT,
            allowNull: true,
            invisible: true
        },
        telephones: ModelUtils_1.getListFieldObj('telephones_text'),
        capacity: {
            type: DataTypes.FLOAT,
            allowNull: true
        },
        info: {
            type: DataTypes.TEXT,
            allowNull: true
        },
    }, {
        underscored: true,
        tableName: 'refineries'
    });
    return Refinery;
};
//# sourceMappingURL=Refinery.js.map