'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (sequelize, DataTypes) {
    const Terminal = sequelize.define('Terminal', {
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        type: {
            type: DataTypes.ENUM('ONSHORE', 'OFFSHORE'),
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        info: {
            type: DataTypes.TEXT,
            allowNull: true
        },
    }, {
        underscored: true,
        tableName: 'terminals'
    });
    return Terminal;
};
//# sourceMappingURL=Terminal.js.map