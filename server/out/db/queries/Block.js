"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const QueryGenerator = require("./QueryGenerator");
exports.Blocks = {
    title: 'Blocos',
    queryStrFn: (queryParams) => {
        const options = {
            table: {
                name: 'blocks',
                fields: [
                    'id',
                    ['name', 'block_name'],
                    'status'
                ]
            },
            joinTables: [
                {
                    name: 'companies',
                    fields: [
                        ['id', 'operator_id'],
                        ['name', 'operator_name'],
                    ],
                    joinField: 'blocks.operator_id'
                },
                {
                    name: 'basins',
                    fields: [
                        ['id', 'basin_id'],
                        ['name', 'basin_name'],
                    ],
                    joinField: 'blocks.basin_id'
                }
            ],
            extraFields: [
                ['"Block"', 'model'],
                ['"Basin"', 'basin_model'],
                ['"Company"', 'operator_model']
            ],
            where: queryParams.filters,
            order: queryParams.order
        };
        return QueryGenerator.generate(options);
    },
    fields: [
        {
            label: 'Nome',
            ref: {
                modelField: 'model',
                idField: 'id',
                valueField: 'block_name'
            }
        },
        {
            label: 'Bacia',
            ref: {
                modelField: 'basin_model',
                idField: 'basin_id',
                valueField: 'basin_name'
            },
            hasFilter: true
        },
        {
            label: 'Operador',
            ref: {
                modelField: 'operator_model',
                idField: 'operator_id',
                valueField: 'operator_name'
            },
            hasFilter: true
        },
        {
            label: 'Status',
            fieldName: 'status',
            type: 'VARCHAR',
            hasFilter: true
        }
    ],
    tableauUrl: 'https://public.tableau.com/views/Blocos/Painel1?:embed=y&:display_count=yes&:toolbar=no'
};
exports.blocksByBasin = {
    queryStrFn: (filter) => {
        const queryParams = {
            filters: [{
                    field: 'basin_id',
                    equal: filter.id
                }],
            order: [{ fieldName: 'block_name', dir: 'asc' }],
            pagination: { first: 0, itemsPerPage: 100 }
        };
        return exports.Blocks.queryStrFn(queryParams);
    },
    fields: exports.Blocks.fields,
};
//# sourceMappingURL=Block.js.map