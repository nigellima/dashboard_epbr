"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const QueryGenerator = require("./QueryGenerator");
const productionUnit = {
    title: 'Production Units',
    queryStrFn: (queryParams) => {
        console.log('getting fpsos');
        const options = {
            table: {
                name: 'production_units',
                fields: [
                    'id',
                    ['name', 'name'],
                    'first_oil',
                    'status',
                    'block',
                    'oil_field',
                    'owner',
                    'oil_processing_capacity',
                    'gas_processing_capacity',
                    'awarded_contractor',
                    'former_owner',
                    'main_fluid',
                    'co2_percentage',
                    'local_content_percentage',
                    'contractors_deadline',
                    'fpso_award',
                    'hull_type',
                ]
            },
            joinTables: [],
            extraFields: [
                ['"ProductionUnit"', 'model'],
                ['"OilField"', 'of_model'],
                ['"Block"', 'b_model'],
            ],
            where: queryParams.filters,
            order: queryParams.order
        };
        return QueryGenerator.generate(options);
    },
    fields: [
        {
            label: 'Unit Name',
            fieldName: 'name',
            type: 'VARCHAR'
        },
        {
            label: 'Status',
            fieldName: 'status',
            type: 'ENUM',
            hasFilter: true
        },
        {
            label: 'Field Name',
            fieldName: 'oil_field',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'Exploration Block',
            fieldName: 'block',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'Field Ownership',
            fieldName: 'owner',
            type: 'VARCHAR',
            hasFilter: true
        },
        {
            label: 'FPSO award (expected)',
            fieldName: 'fpso_award',
            hasFilter: true
        },
        {
            label: 'Awarded Contractor',
            fieldName: 'awarded_contractor',
            hasFilter: true
        },
    ]
};
exports.FPSOs = {
    title: 'FPSOs',
    queryStrFn: (queryParams) => {
        queryParams.filters.push({
            field: 'type',
            equal: '"FPSO"'
        });
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/DashboardFPSOepbr/DashboardEPBrazillianFPSOs?:embed=y&:display_count=yes&publish=yes',
};
exports.FixedProductionUnits = {
    title: 'Plataformas fixas',
    queryStrFn: (queryParams) => {
        queryParams.filters.push({
            field: 'type',
            equal: '"FIXED"'
        });
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/Fixas/Painel1?:embed=y&:display_count=yes&:toolbar=no'
};
exports.SemiSubmersibleProductionUnits = {
    title: 'Semi-subversíveis',
    queryStrFn: (queryParams) => {
        queryParams.filters.push({
            field: 'type',
            equal: '"SEMI"'
        });
        return productionUnit.queryStrFn(queryParams);
    },
    fields: productionUnit.fields,
    tableauUrl: 'https://public.tableau.com/views/Semi/Painel1?:embed=y&:display_count=yes&:toolbar=no',
};
//# sourceMappingURL=ProductionUnit.js.map