"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TableQueries = require("./TableQueries");
const QueryGenerator = require("./QueryGenerator");
const ContractQueries = require("./Contract");
const await_1 = require("../../lib/await");
const db = require("../models");
function baseProjectOpts() {
    return {
        table: {
            name: 'projects',
            fields: [
                ['id', 'p_id'],
                ['name', 'p_name'],
                'value'
            ]
        },
        extraFields: [
            ['"Project"', 'p_model'],
            ['"Company"', 'c_model'],
        ],
        joinTables: [
            {
                name: 'companies',
                fields: [
                    ['id', 'c_id'],
                    ['name', 'c_name'],
                ],
                joinField: 'projects.owner_id'
            },
        ],
        where: [],
        order: []
    };
}
const baseFields = [
    {
        label: 'Nome',
        ref: {
            modelField: 'p_model',
            idField: 'p_id',
            valueField: 'p_name'
        }
    },
    {
        label: 'Contratante',
        ref: {
            modelField: 'c_model',
            idField: 'c_id',
            valueField: 'c_name'
        }
    },
    {
        label: 'Valor',
        fieldName: 'value',
        type: 'CURRENCY'
    },
];
exports.contractsOfContractedInProject = {
    queryStrFn: (filter) => {
        const Project = db.models.Project;
        const project = await_1.await(Project.findById(filter.id));
        const jsonField = JSON.parse(project.json_field);
        const contracts_id = jsonField.contractors[filter.index].contracts_id;
        const queryParams = {
            filters: [{
                    field: 'contracts.id',
                    in: contracts_id
                }],
            order: [{ fieldName: 'start', dir: 'desc' }],
            pagination: { first: 0, itemsPerPage: 100 }
        };
        return ContractQueries.contracts.queryStrFn(queryParams);
    },
    fields: ContractQueries.contracts.fields
};
exports.personsOfContractedInProject = {
    queryStrFn: (filter) => {
        const Project = db.models.Project;
        const project = await_1.await(Project.findById(filter.id));
        const jsonField = JSON.parse(project.json_field);
        const persons_id = jsonField.contractors[filter.index].persons_id;
        const queryParams = {
            filters: [{
                    field: 'persons.id',
                    in: persons_id
                }],
            order: [],
            pagination: { first: 0, itemsPerPage: 100 }
        };
        return TableQueries.queries['Persons'].queryStrFn(queryParams);
    },
    fields: TableQueries.queries['Persons'].fields
};
exports.personsOfOwnerInProject = {
    queryStrFn: (filter) => {
        const Project = db.models.Project;
        const project = await_1.await(Project.findById(filter.id));
        const jsonField = JSON.parse(project.json_field);
        const owner_persons_id = jsonField.owner_persons_id;
        const queryParams = {
            filters: [{
                    field: 'persons.id',
                    in: owner_persons_id
                }],
            order: [{
                    fieldName: 'persons.name',
                    dir: 'asc'
                }],
            pagination: { first: 0, itemsPerPage: 100 }
        };
        return TableQueries.queries['Persons'].queryStrFn(queryParams);
    },
    fields: TableQueries.queries['Persons'].fields
};
exports.projectsTargetSales = {
    queryStrFn: (filter) => {
        const opts = baseProjectOpts();
        opts.where.push({
            field: 'projects.stage',
            equal: '"' + filter.fase + '"'
        });
        opts.where.push({
            field: 'projects.segment_type',
            equal: '"' + filter.type + '"'
        });
        var query = QueryGenerator.generate(opts);
        return query;
    },
    fields: baseFields
};
exports.projectTypesAndStages = {
    queryStrFn: (filter) => {
        const select = 'select segment_type, stage ';
        const fromStr = ' from projects ';
        const group = ' group by segment_type, stage ';
        const query = select + fromStr + group;
        return query;
    },
    fields: []
};
exports.personRelatedProjects = {
    queryStrFn: (filter) => {
        const opts = baseProjectOpts();
        const queryGenerator = new QueryGenerator.QueryGenerator();
        queryGenerator.getFilterStr = (filters, filterKeyword, aliasMap) => {
            const ownerFilter = ' JSON_contains(json_field, \'"' + filter.id + '"\', "$.owner_persons_id") > 0 ';
            const contractedsFilter = ' JSON_contains(JSON_EXTRACT(json_field, "$.contractors[*].persons_id"), \'"' + filter.id + '"\') > 0';
            return ' where ' + ownerFilter + ' or ' + contractedsFilter;
        };
        var query = QueryGenerator.generate(opts, queryGenerator);
        return query;
    },
    fields: baseFields
};
exports.contractRelatedProjects = {
    queryStrFn: (filter) => {
        const opts = baseProjectOpts();
        const queryGenerator = new QueryGenerator.QueryGenerator();
        queryGenerator.getFilterStr = (filters, filterKeyword, aliasMap) => {
            const contractedsFilter = ' JSON_contains(JSON_EXTRACT(json_field, "$.contractors[*].contracts_id"), \'"' + filter.id + '"\') > 0';
            return ' where ' + contractedsFilter;
        };
        var query = QueryGenerator.generate(opts, queryGenerator);
        return query;
    },
    fields: baseFields
};
exports.objectRelatedProjects = {
    queryStrFn: (filter) => {
        const whereOpts = {
            type: 'ProjectObjects',
            dest_model: filter.modelName,
            dest_id: filter.id
        };
        const projectAssocs = await_1.await(db.models.Association.findAll({ where: whereOpts }));
        const projectIds = projectAssocs.map(p => { return p.src_id; });
        const opts = baseProjectOpts();
        opts.where.push({
            field: 'projects.id',
            in: projectIds
        });
        var query = QueryGenerator.generate(opts);
        return query;
    },
    fields: baseFields
};
//# sourceMappingURL=Project.js.map