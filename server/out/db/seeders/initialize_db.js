"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../models");
var umzug = require('../../lib/InitUmzug');
var Sync = require('sync');
const winston = require("winston");
const createFixtures = require("../../test/fixtures/initial_data");
const await_1 = require("../../lib/await");
winston.add(winston.transports.File, { filename: 'log/seeder.log' });
Sync(function () {
    if (process.env.NODE_ENV == 'production') {
        console.log('Production environment not allowed!!');
        return;
    }
    try {
        await_1.await(db.sequelize.getQueryInterface().dropAllTables());
        await_1.await(umzug.up());
        createFixtures.createFixtures();
    }
    catch (e) {
        winston.error(e.errors);
        winston.error(e.stack);
    }
});
//# sourceMappingURL=initialize_db.js.map