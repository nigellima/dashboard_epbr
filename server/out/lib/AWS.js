"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AWS = require("aws-sdk");
function getImagesPath() {
    if (process.env.NODE_ENV != 'production') {
        return process.env.NODE_ENV + '/images/';
    }
    else {
        return 'images/';
    }
}
exports.getImagesPath = getImagesPath;
function saveImage(content, fileName) {
    return new Promise((resolve, reject) => {
        const params = { Bucket: 'insider-oil', Key: fileName };
        var s3 = new AWS.S3({ params });
        s3.getBucketAcl(function (err) {
            if (err) {
                reject(err);
            }
            else {
                const uploadParams = {
                    Body: content,
                    ACL: 'public-read',
                    ContentEncoding: 'image/jpeg',
                };
                s3.upload(uploadParams, function (err) {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve('ok');
                    }
                });
            }
        });
    });
}
exports.saveImage = saveImage;
function fileNameById(id) {
    return 'img_' + id + '.jpg';
}
exports.fileNameById = fileNameById;
//# sourceMappingURL=AWS.js.map