"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const TableQueries = require("../db/queries/TableQueries");
const QTT_SPECIAL_NAME = 'qtt*';
const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
const sources = [
    {
        sourceName: 'DrillingRigs',
        groupFields: [
            'contractor_name',
            'operator_name',
            'land_sea',
            'status',
            'start',
            'end',
            'day_rate'
        ],
        valueFields: [
            'day_rate'
        ]
    },
    {
        sourceName: 'Wells',
        groupFields: [
            'operator_name',
            'block_name',
            'dr_name',
            'type',
            'category',
            'situation',
            'start',
            'end',
            'conclusion'
        ],
        valueFields: []
    },
    {
        sourceName: 'Blocks',
        groupFields: [
            'basin_name',
            'operator_name',
            'status',
        ],
        valueFields: []
    },
    {
        sourceName: 'FPSOs',
        groupFields: [
            'of_name',
            'b_name',
            'pu_status',
            'ow_name',
            'op_name',
            'situation',
        ],
        valueFields: []
    },
    {
        sourceName: 'FixedProductionUnits',
        groupFields: [
            'of_name',
            'b_name',
            'pu_status',
            'ow_name',
            'op_name',
            'situation',
        ],
        valueFields: []
    },
    {
        sourceName: 'SemiSubmersibleProductionUnits',
        groupFields: [
            'of_name',
            'b_name',
            'pu_status',
            'ow_name',
            'op_name',
            'situation',
        ],
        valueFields: []
    },
    {
        sourceName: 'oilFielsdProduction',
        groupFields: [
            'b_name',
            'state',
            'land_sea',
        ],
        valueFields: []
    },
    {
        sourceName: 'oilFieldsDevelopment',
        groupFields: [
            'b_name',
            'state',
            'land_sea',
        ],
        valueFields: []
    },
    {
        sourceName: 'Seismics',
        groupFields: [
            'authorized_company',
            'block_name',
            'basin_name',
            'dou_publi_date',
            'end_date'
        ],
        valueFields: []
    },
    {
        sourceName: 'Contracts',
        groupFields: [
            'supplier',
            'co_name',
            'c_situation',
            'type',
            'start',
            'end',
            'is_name',
            'value',
        ],
        valueFields: [
            'day_rate',
            'value'
        ]
    },
    {
        sourceName: 'Boats',
        groupFields: [
            'type',
            'ow_name',
            'op_name',
        ],
        valueFields: []
    },
];
function getAFields(fieldsList, fieldsMap) {
    return fieldsList.map((fieldName) => {
        return {
            name: fieldName,
            label: fieldsMap[fieldName].label
        };
    });
}
function getSources() {
    const result = sources.map((source) => {
        const tableParams = TableQueries.queries[source.sourceName];
        const fieldsMap = {};
        for (let bqf of tableParams.fields) {
            if (bqf.fieldName) {
                fieldsMap[bqf.fieldName] = bqf;
            }
            else {
                fieldsMap[bqf.ref.valueField] = bqf;
            }
        }
        let valueFields = [
            {
                name: QTT_SPECIAL_NAME,
                label: 'Quantidade'
            }
        ];
        valueFields = valueFields.concat(getAFields(source.valueFields, fieldsMap));
        const fs = {
            sourceName: source.sourceName,
            label: tableParams.title,
            groupFields: getAFields(source.groupFields, fieldsMap),
            valueFields: valueFields,
        };
        return fs;
    });
    return result;
}
exports.getSources = getSources;
function getValueSelectStr(valueField) {
    return valueField == QTT_SPECIAL_NAME ? 'count(*)' : 'sum(' + valueField + ')';
}
function getItemsQuery(baseQueryStr, groupField, valueField, maxNumItems) {
    const selectValueStr = getValueSelectStr(valueField);
    const select = 'select ' + selectValueStr + ' as value, tb.' + groupField + ' as label ';
    const fromStr = ' from (' + baseQueryStr + ') as tb ';
    const group = ' group by tb.' + groupField;
    const order = ' order by value desc ';
    const limit = ' limit 0, ' + maxNumItems;
    const queryStr = select + fromStr + group + order + limit;
    return queryStr;
}
function getDateBasedQuery(baseQueryStr, groupField, valueField, maxNumItems) {
    const selectValueStr = getValueSelectStr(valueField);
    const yearStr = ' year(tb.' + groupField + ') ';
    const select = 'select ' + selectValueStr + ' as value, ' + yearStr + ' as label ';
    const fromStr = ' from (' + baseQueryStr + ') as tb ';
    const where = ' where ' + groupField + ' is not null ';
    const group = ' group by ' + yearStr;
    const order = ' order by label ';
    const queryStr = select + fromStr + where + group + order;
    return queryStr;
}
function getCurrencyRangeValues(baseQueryStr, groupField) {
    return __awaiter(this, void 0, void 0, function* () {
        const selectMinMax = 'select max(' + groupField + ') as maxv, min(' + groupField + ') as minv ';
        const fromStr = ' from (' + baseQueryStr + ') as tb ';
        const minMaxQry = selectMinMax + fromStr;
        const minMax = yield db.sequelize.query(minMaxQry, simpleQueryType);
        const min = minMax[0].minv;
        const max = minMax[0].maxv;
        const exp10 = Math.log(max) / Math.log(10);
        const truncExp10 = Math.floor(exp10);
        const power10 = Math.pow(10, truncExp10);
        return power10;
    });
}
function normalizeLabelAxis(rawItems, max) {
    const result = [];
    const minFactor = parseInt(rawItems[0].label) / max;
    const maxFactor = parseInt(rawItems[rawItems.length - 1].label) / max;
    for (let i = minFactor; i <= maxFactor; i++) {
        const currValue = i * max;
        if (currValue == parseInt(rawItems[0].label)) {
            result.push(rawItems[0]);
            rawItems.shift();
        }
        else {
            result.push({
                label: currValue.toString(),
                value: 0
            });
        }
    }
    return result;
}
function getCurrencyBasedQuery(baseQueryStr, groupField, valueField, maxNumItems) {
    return __awaiter(this, void 0, void 0, function* () {
        const max = yield getCurrencyRangeValues(baseQueryStr, groupField);
        const groupFieldStr = ' floor(' + groupField + ' / ' + max + ') * ' + max;
        const selectValueStr = getValueSelectStr(valueField);
        const select = 'select ' + selectValueStr + ' as value, ' + groupFieldStr + ' as label ';
        const fromStr = ' from (' + baseQueryStr + ') as tb ';
        const where = ' where ' + groupField + ' is not null ';
        const group = ' group by label ';
        const order = ' order by label ';
        const queryStr = select + fromStr + where + group + order;
        const rawItems = yield db.sequelize.query(queryStr, simpleQueryType);
        return normalizeLabelAxis(rawItems, max);
    });
}
function getTotalQuery(baseQueryStr, valueField) {
    const selectValueStr = getValueSelectStr(valueField);
    const select = 'select ' + selectValueStr + ' as value ';
    const fromStr = ' from (' + baseQueryStr + ') as tb ';
    const queryStr = select + fromStr;
    return queryStr;
}
function sumItems(items) {
    let result = 0;
    for (let item of items) {
        result += item.value;
    }
    return result;
}
function getResult(sourceName, groupField, valueField, maxNumItems, filters) {
    return __awaiter(this, void 0, void 0, function* () {
        const queryParams = {
            order: [],
            filters,
            pagination: null
        };
        const tQuery = TableQueries.queries[sourceName];
        const baseQueryStr = tQuery.queryStrFn(queryParams);
        const groupFieldInfo = tQuery.fields.find((f) => {
            if (f.fieldName) {
                return f.fieldName == groupField;
            }
            else {
                return f.ref.valueField == groupField;
            }
        });
        let items = null;
        let othersValue = 0;
        if (groupFieldInfo && groupFieldInfo.type == 'DATE') {
            const itemsQuery = getDateBasedQuery(baseQueryStr, groupField, valueField, maxNumItems);
            items = yield db.sequelize.query(itemsQuery, simpleQueryType);
        }
        else if (groupFieldInfo && groupFieldInfo.type == 'CURRENCY') {
            items = yield getCurrencyBasedQuery(baseQueryStr, groupField, valueField, maxNumItems);
        }
        else {
            const itemsQuery = getItemsQuery(baseQueryStr, groupField, valueField, maxNumItems);
            items = yield db.sequelize.query(itemsQuery, simpleQueryType);
            const totalQuery = getTotalQuery(baseQueryStr, valueField);
            const total = (yield db.sequelize.query(totalQuery, simpleQueryType))[0].value;
            othersValue = total - sumItems(items);
        }
        return {
            items,
            othersValue
        };
    });
}
exports.getResult = getResult;
//# sourceMappingURL=Analytics.js.map