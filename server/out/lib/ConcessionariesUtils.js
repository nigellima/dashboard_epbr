'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const await_1 = require("../lib/await");
function updateConcessionaries(db, modelName, id_field, object) {
    const where = {};
    where[id_field] = object.id;
    const options = { where };
    return db[modelName].destroy(options).then(function () {
        const concessionaries = object.dataValues.concessionaries;
        const concessionaries_props = object.dataValues.concessionaries_props;
        if (!concessionaries || !concessionaries_props)
            return null;
        if (concessionaries.length != concessionaries_props.length)
            throw 'Tamanhos diferentes de concessionários';
        const newConcessionariesRecords = [];
        var prop_sum = 0;
        for (var i = 0; i < concessionaries.length; i++) {
            const ofcRecord = {
                company_id: concessionaries[i].id,
                prop: concessionaries_props[i] / 100.0
            };
            ofcRecord[id_field] = object.id;
            newConcessionariesRecords.push(ofcRecord);
            prop_sum += concessionaries_props[i] * 1.0;
        }
        if (Math.abs(prop_sum - 100.0) > 0.0001)
            throw 'Valores somam ' + prop_sum + '%';
        return db[modelName].bulkCreate(newConcessionariesRecords);
    });
}
exports.updateConcessionaries = updateConcessionaries;
function getConcessionaries(sequelize, id, associationTableName, idFieldName) {
    const select = 'select c.name, c.id, ofc.prop ';
    const fromStr = 'from ' + associationTableName + ' ofc ';
    const companyJoin = ' left outer join companies c on ofc.company_id = c.id ';
    const where = 'where ofc.' + idFieldName + ' = ' + id;
    const order = ' order by ofc.id';
    const queryStr = select + fromStr + companyJoin + where + order;
    const simpleQueryType = { type: sequelize.QueryTypes.SELECT };
    const result = await_1.await(sequelize.query(queryStr, simpleQueryType));
    return result;
}
exports.getConcessionaries = getConcessionaries;
function getConcessionariesProps(sequelize, id, associationTableName, idFieldName) {
    const select = 'select round(ofc.prop * 100, 2) as prop ';
    const fromStr = 'from ' + associationTableName + ' ofc ';
    const where = 'where ofc.' + idFieldName + ' = ' + id;
    const order = ' order by ofc.id';
    const queryStr = select + fromStr + where + order;
    const simpleQueryType = { type: sequelize.QueryTypes.SELECT };
    const result = await_1.await(sequelize.query(queryStr, simpleQueryType));
    return result.map(value => { return value.prop; });
}
exports.getConcessionariesProps = getConcessionariesProps;
function getFormattedConcessionaries(sequelize, id, associationTableName, idFieldName) {
    const select = 'select c.name, round(ofc.prop * 100, 2) as percent ';
    const fromStr = 'from ' + associationTableName + ' ofc ';
    const companyJoin = ' left outer join companies c on ofc.company_id = c.id ';
    const where = 'where ofc.' + idFieldName + ' = ' + id;
    const order = ' order by ofc.id';
    const queryStr = select + fromStr + companyJoin + where + order;
    const simpleQueryType = { type: sequelize.QueryTypes.SELECT };
    const result = await_1.await(sequelize.query(queryStr, simpleQueryType));
    let concessionaries = '';
    for (let row of result) {
        concessionaries += row.name + ': ' + row.percent + '%\n';
    }
    return concessionaries.substr(0, concessionaries.length - 1);
}
exports.getFormattedConcessionaries = getFormattedConcessionaries;
//# sourceMappingURL=ConcessionariesUtils.js.map