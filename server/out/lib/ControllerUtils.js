"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston = require("winston");
function getErrorFunc(res, errorCode, msg) {
    return function (error) {
        if (!error)
            return;
        var errors = error.errors ? error.errors : [];
        if ((typeof error) == 'string') {
            errors.push({ message: error });
            winston.error(error);
        }
        const errObj = {
            errorMsg: msg,
            errors: errors
        };
        if (error.message) {
            errObj.errorMsg += ' ' + error.message;
        }
        winston.error(error.stack);
        res.status(errorCode).json(errObj);
    };
}
exports.getErrorFunc = getErrorFunc;
function getOkFunc(res, msg) {
    return function returnOkJson() {
        res.json({ msg: msg });
        return null;
    };
}
exports.getOkFunc = getOkFunc;
//# sourceMappingURL=ControllerUtils.js.map