"use strict";
const Basin = {
    fields: {
        name: {
            label: 'Nome'
        }
    },
    referencedObjectsOnView: [
        {
            queryName: 'oilFieldsByBasin',
            title: 'Campos'
        },
        {
            queryName: 'blocksByBasin',
            title: 'Blocos'
        },
    ],
    labelField: 'name',
    gridFields: ['name'],
    tableLabel: 'Bacias',
    labelSingular: 'Bacia',
};
module.exports = Basin;
//# sourceMappingURL=Basin.js.map