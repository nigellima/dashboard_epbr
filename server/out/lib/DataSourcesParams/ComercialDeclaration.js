"use strict";
const ComercialDeclaration = {
    fields: {
        block_id: {
            label: 'Bloco'
        },
        block_name: {
            label: 'Bloco'
        },
        attached: {
            label: 'Anexado'
        },
        date: {
            label: 'Data'
        },
    },
    labelField: 'block_name',
    gridFields: ['block_name', 'basin_name', 'oil_field_name', 'date'],
    tableLabel: 'Declarações de comercialidade',
    labelSingular: 'Declaração de comercialidade',
};
module.exports = ComercialDeclaration;
//# sourceMappingURL=ComercialDeclaration.js.map