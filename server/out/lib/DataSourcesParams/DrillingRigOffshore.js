"use strict";
const DrillingRigOffshore = {
    fields: {
        name: {
            label: "Sonda Offshore"
        },
        contractor_name: {
            label: "Contratante"
        },
        contractor_id: {
            label: "Contratante"
        },
        operator_name: {
            label: "Operador"
        },
        operator_id: {
            label: "Operador"
        },
        type: {
            label: "Tipo"
        },
        status: {
            label: "Status"
        },
        lda: {
            label: "LDA"
        },
        start: {
            label: "Início"
        },
        end: {
            label: "Fim"
        },
        day_rate: {
            label: "Day rate (US$)",
            isCurrency: true
        },
        photo: {
            label: 'Foto',
            isPhoto: true
        },
        info: {
            label: "Informações"
        },
        coords_admin: {
            label: 'Coordenadas (admin)'
        },
    },
    labelField: "name",
    gridFields: ["name", "contractor_name", "type", "status", "lda", "start", "end"],
    tableLabel: "Sondas offshore",
    labelSingular: 'Sonda offshore',
    excelParams: {
        keyField: "sonda",
        fields: {
            tipo: 'type',
            sonda: 'name',
            contratada: 'contractor',
            status: 'status',
            lda: 'lda',
            início: 'start',
            fim: 'end',
            'day rate': 'day_rate',
        }
    },
    referencedObjectsOnView: [
        {
            queryName: 'wellsByDrillingRigOffshore',
            title: 'Poços'
        },
    ]
};
module.exports = DrillingRigOffshore;
//# sourceMappingURL=DrillingRigOffshore.js.map