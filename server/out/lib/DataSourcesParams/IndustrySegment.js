"use strict";
const IndustrySegment = {
    fields: {
        name: {
            label: 'Nome'
        },
    },
    labelField: 'name',
    gridFields: ['name'],
    tableLabel: 'Segmentos',
    labelSingular: 'Segmento',
};
module.exports = IndustrySegment;
//# sourceMappingURL=IndustrySegment.js.map