"use strict";
const Bid = {
    fields: {
        period: {
            label: "Data"
        },
        production_unit_name: {
            label: 'Unidade de produção'
        },
        production_unit_id: {
            label: 'Unidade de produção'
        },
    },
    labelField: "period",
    gridFields: ["period", "production_unit_name"],
    tableLabel: "Datas de manutenção",
    labelSingular: 'Data de manutenção',
};
module.exports = Bid;
//# sourceMappingURL=MaintenanceDate.js.map