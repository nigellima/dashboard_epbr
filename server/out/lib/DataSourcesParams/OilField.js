"use strict";
const OilField = {
    fields: {
        name: {
            label: "Nome"
        },
        basin_id: {
            label: "Bacia"
        },
        basin_name: {
            label: "Bacia"
        },
        state: {
            label: "Estado"
        },
        concessionaries: {
            label: "Concessionárias",
            isConcessionaries: true,
            isManyToMany: true,
            comboSource: 'Company'
        },
        concessionaries_props: {
            label: "Concessionárias % (admin)",
            isList: true
        },
        formatted_shore: {
            label: "Terra/Mar"
        },
        shore: {
            label: "ignorar"
        },
        stage: {
            label: "Estágio (admin)"
        },
        updates: {
            label: "Atualizações"
        },
        block_name: {
            label: "Bloco"
        },
        block_id: {
            label: "Bloco"
        },
        operator_name: {
            label: "Operador"
        },
        operator_id: {
            label: "Operador"
        },
        polygons_admin: {
            label: "Polígonos (admin)",
            isTextArea: true
        },
        polygons: {
            label: "Polígonos (admin) (somente leitura)"
        },
    },
    labelField: "name",
    gridFields: ['name', 'basin_name', 'state', 'shore', 'stage'],
    tableLabel: "Campos",
    labelSingular: 'Campo',
    referencedObjectsOnView: [
        {
            queryName: 'hydrocarbonEvidencesByOilField',
            title: 'Indícios de hidrocarbonetos'
        },
        {
            queryName: 'productionUnitByOilField',
            title: 'Unidades de produção'
        },
    ],
    excelParams: {
        keyField: "nome",
        fields: {
            'nome': 'name',
            'bacia': 'basin',
            'bloco': 'block',
            'estado': 'state',
            'concessionárias': 'concessionaries',
            'terra/mar': 'shore',
            'estágio': 'stage',
            'atualizações': 'updates',
        }
    },
};
module.exports = OilField;
//# sourceMappingURL=OilField.js.map