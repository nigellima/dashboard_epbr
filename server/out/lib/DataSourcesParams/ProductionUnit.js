"use strict";
const ProductionUnit = {
    fields: {
        name: {
            label: 'Unit Name'
        },
        oil_field_id: {
            label: 'Field Name'
        },
        oil_field: {
            label: 'Field Name'
        },
        block_id: {
            label: 'Exploration Block'
        },
        block: {
            label: 'Exploration Block'
        },
        type: {
            label: 'Type'
        },
        hull_type: {
            label: 'Hull Type'
        },
        status: {
            label: 'Status'
        },
        coords_admin: {
            label: 'Coordinates'
        },
        general_info: {
            label: 'General Info'
        },
        main_fluid: {
            label: 'Main Fluid'
        },
        compression: {
            label: 'Compression'
        },
        power_generation: {
            label: 'Power Generation'
        },
        automation_scope: {
            label: 'Automation Scope'
        },
        fpso_award: {
            label: 'Expected quarter for FPSO award'
        },
        owner_id: {
            label: 'Current Field Ownership'
        },
        owner: {
            label: 'Current Field Ownership'
        },
        former_owner_id: {
            label: 'Former Field Ownership'
        },
        former_owner: {
            label: 'Former Field Ownership'
        },
        situation: {
            label: 'Situação'
        },
        oil_processing_capacity: {
            label: 'Oil Processing Capacity (b/d)'
        },
        gas_processing_capacity: {
            label: 'Gas Compression Capacity (m³/d)'
        },
        liquids_processing_capacity: {
            label: 'Liquids Processing Capacity (b/d)'
        },
        local_content_percentage: {
            label: 'Local Content for Development or Production (%)'
        },
        contractors_deadline: {
            label: 'Contractor-bidders deadline for FPSO RFQ delivery'
        },
        depth: {
            label: "Lâmina d'água"
        },
        first_oil: {
            label: 'Predictable Quarter for First Oil'
        },
        day_rate: {
            label: "Day rate (US$)",
            isCurrency: true
        },
        photo: {
            label: 'Photo',
            isPhoto: true
        },
        awarded_contractor_id: {
            label: 'Awarded Contractor'
        },
        awarded_contractor: {
            label: 'Awarded Contractor'
        },
        eng_company_id: {
            label: 'Engineering Company for Pre-FEED and FEED'
        },
        eng_company: {
            label: 'Engineering Company for Pre-FEED and FEED'
        },
        co2_percentage: {
            label: '% of CO2'
        },
    },
    labelField: 'name',
    gridFields: ['name', 'oil_field', 'block'],
    tableLabel: 'Production Unit',
    labelSingular: 'Production Unit',
    excelParams: {
        keyField: "name",
        fields: {
            'name': 'name'
        }
    },
    referencedObjectsOnView: [
        {
            queryName: 'maintenanceDatesByProductionUnit',
            title: 'Datas de manutenção'
        },
    ]
};
module.exports = ProductionUnit;
//# sourceMappingURL=ProductionUnit.js.map