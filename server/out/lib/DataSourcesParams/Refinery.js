"use strict";
const Refinery = {
    fields: {
        name: {
            label: 'Nome'
        },
        address: {
            label: 'Endereço'
        },
        telephones: {
            label: 'Telefone',
            isList: true
        },
        capacity: {
            label: 'Capacidade'
        },
        info: {
            label: "Informações"
        },
    },
    labelField: 'name',
    gridFields: ['name', 'capacity'],
    tableLabel: 'Refinarias',
    labelSingular: 'Refinaria',
};
module.exports = Refinery;
//# sourceMappingURL=Refinery.js.map