"use strict";
const Terminal = {
    fields: {
        name: {
            label: 'Nome'
        },
        address: {
            label: 'Endereço'
        },
        type: {
            label: "Terrestre/Marítimo"
        },
        info: {
            label: "Informações"
        },
    },
    labelField: 'name',
    gridFields: ['name', 'type'],
    tableLabel: 'Terminais',
    labelSingular: 'Terminal',
};
module.exports = Terminal;
//# sourceMappingURL=Terminal.js.map