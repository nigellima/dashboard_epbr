"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const TableQueries_1 = require("../db/queries/TableQueries");
const db = require("../db/models");
function getFilterResult(qryName, fieldName) {
    return __awaiter(this, void 0, void 0, function* () {
        const queryParams = {
            order: [],
            filters: [],
            pagination: {
                first: 0,
                itemsPerPage: 300
            }
        };
        const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
        const baseQueryStr = TableQueries_1.queries[qryName].queryStrFn(queryParams);
        const select = 'select count(*) as qtt, tb.' + fieldName + ' as value ';
        const fromStr = ' from (' + baseQueryStr + ') as tb ';
        const group = ' group by tb.' + fieldName;
        const order = ' order by value ';
        const queryStr = select + fromStr + group + order;
        const recordsPromise = db.sequelize.query(queryStr, simpleQueryType);
        const records = yield recordsPromise;
        return records;
    });
}
exports.getFilterResult = getFilterResult;
//# sourceMappingURL=Filters.js.map