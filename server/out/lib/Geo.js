"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function coordToString(coords) {
    if (!coords) {
        return null;
    }
    return coords.lat + ', ' + coords.lng;
}
exports.coordToString = coordToString;
function stringToCoord(strWithCoords) {
    if (!strWithCoords || strWithCoords.length == 0) {
        return null;
    }
    const parts = strWithCoords.split(',');
    const lat = parseFloat(parts[0].trim());
    const lng = parseFloat(parts[1].trim());
    return { lat, lng };
}
exports.stringToCoord = stringToCoord;
function polygonsToStr(polygons) {
    const polygonsStrs = [];
    for (var polygon of polygons) {
        const pointStrs = [];
        for (var point of polygon) {
            pointStrs.push(coordToString(point));
        }
        polygonsStrs.push(pointStrs.join('\n'));
    }
    return polygonsStrs.join('\n*\n');
}
exports.polygonsToStr = polygonsToStr;
function strToPolygons(polygonsStr_) {
    const polygons = [];
    const polygonsStr = polygonsStr_.split('*');
    for (var polygonStr of polygonsStr) {
        const polygon = [];
        const points = polygonStr.split('\n');
        for (var point of points) {
            const coords = point.split(',');
            if (coords.length != 2) {
                continue;
            }
            const lat = parseFloat(coords[0].trim());
            const lng = parseFloat(coords[1].trim());
            polygon.push({ lat, lng });
        }
        polygons.push(polygon);
    }
    return polygons;
}
exports.strToPolygons = strToPolygons;
//# sourceMappingURL=Geo.js.map