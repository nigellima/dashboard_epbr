"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
exports.getRecordOptions = {
    'SingleNews': {
        model: db.models.News,
        seqOptions: {
            attributes: [
                'title',
                'content',
                'tableau_url',
                'created_at'
            ],
            include: [{
                    model: db.models.News.associations['production_unit'].target,
                    as: 'production_unit',
                    attributes: ['name']
                }]
        }
    }
};
//# sourceMappingURL=GetRecordOptions.js.map