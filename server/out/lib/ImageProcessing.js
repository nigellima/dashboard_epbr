"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lwip = require("lwip");
function resampleImage(image, width, height) {
    const imgProp = image.width() / image.height();
    const desiredProp = width / height;
    var resizeWidth, resizeHeight;
    if (imgProp > desiredProp) {
        resizeWidth = height * imgProp;
        resizeHeight = height;
    }
    else {
        resizeWidth = width;
        resizeHeight = width / imgProp;
    }
    return image.batch()
        .resize(resizeWidth, resizeHeight)
        .crop(width, height);
}
exports.resampleImage = resampleImage;
function resample(buffer, width, height) {
    return new Promise(open);
    function open(resolve, reject) {
        lwip.open(buffer, 'jpg', execute.bind(this, resolve, reject));
    }
    function execute(resolve, reject, err, image) {
        if (err) {
            reject(err);
            return;
        }
        const batch = resampleImage(image, width, height);
        batch.toBuffer("jpg", {}, save.bind(this, resolve, reject));
    }
    function save(resolve, reject, err, processedBuffer) {
        if (err) {
            reject(err);
            return;
        }
        resolve(processedBuffer);
    }
}
exports.resample = resample;
//# sourceMappingURL=ImageProcessing.js.map