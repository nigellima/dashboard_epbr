"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const QueryGenerator = require("../db/queries/QueryGenerator");
const TableQueries_1 = require("../db/queries/TableQueries");
const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
function getProductionUnitsInsideAreaQuery(geoLimits) {
    const options = {
        table: {
            name: 'production_units',
            fields: [
                'id',
                'name',
            ]
        },
        extraFields: [
            ['"ProductionUnit"', 'model']
        ],
        joinTables: [],
        where: [
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lat") >= ' + geoLimits.latMin },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lat") <= ' + geoLimits.latMax },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lng") >= ' + geoLimits.lngMin },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lng") <= ' + geoLimits.lngMax }
        ],
        order: []
    };
    const puQueryStr = QueryGenerator.generate(options);
    return puQueryStr;
}
function geDrillingRigsInsideAreaQuery(geoLimits) {
    const queryParams = {
        order: [],
        filters: [
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lat") >= ' + geoLimits.latMin },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lat") <= ' + geoLimits.latMax },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lng") >= ' + geoLimits.lngMin },
            { customFilter: 'JSON_EXTRACT(coordinates, "$.lng") <= ' + geoLimits.lngMax }
        ],
        pagination: {
            first: 0,
            itemsPerPage: 300
        }
    };
    const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
    let drQueryStr = TableQueries_1.queries['DrillingRigs'].queryStrFn(queryParams);
    drQueryStr = 'select dr_id as id, dr_name as name, model from (' + drQueryStr + ') as dr';
    return drQueryStr;
}
function getItemsInsideArea(geoLimits) {
    return __awaiter(this, void 0, void 0, function* () {
        const subQueries = [];
        subQueries.push(getProductionUnitsInsideAreaQuery(geoLimits));
        subQueries.push(geDrillingRigsInsideAreaQuery(geoLimits));
        const query = '(' + subQueries.join(') union (') + ')';
        const items = yield db.sequelize.query(query, simpleQueryType);
        return items;
    });
}
exports.getItemsInsideArea = getItemsInsideArea;
//# sourceMappingURL=Maps.js.map