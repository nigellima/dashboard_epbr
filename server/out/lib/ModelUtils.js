'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require("sequelize");
const AWS = require("./AWS");
const moment = require("moment");
const Geo_1 = require("./Geo");
const awaitLib = require("../lib/await");
exports.simpleQueryType = { type: Sequelize.QueryTypes.SELECT };
function getListFieldObj(textFieldName) {
    return {
        type: Sequelize.VIRTUAL,
        get: function () {
            const value = this[textFieldName];
            if (!value)
                return null;
            return JSON.parse(value);
        },
        set: function (newValue) {
            this[textFieldName] = JSON.stringify(newValue);
        }
    };
}
exports.getListFieldObj = getListFieldObj;
function getCoordFieldObj(textFieldName) {
    return {
        type: Sequelize.VIRTUAL,
        get: function () {
            const coordsValue = this[textFieldName];
            if (!coordsValue || coordsValue.length == 0) {
                return null;
            }
            return Geo_1.coordToString(JSON.parse(coordsValue));
        },
        set: function (coordsStr) {
            const coords = Geo_1.stringToCoord(coordsStr);
            this[textFieldName] = JSON.stringify(coords);
        }
    };
}
exports.getCoordFieldObj = getCoordFieldObj;
function getObjRefField() {
    const db = require('../db/models');
    if (!this.model_name)
        return [];
    const referencedModel = db.models[this.model_name];
    const referencedObj = awaitLib.await(referencedModel.findById(this.obj_id));
    const refObjName = referencedObj ? referencedObj.name : null;
    return [{
            id: this.obj_id,
            model_id: this.model_id,
            model: this.model_name,
            name: refObjName
        }];
}
exports.getObjRefField = getObjRefField;
function formatImageFileName(modelName, id) {
    const fileName = AWS.getImagesPath() + modelName + '/img_' + id + '_original.jpg';
    return fileName;
}
exports.formatImageFileName = formatImageFileName;
function saveOriginalImage(imgBytes, modelName, id) {
    return __awaiter(this, void 0, void 0, function* () {
        const imgArray = imgBytes;
        if (!imgArray)
            return;
        const imgBuffer = new Buffer(imgArray);
        const fileName = formatImageFileName(modelName, id);
        return AWS.saveImage(imgBuffer, fileName);
    });
}
exports.saveOriginalImage = saveOriginalImage;
function fieldTypeStr(field) {
    let typeStr = 'VARCHAR';
    try {
        typeStr = field.type.toString();
    }
    catch (e) {
        typeStr = 'ENUM';
    }
    return typeStr;
}
exports.fieldTypeStr = fieldTypeStr;
function saveRecordUpdates(modelName, record, newData) {
    if (modelName == 'News')
        return Promise.resolve();
    const db = require('../db/models');
    const dataSource = db.models[modelName];
    const modifiedRecords = [];
    for (var fieldName in newData) {
        let newValue = newData[fieldName];
        let oldValue = record[fieldName];
        let field = dataSource.attributes[fieldName];
        if (!field)
            continue;
        let typeStr = fieldTypeStr(field);
        if (typeStr == 'DATE') {
            newValue = moment(newValue).utcOffset(0).format('DD/MM/YYYY');
            oldValue = moment(oldValue).utcOffset(0).format('DD/MM/YYYY');
        }
        else if (oldValue && oldValue.constructor == Array) {
            newValue = JSON.stringify(newValue);
            oldValue = JSON.stringify(oldValue);
        }
        if (newValue != oldValue) {
            modifiedRecords.push(fieldName);
        }
    }
    const update = {
        model: modelName,
        obj_id: record.id,
        type: 'EDIT',
        updates: JSON.stringify(modifiedRecords)
    };
    return db.models.UpdateLog.create(update);
}
exports.saveRecordUpdates = saveRecordUpdates;
//# sourceMappingURL=ModelUtils.js.map