'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var Sync = require('sync');
function executePromisesSequentialy(promises) {
    var promise = promises[0];
    for (var i = 1; i < promises.length; i++) {
        promise = promise.then();
    }
    return promise;
}
exports.executePromisesSequentialy = executePromisesSequentialy;
function syncify(func) {
    return new Promise((resolve, reject) => {
        Sync(() => {
            func();
            resolve();
        }, (err) => { reject(err); });
    });
}
exports.syncify = syncify;
function syncifyES7(func) {
    return new Promise((resolve, reject) => {
        function f() {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    const result = func();
                    resolve(result);
                }
                catch (err) {
                    reject(err);
                }
            });
        }
        f();
    });
}
exports.syncifyES7 = syncifyES7;
//# sourceMappingURL=PromiseUtils.js.map