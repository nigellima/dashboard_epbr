"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const TableQueries_1 = require("../db/queries/TableQueries");
const dsParams = require("../lib/DataSourcesParams");
const winston = require("winston");
function translate(requestLogItem) {
    return __awaiter(this, void 0, void 0, function* () {
        const path = requestLogItem.path;
        let queryObj;
        try {
            queryObj = JSON.parse(requestLogItem.query);
            if ((typeof queryObj) == 'string') {
                queryObj = JSON.parse(queryObj);
            }
            if (path == '/get_table_data') {
                const typedQueryObj = queryObj;
                const tableQuery = TableQueries_1.queries[typedQueryObj.queryName];
                return Promise.resolve('Lista: ' + tableQuery.title);
            }
            else if (path == '/view_record/') {
                const typedQueryObj = queryObj;
                const model = db.models[typedQueryObj.dataSource];
                const params = dsParams[typedQueryObj.dataSource];
                const record = yield model.findById(typedQueryObj.id);
                return Promise.resolve(params.labelSingular + ': ' + record[params.labelField]);
            }
            else if (path == '/search') {
                return Promise.resolve('Busca: ' + queryObj.searchValue);
            }
            else if (path == '/get_record/') {
                if (queryObj.optionsName != 'SingleNews') {
                    return Promise.resolve(requestLogItem.query);
                }
                const insight = yield db.models.News.findById(queryObj.id);
                return Promise.resolve('Insight: ' + insight.title);
            }
        }
        catch (err) {
            winston.error(err);
            return Promise.resolve(requestLogItem.query);
        }
        return Promise.resolve(requestLogItem.query);
    });
}
exports.translate = translate;
//# sourceMappingURL=RequestLogTranslator.js.map