"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function formatPercentage(prop) {
    return (prop * 100.0).toFixed(2).replace('.', ',') + '%';
}
exports.formatPercentage = formatPercentage;
//# sourceMappingURL=StringUtils.js.map