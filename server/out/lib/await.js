"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Sync = require('sync');
function transPro(promise, callback) {
    var promiseResponse = promise.then(function (result) {
        callback(null, result);
        return null;
    });
    if (promiseResponse.thenCatch) {
        promiseResponse.thenCatch(function (error) {
            console.error(error.stack);
            callback(error);
            throw error;
        });
    }
    else if (promiseResponse.catch) {
        promiseResponse.catch(function (error) {
            console.error(error.stack);
            callback(error);
            throw error;
        });
    }
    return promiseResponse;
}
;
function await(promise) {
    const transProTemp = transPro;
    return transProTemp.sync(null, promise);
}
exports.await = await;
//# sourceMappingURL=await.js.map