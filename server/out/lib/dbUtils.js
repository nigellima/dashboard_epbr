"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
var DataSources = require('./DataSources');
const QueryGenerator = require("../db/queries/QueryGenerator");
const TableQueries = require("../db/queries/TableQueries");
function assignObjects(objDst, objSrc) {
    if (!objDst)
        objDst = {};
    for (let attName in objSrc) {
        objDst[attName] = objSrc[attName];
    }
    return objDst;
}
function getDataSource(dataSourceName) {
    const model = db.models[dataSourceName];
    if (model)
        return model;
    else {
        return createDataSource(dataSourceName);
    }
}
exports.getDataSource = getDataSource;
function findAllCustom(model, options) {
    options = options ? options : {};
    options.where = options.where ? options.where : {};
    options.include = [{ all: true }];
    return model.findAll(options);
}
exports.findAllCustom = findAllCustom;
function simplifyItem(model, item) {
    for (var associationName in model.associations) {
        const association = model.associations[associationName];
        for (var att in association.target.attributes) {
            if (att == 'id')
                continue;
            const fieldName = association.as + '_' + att;
            if (item[association.as])
                item.dataValues[fieldName] = item[association.as][att];
            else
                item.dataValues[fieldName] = '';
        }
    }
}
function simplifyArray(model, array) {
    for (var i = 0; i < array.length; i++) {
        simplifyItem(model, array[i]);
    }
}
exports.simplifyArray = simplifyArray;
function createDataSource(dataSourceName) {
    var dataSourceParams = DataSources[dataSourceName];
    if (!dataSourceParams)
        return null;
    var dataSource = {
        name: dataSourceParams.modelName,
        create: function (newItemData) {
            for (var filterField in dataSourceParams.filters)
                newItemData[filterField] = dataSourceParams.filters[filterField];
            return dataSourceParams.model.create(newItemData);
        },
        findAll: function (options) {
            options = options ? options : {};
            options.where = assignObjects(options.where, dataSourceParams.filters);
            return dataSourceParams.model.findAll(options);
        },
        findById: function (id, options) {
            return dataSourceParams.model.findById(id, options);
        },
        destroy: function (options) {
            return dataSourceParams.model.destroy(options);
        },
        count: (options) => {
            return dataSourceParams.model.count(options);
        },
        associations: dataSourceParams.model.associations,
        attributes: dataSourceParams.model.attributes,
        tableAttributes: dataSourceParams.model.tableAttributes
    };
    return dataSource;
}
function filterShowFields(records, gridFields) {
    const resultArray = [];
    for (var record of records) {
        const resultRecord = {};
        for (var gridField of gridFields) {
            resultRecord[gridField] = record.dataValues[gridField];
        }
        resultRecord.id = record.dataValues.id;
        resultArray.push(resultRecord);
    }
    return resultArray;
}
exports.filterShowFields = filterShowFields;
const extraDataParams = [
    { key: 'TableauUrl', fieldName: 'tableauUrls' },
    { key: 'EmbedStrs', fieldName: 'embedStrs' },
];
function saveExtraData(modelName, id, extraData) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!extraData)
            return;
        const model = db.models.ModelValueAssocation;
        for (let extraDataParam of extraDataParams) {
            const destroyOpts = {
                model_name: modelName,
                obj_id: id,
                desc: extraDataParam.key,
            };
            yield model.destroy({ where: destroyOpts });
            const rawValues = extraData[extraDataParam.fieldName];
            const selectedValues = rawValues.filter(v => { return v != ''; });
            if (selectedValues.length == 0)
                continue;
            let modelValue = {
                model_name: modelName,
                obj_id: id,
                desc: extraDataParam.key,
                value: selectedValues
            };
            yield model.create(modelValue);
        }
    });
}
exports.saveExtraData = saveExtraData;
function loadExtraData(modelName, id) {
    return __awaiter(this, void 0, void 0, function* () {
        const result = {
            tableauUrls: [],
            embedStrs: []
        };
        for (let extraDataParam of extraDataParams) {
            const findOpt = {
                desc: extraDataParam.key,
                obj_id: id,
                model_name: modelName
            };
            const record = yield db.models.ModelValueAssocation.findOne({ where: findOpt });
            if (record) {
                result[extraDataParam.fieldName] = JSON.parse(record.value);
            }
        }
        return result;
    });
}
exports.loadExtraData = loadExtraData;
function execQuery(options) {
    const simpleQueryType = { type: db.sequelize.QueryTypes.SELECT };
    const queryStr = QueryGenerator.generate(options);
    return db.sequelize.query(queryStr, simpleQueryType);
}
exports.execQuery = execQuery;
function simpleQuery(table, fieldNames) {
    const options = {
        table: {
            name: table,
            fields: fieldNames
        },
        extraFields: [],
        joinTables: [],
        where: [],
        order: []
    };
    return execQuery(options);
}
exports.simpleQuery = simpleQuery;
function getTableQueryData(reqQuery) {
    return __awaiter(this, void 0, void 0, function* () {
        const queryParams = reqQuery.queryParams;
        queryParams.filters = queryParams.filters ? queryParams.filters : [];
        const queryName = reqQuery.queryName;
        const query = TableQueries.queries[queryName];
        const fields = query.fields;
        const results = yield TableQueries.getQueryResult(queryName, queryParams);
        const records = results[0];
        if (query.recordProcessor) {
            for (var record of records) {
                yield query.recordProcessor(record);
            }
        }
        const result = {
            fields,
            records,
            count: results[1][0].count
        };
        return result;
    });
}
exports.getTableQueryData = getTableQueryData;
//# sourceMappingURL=dbUtils.js.map