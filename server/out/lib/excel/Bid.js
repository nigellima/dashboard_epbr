"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ImportExcelClass_1 = require("./ImportExcelClass");
const search_1 = require("../search");
const await_1 = require("../../lib/await");
class Bid extends ImportExcelClass_1.ImportExcel {
    setRecord(record, header, fields, rowValues, model) {
        super.setRecord(record, header, fields, rowValues, model);
        this.setOpeningTime(record, header, rowValues);
        this.setObject(record, header, rowValues);
    }
    setOpeningTime(record, header, rowValues) {
        const openingTimeIndex = header.indexOf('hora de abertura');
        var dateOpening = record.opening_moment;
        const timeFromExcel = rowValues[openingTimeIndex];
        dateOpening.setUTCHours(0);
        dateOpening.setUTCMinutes(0);
        const dateOpeningMili = dateOpening.getTime();
        const openingTimeUTCInMiliseconds = this.getTimeInMillisecondsFromExcelTime(timeFromExcel, dateOpening);
        record.opening_moment = new Date(dateOpeningMili + openingTimeUTCInMiliseconds);
    }
    setObject(record, header, rowValues) {
        const objectStr = this.valueFromHeaderName(rowValues, header, 'objeto');
        const searchResult = await_1.await(search_1.searchEqual(objectStr, 1));
        if (searchResult.length == 0) {
            record.object = [];
        }
        else {
            let object = {
                id: searchResult[0].id,
                model: searchResult[0].model
            };
            record.object = [object];
        }
    }
    valueFromHeaderName(rowValues, header, columnTitle) {
        const columnIndex = header.indexOf(columnTitle);
        return rowValues[columnIndex];
    }
}
exports.Bid = Bid;
//# sourceMappingURL=Bid.js.map