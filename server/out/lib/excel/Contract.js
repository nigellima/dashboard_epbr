"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ImportExcelClass_1 = require("./ImportExcelClass");
const search = require("../search");
const await_1 = require("../../lib/await");
class Contract extends ImportExcelClass_1.ImportExcel {
    setRecord(record, header, fields, rowValues, model) {
        super.setRecord(record, header, fields, rowValues, model);
        this.setObject(record, header, rowValues);
    }
    setObject(record, header, rowValues) {
        const objectStr = this.valueFromHeaderName(rowValues, header, 'objeto');
        if (!objectStr)
            return;
        const objects = objectStr.split(',');
        record.projects = [];
        objects.map((projectStr) => {
            const searchResult = await_1.await(search.searchEqual(objectStr, 1));
            if (searchResult.length > 0) {
                record.projects.push(searchResult[0]);
            }
        });
    }
    valueFromHeaderName(rowValues, header, columnTitle) {
        const columnIndex = header.indexOf(columnTitle);
        return rowValues[columnIndex];
    }
}
exports.Contract = Contract;
//# sourceMappingURL=Contract.js.map