"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ModelUtils_1 = require("../ModelUtils");
const db = require("../../db/models");
const dsParams = require("./../DataSourcesParams");
const winston = require("winston");
var XLSX = require('xlsx');
var Sync = require('sync');
const await_1 = require("../../lib/await");
const moment = require("moment-timezone");
const saoPauloZone = moment.tz.zone('America/Sao_Paulo');
;
function toLowerCaseFields(fields) {
    let result = {};
    for (let key in fields) {
        result[key.toLowerCase()] = fields[key];
    }
    return result;
}
class ImportExcel {
    getRowValues(worksheet, row) {
        var rowValues = [];
        var range = XLSX.utils.decode_range(worksheet['!ref']);
        var firstCol = range.s.c;
        var lastCol = range.e.c;
        for (var col = firstCol; col <= lastCol; col++) {
            var cellAddress = XLSX.utils.encode_cell({ r: row, c: col });
            var cell = worksheet[cellAddress];
            if (cell)
                rowValues.push(cell.v);
            else
                rowValues.push("");
        }
        return rowValues;
    }
    getLineOffset(worksheet) {
        return 0;
    }
    getHeader(worksheet, lineOffset) {
        var header = this.getRowValues(worksheet, lineOffset);
        for (var i = 0; i < header.length; i++)
            header[i] = header[i].toLowerCase().trim();
        return header;
    }
    validateHeader(header, modelName) {
        var excelParams = dsParams[modelName].excelParams;
        const missingFields = [];
        for (var key in excelParams.fields) {
            if (header.indexOf(key.toLowerCase()) < 0) {
                missingFields.push(key);
            }
        }
        if (missingFields.length > 0) {
            throw "O cabeçalho do arquivo Excel não possui o(s) campo(s) " + missingFields.join(', ');
        }
    }
    XLSnum2date(v) {
        var date = XLSX.SSF.parse_date_code(v);
        var val = new Date(0);
        val.setUTCFullYear(date.y);
        val.setUTCMonth(date.m - 1);
        val.setUTCDate(date.d);
        val.setUTCHours(date.H);
        val.setUTCMinutes(date.M);
        val.setUTCSeconds(date.S);
        return val;
    }
    str2date(dateStr) {
        try {
            const dateParts = dateStr.split('/');
            if (dateParts.length != 3)
                return null;
            const formattedDateStr = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];
            const resultDate = new Date(formattedDateStr);
            return resultDate;
        }
        catch (err) {
            return null;
        }
    }
    getDateValue(value) {
        if (value == '')
            return null;
        else if (typeof value == 'number')
            return this.XLSnum2date(value);
        else
            return this.str2date(value);
    }
    setRecordValueFromAssociation(record, value, headerField, association) {
        const labelField = dsParams[association.target.name].labelField;
        var searchParams = {};
        searchParams[labelField] = value;
        try {
            var associatedRecord = await_1.await(association.target.findOne({ where: searchParams }));
            if (!value || value == '') {
                record[association.identifierField] = null;
            }
            else if (associatedRecord) {
                record[association.identifierField] = associatedRecord.id;
            }
            else {
                throw "Valor '" + value + "' do campo '" + headerField + "' não encontrado.";
            }
        }
        catch (e) {
            throw "Valor '" + value + "' do campo '" + headerField + "' não encontrado.";
        }
    }
    setRecord(record, header, fields, rowValues, model) {
        const _dsParams = dsParams[model.name];
        const lowercaseFields = toLowerCaseFields(fields);
        for (var col = 0; col < header.length; col++) {
            var headerField = header[col];
            var fieldName = lowercaseFields[headerField];
            if (!fieldName)
                continue;
            var typeStr = ModelUtils_1.fieldTypeStr(model.attributes[fieldName]);
            let value = rowValues[col];
            if (!value || value == '')
                continue;
            const association = model.associations[fieldName];
            if (association) {
                this.setRecordValueFromAssociation(record, value, headerField, association);
            }
            else if (typeStr == 'DATE' || typeStr == 'DATETIME') {
                record[fieldName] = this.getDateValue(value);
            }
            else if (typeStr.includes('VARCHAR')) {
                record[fieldName] = this.cleanString(value);
            }
            else if (typeStr.includes('INTEGER')) {
                if (isNaN(value)) {
                    value = null;
                }
                record[fieldName] = value;
            }
            else {
                record[fieldName] = value;
            }
            if (_dsParams.fields[fieldName] && _dsParams.fields[fieldName].isList) {
                const result = [];
                const arrayValues = value.split(',');
                for (let arrayValue of arrayValues)
                    result.push(this.cleanString(arrayValue));
                record[fieldName] = result;
            }
        }
    }
    cleanString(input) {
        return input.toString().trim().replace(/\s+/g, ' ');
    }
    getWorkbook(excelBuf) {
        return XLSX.read(excelBuf, { type: "buffer", cellDates: true });
    }
    getRange(worksheet) {
        return XLSX.utils.decode_range(worksheet['!ref']);
    }
    genStatusStr(insertedRecords, updatedRecords, invalidStatus) {
        var status = '';
        status += "Registros criados: " + insertedRecords;
        status += "\nRegistros atualizados: " + updatedRecords;
        status += "\nRegistros inválidos: " + invalidStatus.length;
        return status;
    }
    execute(excelBuf, modelName) {
        const _this = this;
        const promise = new Promise(function (resolve, reject) {
            Sync(function () {
                try {
                    const workbook = _this.getWorkbook(excelBuf);
                    const first_sheet_name = workbook.SheetNames[0];
                    const worksheet = workbook.Sheets[first_sheet_name];
                    const lineOffset = _this.getLineOffset(worksheet);
                    const header = _this.getHeader(worksheet, lineOffset);
                    _this.validateHeader(header, modelName);
                    const excelParams = dsParams[modelName].excelParams;
                    const range = _this.getRange(worksheet);
                    const saveRecordData = {
                        worksheet,
                        row: -1,
                        keyFieldIndexInExcel: header.indexOf(excelParams.keyField),
                        modelKeyField: excelParams.fields[excelParams.keyField],
                        model: db.models[modelName],
                        header,
                        insertedRecords: 0,
                        updatedRecords: 0,
                        excelParams,
                        invalidStatus: []
                    };
                    const numRows = range.e.r;
                    for (var row = 1 + lineOffset; row <= numRows; row++) {
                        saveRecordData.row = row;
                        if (!_this.saveRecord(saveRecordData)) {
                            continue;
                        }
                        if ((row % 1000) == 0) {
                            const partialStatus = _this.genStatusStr(saveRecordData.insertedRecords, saveRecordData.updatedRecords, saveRecordData.invalidStatus);
                            db.models.ExcelImportLog.create({
                                model: modelName,
                                status: 'Atualização ' + row + '/' + numRows,
                                result: partialStatus + '\n' + saveRecordData.invalidStatus.join('\n')
                            });
                        }
                    }
                    const status = _this.genStatusStr(saveRecordData.insertedRecords, saveRecordData.updatedRecords, saveRecordData.invalidStatus);
                    db.models.ExcelImportLog.create({
                        model: modelName,
                        status: 'OK',
                        result: status + '\n' + saveRecordData.invalidStatus.join('\n')
                    });
                    resolve({ status: status, invalidRecordsStatus: saveRecordData.invalidStatus });
                }
                catch (err) {
                    winston.error('Erro ao importar: ', row, err);
                    db.models.ExcelImportLog.create({
                        model: modelName,
                        status: 'ERROR',
                        result: JSON.stringify(err)
                    });
                    reject(err);
                }
            });
        });
        return promise;
    }
    saveRecord(d) {
        const rowValues = this.getRowValues(d.worksheet, d.row);
        const searchParams = {};
        var searchKeyValue = rowValues[d.keyFieldIndexInExcel];
        searchKeyValue = this.cleanString(searchKeyValue);
        searchParams[d.modelKeyField] = searchKeyValue;
        var record = await_1.await(d.model.findOne({ where: searchParams }));
        if (record) {
            try {
                this.setRecord(record, d.header, d.excelParams.fields, rowValues, d.model);
                await_1.await(record.save());
                d.updatedRecords++;
            }
            catch (error) {
                this.addError(error, d.row, d.invalidStatus);
            }
        }
        else {
            record = {};
            try {
                this.setRecord(record, d.header, d.excelParams.fields, rowValues, d.model);
                await_1.await(d.model.create(record));
                d.insertedRecords++;
            }
            catch (error) {
                this.addError(error, d.row, d.invalidStatus);
            }
        }
        return true;
    }
    addError(error, row, invalidStatus) {
        var msg = error.message;
        if (!msg)
            msg = error;
        invalidStatus.push('Registro ' + row + ': ' + msg);
    }
    getTimeInMillisecondsFromExcelTime(excelTime, baseDate) {
        const dateOpeningMili = baseDate.getTime();
        const tzOffsetInMinutes = saoPauloZone.offset(dateOpeningMili);
        const timeFromExcelInMinutes = excelTime * 24 * 60;
        const resultTimeUTCInMinutes = timeFromExcelInMinutes + tzOffsetInMinutes;
        const result = resultTimeUTCInMinutes * 60000;
        return result;
    }
}
exports.ImportExcel = ImportExcel;
//# sourceMappingURL=ImportExcelClass.js.map