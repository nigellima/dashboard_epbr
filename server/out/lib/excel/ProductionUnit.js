"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ImportExcelClass_1 = require("./ImportExcelClass");
class ProductionUnit extends ImportExcelClass_1.ImportExcel {
    setRecord(record, header, fields, rowValues, model) {
        super.setRecord(record, header, fields, rowValues, model);
        this.saveCoordinates(record, header, rowValues);
    }
    saveCoordinates(record, header, rowValues) {
        const lat = this.valueFromHeaderName(rowValues, header, 'latitude');
        const lng = this.valueFromHeaderName(rowValues, header, 'longitude');
        record.coordinates = JSON.stringify({ lat, lng });
    }
    valueFromHeaderName(rowValues, header, columnTitle) {
        const columnIndex = header.indexOf(columnTitle);
        return rowValues[columnIndex];
    }
}
exports.ProductionUnit = ProductionUnit;
//# sourceMappingURL=ProductionUnit.js.map