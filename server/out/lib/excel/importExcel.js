"use strict";
const ImportExcelClass_1 = require("./ImportExcelClass");
const Block_1 = require("./Block");
const Production_1 = require("./Production");
const Bid_1 = require("./Bid");
const Contract_1 = require("./Contract");
const ProductionUnit_1 = require("./ProductionUnit");
const Boat_1 = require("./Boat");
function execute(excelBuf, modelName) {
    const excelClasses = {
        'Block': Block_1.BlockC,
        'Production': Production_1.Production,
        'Bid': Bid_1.Bid,
        'Contract': Contract_1.Contract,
        'ProductionUnit': ProductionUnit_1.ProductionUnit,
        'Boat': Boat_1.Boat,
    };
    var excelClass = excelClasses[modelName];
    if (!excelClass)
        excelClass = ImportExcelClass_1.ImportExcel;
    const importExcel = new excelClass();
    return importExcel.execute(excelBuf, modelName);
}
module.exports = execute;
//# sourceMappingURL=importExcel.js.map