"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const winston = require("winston");
function userFromToken(token) {
    if (!token || token == '') {
        return Promise.resolve(null);
    }
    return db.models.User.findOne({ where: { token: token } });
}
exports.userFromToken = userFromToken;
function login(username, password) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield db.models.User.findOne({ where: { login: username } });
        if (user && user.active) {
            if (user.password == password) {
                return yield user.generateToken();
            }
            else {
                throw 'A senha está incorreta';
            }
        }
        else {
            throw 'Usuário não existe';
        }
    });
}
exports.login = login;
function authorizeHTML(req, res, next) {
    userFromToken(req.query.token).then(callback);
    function callback(user) {
        if (user) {
            req.user = user;
            next();
        }
        else
            res.redirect('/login');
        return null;
    }
}
exports.authorizeHTML = authorizeHTML;
function authorize(req, res, next, checkAdmin) {
    var token = req.query.token;
    if (!token)
        token = req.body.token;
    if (!token && req.body.params)
        token = req.body.params.token;
    if (!token) {
        invalidToken();
        return;
    }
    userFromToken(token).then(callback);
    function callback(user) {
        if (user) {
            if (checkAdmin && !user.admin) {
                winston.info('invalid admin request');
                res.status(401).json({ errorMsg: 'Not admin' });
                return;
            }
            req.user = user;
            const logObj = {
                path: req.route.path,
                agent: req.headers['user-agent'],
                query: req.query,
                user: user.login,
                method: req.method
            };
            try {
                if (req._body) {
                    logObj.request = JSON.stringify(req.body).substr(0, 250);
                }
                logObj.query = JSON.stringify(logObj.query);
                db.models.RequestLog.create(logObj).catch((error) => {
                    winston.error(error.stack);
                });
            }
            catch (err) {
                winston.error(err);
            }
            next();
        }
        else
            invalidToken();
        return null;
    }
    function invalidToken() {
        res.status(401).json({ errorMsg: 'Invalid token' });
    }
}
function authAdmin(req, res, next) {
    authorize(req, res, next, true);
}
exports.authAdmin = authAdmin;
function authUser(req, res, next) {
    authorize(req, res, next, false);
}
exports.authUser = authUser;
function logout(user) {
    user.token = '';
    user.save();
}
exports.logout = logout;
//# sourceMappingURL=session.js.map