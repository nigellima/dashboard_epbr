"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const maxCount = 1000;
        let counter = 0;
        let reqModel = db.models.RequestLog;
        while (true) {
            try {
                let options = {
                    limit: maxCount,
                    offset: counter
                };
                let records = yield reqModel.findAll(options);
                if (records.length == 0)
                    break;
                for (let record of records) {
                    try {
                        JSON.parse(record.query);
                    }
                    catch (err) {
                        record.query = "{}";
                        yield record.save();
                    }
                }
                counter += maxCount;
                console.log(records[0]);
            }
            catch (err) {
                console.log(err);
            }
        }
    });
}
main();
//# sourceMappingURL=convert_to_json.js.map