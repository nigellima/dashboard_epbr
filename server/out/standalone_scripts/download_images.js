"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../db/models");
const lwip = require("lwip");
var Sync = require('sync');
const await_1 = require("../lib/await");
Sync(function () {
    const records = await_1.await(db.models.DrillingRigOffshore.findAll({ order: 'name' }));
    console.log('num: ', records.length);
    for (var record of records) {
        console.log(record.id);
        if (!record.photo)
            continue;
        lwip.open(record.photo, 'jpg', onPhotoOpen.bind(this, record.id));
    }
});
function onPhotoOpen(id, err, image) {
    if (!image)
        return;
    const fileName = 'out/img_' + id + '_original.jpg';
    image.writeFile(fileName, function (err) {
        console.log(fileName);
    });
}
//# sourceMappingURL=download_images.js.map