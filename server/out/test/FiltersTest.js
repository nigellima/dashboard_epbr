"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fiberTests = require("./lib/fiberTests");
const Filters = require("../lib/Filters");
const libAwait = require("../lib/await");
var notModGroup = {
    first: (test) => {
        const results = libAwait.await(Filters.getFilterResult('DrillingRigs', 'contractor_name'));
        test.equal(2, results.length);
        test.equal('Petrobras', results[0].value);
        test.equal(4, results[0].qtt);
        test.done();
    }
};
exports.notModDBGroup = fiberTests.convertTests(notModGroup, true);
//# sourceMappingURL=FiltersTest.js.map