"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fiberTests = require("./lib/fiberTests");
const db = require("../db/models");
const dbServerController = require("../controllers/dbServerController");
const await_1 = require("../lib/await");
const utils = require('./lib/utils');
const notModDBGroup = {
    personsOfContractedInProject: (test) => {
        const revampId = utils.idByName('Project', 'Revamp de Mexilhão');
        const filters = {
            id: revampId,
            index: 1
        };
        const query = {
            queryName: 'personsOfContractedInProject',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(3, resQueryValues.records.length);
        test.equal('Felipe', resQueryValues.records[0].person_name);
        test.equal('Marcelo', resQueryValues.records[1].person_name);
        test.done();
    },
    personsOfOwnerInProject: (test) => {
        const revampId = utils.idByName('Project', 'Revamp de Mexilhão');
        const filters = {
            id: revampId,
            index: 1
        };
        const query = {
            queryName: 'personsOfOwnerInProject',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(2, resQueryValues.records.length);
        test.equal('Felipe', resQueryValues.records[0].person_name);
        test.equal('Guilherme Stiebler', resQueryValues.records[1].person_name);
        test.done();
    },
    projectsTargetSales: (test) => {
        const revampId = utils.idByName('Project', 'Revamp de Mexilhão');
        const filters = {
            fase: 'CAPEX',
            type: 'Petróleo'
        };
        const query = {
            queryName: 'projectsTargetSales',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(1, resQueryValues.records.length);
        test.equal('Libra', resQueryValues.records[0].p_name);
        test.equal('Ouro Preto', resQueryValues.records[0].c_name);
        test.done();
    },
    projectTypesAndStages: (test) => {
        const filters = {
            fase: 'CAPEX',
        };
        const query = {
            queryName: 'projectTypesAndStages',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(3, resQueryValues.records.length);
        test.equal('Petróleo', resQueryValues.records[1].segment_type);
        test.equal('CAPEX', resQueryValues.records[1].stage);
        test.done();
    },
    Project: (test) => {
        const projects = await_1.await(db.models.Project.findAll());
        const mexilhao = projects[0];
        const objs = mexilhao.objects;
        test.equal('Plataforma', objs[0].description);
        const jsonField = JSON.parse(mexilhao.json_field);
        test.equal(2, jsonField.contractors.length);
        test.equal("39", jsonField.contractors[0].contractor_id);
        test.equal(3, jsonField.contractors[0].persons_id.length);
        test.equal(1, jsonField.contractors[0].persons_id[0]);
        test.equal('contrato global', jsonField.contractors[0].scope);
        test.equal('engenharia', jsonField.contractors[1].scope);
        test.done();
    },
    personRelatedProjectsContractor: (test) => {
        const filters = {
            id: '4',
        };
        const query = {
            queryName: 'personRelatedProjects',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(1, resQueryValues.records.length);
        test.equal('Revamp de Mexilhão', resQueryValues.records[0].p_name);
        test.done();
    },
    personRelatedProjectsContracteds: (test) => {
        const filters = {
            id: '5',
        };
        const query = {
            queryName: 'personRelatedProjects',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(2, resQueryValues.records.length);
        test.equal('Libra', resQueryValues.records[0].p_name);
        test.equal('Áries', resQueryValues.records[1].p_name);
        test.done();
    },
    contractRelatedProjects: (test) => {
        const filters = {
            id: '3',
        };
        const query = {
            queryName: 'contractRelatedProjects',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(1, resQueryValues.records.length);
        test.equal('Revamp de Mexilhão', resQueryValues.records[0].p_name);
        test.done();
    },
    objectRelatedProjects: (test) => {
        const filters = {
            id: utils.idByName('ProductionUnit', 'Pioneer'),
            modelName: 'ProductionUnit'
        };
        const query = {
            queryName: 'objectRelatedProjects',
            filters: filters
        };
        const reqQueryValues = { query };
        const resQueryValues = utils.getJsonResponse.sync(null, dbServerController.getQueryData, reqQueryValues);
        test.equal(1, resQueryValues.records.length);
        test.equal('Revamp de Mexilhão', resQueryValues.records[0].p_name);
        test.done();
    },
};
const group = {
    ProjectEdit: (test) => {
        {
            const projects = await_1.await(db.models.Project.findAll());
            const mexilhao = projects[0];
            const jsonField1 = {
                "contractors": [
                    {
                        "scope": "contrato global",
                        "persons_id": ["1", "2", "3"],
                        "contractor_id": "39",
                        contracts_id: ["2", "3"]
                    },
                    {
                        "scope": "engenharia",
                        "persons_id": ["2", "3"],
                        "contractor_id": "17",
                        contracts_id: ["2", "1"]
                    }
                ],
                owner_persons_id: ["2", "1"]
            };
            mexilhao.json_field = jsonField1;
            await_1.await(mexilhao.save());
        }
        const projects = await_1.await(db.models.Project.findAll());
        const mexilhao = projects[0];
        const jsonField = JSON.parse(mexilhao.json_field);
        test.equal(2, jsonField.contractors.length);
        test.equal("39", jsonField.contractors[0].contractor_id);
        test.equal(3, jsonField.contractors[0].persons_id.length);
        test.equal(1, jsonField.contractors[0].persons_id[0]);
        test.equal('contrato global', jsonField.contractors[0].scope);
        test.equal('engenharia', jsonField.contractors[1].scope);
        test.done();
    }
};
exports.notModDBGroup = fiberTests.convertTests(notModDBGroup, true);
exports.group = fiberTests.convertTests(group, false);
//# sourceMappingURL=ProjectTest.js.map