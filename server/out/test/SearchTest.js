"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fiberTests = require("./lib/fiberTests");
const search_1 = require("../lib/search");
const await_1 = require("../lib/await");
var group = {
    searchLike: (test) => {
        const result = await_1.await(search_1.searchLike('Guilherme', 5));
        test.equal(2, result.length);
        test.equal('Guilherme Stiebler', result[0].name);
        test.equal(1, result[0].id);
        test.equal('Petrobrás compra Statoil', result[1].name);
        test.done();
    },
    searchInsight: (test) => {
        const result = await_1.await(search_1.searchLike('verdade', 5));
        test.equal(1, result.length);
        test.equal('Petrobrás compra Statoil', result[0].name);
        test.done();
    },
    searchEqual: (test) => {
        const resultAbalone = await_1.await(search_1.searchEqual('Abalone', 1));
        test.equal(1, resultAbalone.length);
        test.equal('Abalone', resultAbalone[0].name);
        test.equal('OilField', resultAbalone[0].model);
        const resultEmpty = await_1.await(search_1.searchEqual('Aba', 1));
        test.equal(0, resultEmpty.length);
        test.done();
    },
};
exports.group = fiberTests.convertTests(group, true);
//# sourceMappingURL=SearchTest.js.map