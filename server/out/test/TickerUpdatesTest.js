"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fiberTests = require("./lib/fiberTests");
const TickerUpdatesController = require("../controllers/TickerUpdatesController");
var utils = require('./lib/utils');
var notModGroup = {
    get: (test) => {
        const req = {};
        const tickerResults = utils.getJsonResponse.sync(null, TickerUpdatesController.getUpdates, req);
        test.equal(6, tickerResults.items.length);
        test.equal('Insight', tickerResults.items[0].category);
        test.equal('Petrobrás é privatizada', tickerResults.items[0].title);
        test.equal('/app/view_new?id=3', tickerResults.items[0].link);
        test.equal('Atualização', tickerResults.items[4].category);
        test.equal('Navio Transpetro Ataulfo Alves: Porte Bruto (DWT), Ano de construção', tickerResults.items[4].title);
        test.equal('/app/view_record?source=Fleet&id=1', tickerResults.items[4].link);
        test.done();
    }
};
exports.notModDBGroup = fiberTests.convertTests(notModGroup, true);
//# sourceMappingURL=TickerUpdatesTest.js.map