'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = function (db) {
    const InsightsPublisher = db.models.InsightsPublisher;
    const newRecordsData = [
        {
            order: 0,
            section: 'flexSlider',
            insight_id: 1,
        },
        {
            order: 1,
            section: 'flexSlider',
            insight_id: 3,
        },
        {
            order: 2,
            section: 'flexSlider',
            insight_id: 2,
        },
        {
            order: 0,
            section: 'section2Articles',
            insight_id: 3,
        },
        {
            order: 1,
            section: 'section2Articles',
            insight_id: 2,
        },
    ];
    return InsightsPublisher.bulkCreate(newRecordsData);
};
//# sourceMappingURL=InsightsPublisher.js.map