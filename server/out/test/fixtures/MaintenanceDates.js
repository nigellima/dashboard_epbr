'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const utils = require("../lib/utils");
module.exports = function (db) {
    const MaintenanceDate = db.models.MaintenanceDate;
    const newRecordsData = [
        {
            period: '2015-10-20',
            production_unit_id: utils.idByName('ProductionUnit', 'Capixaba'),
        },
        {
            period: '2010-02-20',
            production_unit_id: utils.idByName('ProductionUnit', 'Capixaba'),
        },
        {
            period: '2020-01-01',
            production_unit_id: utils.idByName('ProductionUnit', 'Pioneiro de Libra'),
        },
    ];
    return MaintenanceDate.bulkCreate(newRecordsData);
};
//# sourceMappingURL=MaintenanceDates.js.map