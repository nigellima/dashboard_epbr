"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils = require("../lib/utils");
const await_1 = require("../../lib/await");
module.exports = function (db) {
    const abaloneId = utils.idByName('OilField', 'Abalone');
    const guilhermeId = utils.idByName('Person', 'Guilherme Stiebler');
    const newsHTML = '<p>um campo: <a href="/app/view_record?source=OilField&amp;id=' + abaloneId +
        '" style="background-color: rgb(255, 255, 255);">Abalone</a> ' +
        'esse aqui é o google <a href="http://google.com" target="" style="background-color: rgb(255, 255, 255);">' +
        'Google</a> link de verdade e aqui um nome: </p><p><a href="/app/view_record?source=Person&amp;id=' + guilhermeId +
        '" style="background-color: rgb(255, 255, 255);">' +
        'Guilherme Stiebler</a></p>';
    const camamuId = utils.idByName('Basin', 'Camamu');
    const contentCamamu = '<a href="/app/view_record?source=Basin&amp;id=' + camamuId + '" >Camamu</a> ';
    const newsObjs = [];
    for (var newsObj of newsObjs) {
        await_1.await(db.models.News.create(newsObj));
    }
};
//# sourceMappingURL=NewsFixtures.js.map