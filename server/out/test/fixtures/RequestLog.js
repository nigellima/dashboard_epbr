'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const await_1 = require("../../lib/await");
module.exports = function (db) {
    const objs = [
        {
            user: "gstiebler",
            agent: 'agent',
            path: "/get_table_data",
            query: { "queryName": "FPSOs", "queryParams": { "pagination": { "first": "0", "itemsPerPage": "10" }, "order": [{ "fieldName": "pu_name", "dir": "asc" }] } }
        },
        {
            user: "gstiebler",
            agent: 'agent',
            path: "/view_record/",
            query: { "dataSource": "ProductionUnit", "id": "3" }
        },
        {
            user: "maciel",
            agent: 'agent',
            path: "/get_record/",
            query: { "id": "2", "optionsName": "SingleNews" }
        },
    ];
    for (var obj of objs) {
        await_1.await(db.models.RequestLog.create(obj));
    }
};
//# sourceMappingURL=RequestLog.js.map