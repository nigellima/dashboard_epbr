"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db = require("../../db/models");
const await_1 = require("../../lib/await");
function createFixtures() {
    await_1.await(require('./User')(db));
    await_1.await(require('./Company')(db));
    await_1.await(require('./Refinery')(db));
    await_1.await(require('./Terminal')(db));
    await_1.await(require('./Fleet')(db));
    await_1.await(require('./Basin')(db));
    require('./Block')(db);
    await_1.await(require('./DrillingRigOffshore')(db));
    await_1.await(require('./DrillingRigOnshore')(db));
    require('./Person')(db);
    require('./OilField')(db);
    await_1.await(require('./Seismic')(db));
    await_1.await(require('./AmbientalLicense')(db));
    await_1.await(require('./Reserve')(db));
    require('./NewsFixtures')(db);
    await_1.await(require('./ComercialDeclarationFixture')(db));
    await_1.await(require('./ProductionUnit')(db));
    require('./Well')(db);
    await_1.await(require('./HydrocarbonEvidence')(db));
    await_1.await(require('./GasPipeline')(db));
    await_1.await(require('./OilPipeline')(db));
    await_1.await(require('./GasMovement')(db));
    await_1.await(require('./Production')(db));
    require('./IndustrySegment')(db);
    await_1.await(require('./Bid')(db));
    require('./Contract')(db);
    await_1.await(require('./MaintenanceDates')(db));
    await_1.await(require('./ErrorReport')(db));
    await_1.await(require('./ExcelImportLog')(db));
    await_1.await(require('./InsightsPublisher')(db));
    require('./UpdateLog')(db);
    require('./Boat')(db);
    require('./Project')(db);
    require('./RequestLog')(db);
    for (var i = 1; i <= 3; i++) {
        const company = await_1.await(db.models.Company.findById(i));
        company.main_person_id = i;
        await_1.await(company.save());
    }
}
exports.createFixtures = createFixtures;
;
//# sourceMappingURL=initial_data.js.map