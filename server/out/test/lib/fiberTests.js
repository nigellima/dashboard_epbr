"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
process.env['NODE_ENV'] = 'test';
const winston = require("winston");
const db = require("../../db/models");
const createFixtures = require("../fixtures/initial_data");
var Sync = require('sync');
const await_1 = require("../../lib/await");
var umzug = require('../../lib/InitUmzug');
var lastFunctionModfiesDB = true;
process.on('uncaughtException', function (err) {
    winston.error(err.stack);
});
function initializeDB() {
    try {
        await_1.await(db.sequelize.getQueryInterface().dropAllTables());
        console.log('before db');
        await_1.await(umzug.up());
        console.log('calling db');
        createFixtures.createFixtures();
    }
    catch (e) {
        winston.error(e.errors);
        winston.error(e.stack);
    }
}
exports.initializeDB = initializeDB;
function getDefaultSetUpFn(modifiesDB) {
    return function (callback) {
        if (lastFunctionModfiesDB) {
            initializeDB();
        }
        callback();
        lastFunctionModfiesDB = modifiesDB;
    };
}
function showErrosOnCallback(test, callback) {
    try {
        callback(test);
    }
    catch (e) {
        winston.error(e);
        winston.error(e.stack);
    }
}
function syncBaseFunc(callback) {
    var f = function (test) {
        Sync(function () {
            showErrosOnCallback(test, callback);
        });
    };
    return f;
}
function convertTests(group, doNotModifyDB) {
    let newGroup = {};
    newGroup.setUp = syncBaseFunc(getDefaultSetUpFn(!doNotModifyDB));
    for (var propertyName in group) {
        newGroup[propertyName] = syncBaseFunc(group[propertyName]);
    }
    return newGroup;
}
exports.convertTests = convertTests;
//# sourceMappingURL=fiberTests.js.map