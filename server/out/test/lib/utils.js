"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const await_1 = require("../../lib/await");
const db = require("../../db/models");
const winston = require("winston");
function idByValue(modelName, fieldName, value) {
    const model = db.models[modelName];
    const filter = {};
    filter[fieldName] = value;
    const record = await_1.await(model.findOne({ where: filter }));
    if (!record) {
        winston.error('Record not found: ', modelName, fieldName, value);
        return -1;
    }
    return record.id;
}
exports.idByValue = idByValue;
function idByName(modelName, value) {
    return idByValue(modelName, 'name', value);
}
exports.idByName = idByName;
function deStringify(json) {
    var str = JSON.stringify(json);
    return JSON.parse(str);
}
exports.deStringify = deStringify;
function getJsonResponse(func, req, callback) {
    const res = {
        json: jsonRes,
        status: status
    };
    func(req, res);
    function jsonRes(response) {
        callback(null, deStringify(response));
    }
    function status(code) {
        const result = {
            json: function (response) {
                callback(null, { code: code, error: deStringify(response) });
            }
        };
        return result;
    }
}
exports.getJsonResponse = getJsonResponse;
function compareArray(test, array1, array2) {
    test.equal(array1.length, array2.length);
    for (var i = 0; i < array1.length; i++)
        test.equal(array1[i], array2[i]);
}
exports.compareArray = compareArray;
//# sourceMappingURL=utils.js.map